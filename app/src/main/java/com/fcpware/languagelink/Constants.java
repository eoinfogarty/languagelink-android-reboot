package com.fcpware.languagelink;

public class Constants {
    public static final String PACKAGE = "com.fcpware.languagelink";
    public static final String SERVICE = "language-link-android";
    public static final String HOST = "54.164.48.7";
    public static final int PORT = 5222;
    public static final int PACKET_REPLY_TIMEOUT = 30000;

    //s3
    public static final String AWS_ACCOUNT_ID = "978799865060";
    public static final String COGNITO_POOL_ID = "us-east-1:9579ae88-9515-4a2f-97fd-d12d8d47fd66";
    public static final String COGNITO_ROLE_UNAUTH = "arn:aws:iam::978799865060:role/Cognito_LanguageExchangeUnauth_DefaultRole";
    public static final String BUCKET_NAME = "language-exchange/avatars";

}
