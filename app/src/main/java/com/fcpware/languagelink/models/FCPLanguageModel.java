package com.fcpware.languagelink.models;

import android.content.Context;

import com.fcpware.languagelink.R;

import java.io.Serializable;

/**
 * represents a language a language and its proficiency
 */
public class FCPLanguageModel implements Serializable {

    public static final int BEGINNER = 0;
    public static final int INTERMEDIATE = 1;
    public static final int ADVANCED = 2;
    public static final int FLUENT = 3;


    private String name;
    private int proficencyCode;


    public FCPLanguageModel(String language, int code){
        name = language;
        proficencyCode = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProficiencyCode() {
        return proficencyCode;
    }

    public void setProficiencyCode(int proficencyCode) {
        this.proficencyCode = proficencyCode;
    }

    /**
     * We need a context so we can get a list in different languages
     *
     * the proficiency list
     * <array name="proficiency_list">
     *   <item>Beginner</item>
     *   <item>Intermediate</item>
     *   <item>Advanced</item>
     *   <item>Fluent</item>
     * </array>
     **/

    public String getProficiencyName(Context context) {
        String[] proficiency_array = context.getResources().getStringArray(R.array.proficiency_list);

        return proficiency_array[proficencyCode];
    }

}
