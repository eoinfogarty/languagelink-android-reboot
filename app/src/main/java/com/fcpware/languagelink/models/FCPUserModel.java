package com.fcpware.languagelink.models;

import android.content.Context;
import android.util.Log;

import com.fcpware.languagelink.XMPP.vcard.FCPVCardManager;
import com.fcpware.languagelink.XMPP.vcard.packet.VCard;
import com.fcpware.languagelink.managers.FCPAvatarPresenceManager;

import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.roster.packet.RosterPacket;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * The model that represents a user or a friend
 */
//TODO split into two, have a buddy model that extends the user model

public class FCPUserModel {
    private static final String TAG = "FCPUserModel";

    // Singleton variables
    private static FCPUserModel instance = null;

    private ArrayList<FCPLanguageModel> languages = new ArrayList<>();
    private RosterPacket.ItemStatus subscriptionStatus;
    private RosterPacket.ItemType subscriptionType;
    private Presence presence;
    private VCard vCard = new VCard();

    public FCPUserModel() {
        Log.d(TAG, "Creating user model");
    }

    public synchronized static FCPUserModel getInstance() {
        if (instance == null) {
            instance = new FCPUserModel();
        }
        return instance;
    }

    public void createFromVCard(VCard vCard) {
        Log.d(TAG, "Creating from vCard");

        this.vCard = vCard;
        setNickName(vCard.getNickName());
        setAvatar(vCard.getAvatar());
        setJabberId(vCard.getJabberId());
        setLanguages(vCard.getLanguages());
        setIntro(vCard.getIntro());
    }

    public VCard getVCard() {
        return vCard;
    }

    public void save(XMPPTCPConnection connection, Context context) {
        new Thread(new SaveTask(connection, context)).start();
    }

    public void setLanguages(ArrayList<FCPLanguageModel> languages) {
        Log.d(TAG, "new languages size : " + languages.size());
        Log.d(TAG, "old languages size : " + this.languages.size());

        this.languages = languages;
        vCard.setLanguages(languages);
    }

    public RosterPacket.ItemType getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(RosterPacket.ItemType subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

    public Presence getPresence() {
        return presence;
    }

    public void setPresence(Presence presence) {
        this.presence = presence;
    }

    public RosterPacket.ItemStatus getSubscriptionStatus() {
        return subscriptionStatus;
    }

    public void setSubscriptionStatus(RosterPacket.ItemStatus subscriptionStatus) {
        this.subscriptionStatus = subscriptionStatus;
    }

    public String getJabberId() {
        return vCard.getJabberId();
    }

    public void setJabberId(String jabberId) {
        Log.d(TAG, "JabberId set :" + jabberId);
        vCard.setJabberId(jabberId);
    }

    public byte[] getAvatar() {
        return vCard.getAvatar();
    }

    public void setAvatar(byte[] avatar) {
        Log.d(TAG, "Avatar set :");
        vCard.setAvatar(avatar);
    }

    public String getNickName() {
        return vCard.getNickName();
    }

    public void setNickName(String nickName) {
        vCard.setNickName(nickName);
    }

    public ArrayList<FCPLanguageModel> getLanguages() {

        Collections.sort(languages, new Comparator<FCPLanguageModel>() {
            @Override
            public int compare(FCPLanguageModel lhs, FCPLanguageModel rhs) {
                return lhs.getProficiencyCode()-rhs.getProficiencyCode();
            }
        });

        return languages;
    }

    public String getIntro() {
        return vCard.getIntro();
    }

    public void setIntro(String intro) {
        vCard.setIntro(intro);
    }

    class SaveTask implements Runnable {

        XMPPTCPConnection connection;
        Context context;

        public SaveTask(XMPPTCPConnection connection, Context context) {
            this.connection = connection;
            this.context = context;
        }

        @Override
        public void run() {
            try {
                Log.d(TAG, "VCard save with packet id " + vCard.getPacketID());
                /**
                 * Here we need to listen for the result of the vcard set
                 * and send a presence with the new avatar hash with if the upload is successful
                 */
//                connection.addPacketListener(new VCardSaveResultListener().invoke(),
//                        new PacketIDFilter(vCard.getPacketID()));
                FCPVCardManager.getInstanceFor(connection).saveVCard(vCard);
                //vCard.save(connection);
            } catch (SmackException.NoResponseException | XMPPException.XMPPErrorException | SmackException.NotConnectedException e) {
                e.printStackTrace();
            }
        }

        private class VCardSaveResultListener {
            public PacketListener invoke() {
                return new PacketListener() {
                    @Override
                    public void processPacket(Packet packet) throws SmackException.NotConnectedException {
                        Log.d(TAG, "VCard save - Packet back :  " + packet.getPacketID());
                        if (((IQ) packet).getType() == IQ.Type.result) {
                            FCPAvatarPresenceManager fcpAvatarPresenceManager = FCPAvatarPresenceManager.getInstance(context);
                            fcpAvatarPresenceManager.publishAvatarUpdatePresence(getAvatar());
                        }
                        connection.removePacketListener(this);
                    }
                };
            }
        }
    }
}
