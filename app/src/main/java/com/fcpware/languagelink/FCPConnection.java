package com.fcpware.languagelink;

import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;

import com.fcpware.languagelink.events.BusProvider;
import com.fcpware.languagelink.events.ConnectivityEvent;
import com.squareup.otto.Subscribe;

import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.ReconnectionManager;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.sasl.provided.SASLPlainMechanism;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.ping.PingFailedListener;
import org.jivesoftware.smackx.ping.PingManager;

import java.io.IOException;

/**
 * This class manages an xmpp connection
 * It will handle reconnects, keep alives and stream management
 */
public class FCPConnection implements ConnectionListener , PingFailedListener{
    private static final String TAG = FCPConnection.class.getSimpleName();

    private XMPPTCPConnection connection;

    // Singleton variable
    private static FCPConnection instance = null;

    public synchronized static FCPConnection getInstance() {
        if(instance==null){
            instance = new FCPConnection();
        }
        return instance;
    }

    public FCPConnection(){
        Log.d(TAG, "Creating the Connection");
        BusProvider.getInstance().register(this);

        XMPPTCPConnectionConfiguration.Builder conf = XMPPTCPConnectionConfiguration.builder();
        conf.setHost(Constants.HOST);
        conf.setPort(Constants.PORT);
        conf.setServiceName(Constants.HOST); //todo where do i set the resource ie language-link-android
        conf.setSendPresence(false);
        conf.setDebuggerEnabled(true);

        connection = new XMPPTCPConnection(conf.build());
        connection.setPacketReplyTimeout(Constants.PACKET_REPLY_TIMEOUT);
        connection.addConnectionListener(this);

//        ServerPingWithAlarmManager pingWithAlarmManager = ServerPingWithAlarmManager.getInstanceFor(connection);
//        pingWithAlarmManager.setEnabled(true);
//
//        Log.d(TAG, "Pingmanager enabled : " + pingWithAlarmManager.isEnabled());


        PingManager pingManager = PingManager.getInstanceFor(connection);
        pingManager.setPingInterval(900); //15 minute ping
        Log.d(TAG, " Ping : " + pingManager.getPingInterval());
//
        //TODO test
        ReconnectionManager reconnectionManager = ReconnectionManager.getInstanceFor(connection);
        reconnectionManager.enableAutomaticReconnection();

        Log.d(TAG, "Automatic reconnection enabled : " + ReconnectionManager.getInstanceFor(connection).isAutomaticReconnectEnabled());

        //TODO do i need? can I use?
        SASLAuthentication.blacklistSASLMechanism("SCRAM-SHA-1");
        SASLAuthentication.blacklistSASLMechanism("DIGEST-MD5");
        SASLAuthentication.registerSASLMechanism(new SASLPlainMechanism());

    }

    @Subscribe
    public void ConnectivityChanged(ConnectivityEvent event){

        Log.d(TAG, "Connectivity event recieved");
        Log.d(TAG, "Connection status : " + connection.isConnected());

            ConnectivityManager cm = ((ConnectivityManager) event.getContext().getSystemService(Context.CONNECTIVITY_SERVICE));
            if (cm == null) {
                return;
            }
            // Now to check if we're actually connected
            if (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected()) {

                //TODO maybe post to crashalytics
                    Thread thread = new Thread(new Runnable(){
                        @Override
                        public void run() {
                            try {
                                Log.d(TAG, "Attempting connect " + connection.isSmResumptionPossible());
                                connection.connect();

                            } catch (SmackException.ConnectionException e){
                                Log.d(TAG, "Exception : " + e.getFailedAddresses());
                            } catch (SmackException | XMPPException | IOException e) {
                                e.printStackTrace();

                            }
                        }
                    });

                    thread.start();
            }
    }

    public Boolean connect(String user, String password){
        try {

            connection.connect();
            connection.setUseStreamManagement(true);
            connection.setUseStreamManagementResumption(true);
            connection.login(user, password);

        } catch (XMPPException e) {
            e.printStackTrace();
            return false; //TODO we can send more information back to the user as to why they failed
        } catch (SmackException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public Boolean sendPacket(Packet packet){
        try {
            connection.sendPacket(packet);
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
            return false; //TODO i should send information as to why it failed and ehat happened
        }
        return true;
    }

    public boolean isConnected() {
        //TODO maybe ping? is it useful
        Boolean isConnected = connection != null && connection.isConnected();
        Log.d(TAG, "isConnected : " + isConnected);
        return isConnected;
    }

    public void disconnect(){
        try {

            connection.sendPacket(new Presence(Presence.Type.unavailable));
            connection.disconnect();
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void connected(XMPPConnection connection) {
        Log.d(TAG, "Connection Listener - connected to " + connection.getHost());
        Log.d(TAG, "Connection Listener - authentucated: " + connection.isAuthenticated());

    }

    @Override
    public void authenticated(XMPPConnection connection, boolean resumed) {
        Log.d(TAG, "Connection Listener - authenticated");
    }


    @Override
    public void connectionClosed() {
        Log.d(TAG, "Connection Listener - connectionClosed");
    }

    @Override
    public void connectionClosedOnError(Exception e) {
        Log.d(TAG, "Connection Listener - connectionClosedOnError");
        Log.d(TAG, connection.getStreamId());
        Log.d(TAG, "" + connection.getLastStanzaReceived());
        Log.d(TAG, connection.getHost());
        Log.d(TAG, connection.getUser());

        try {
            connection.connect();
        } catch (SmackException e1) {
            Log.d(TAG, "connectionClosedOnError Listener - connect failed");
            e1.printStackTrace();
        } catch (IOException | XMPPException e1) {
            e1.printStackTrace();
        }
    }

    @Override
    public void reconnectingIn(int seconds) {
        Log.d(TAG, "Connection Listener - reconnectingIn " + seconds);
   }

    @Override
    public void reconnectionSuccessful() {
        Log.d(TAG, "Connection Listener - reconnectionSuccessful");
    }

    @Override
    public void reconnectionFailed(Exception e) {
        Log.d(TAG, "Connection Listener - reconnectionFailed");
        Log.d(TAG, "Connection Listener - reconnectionFailed: " + e.getMessage());
    }

    public XMPPTCPConnection getConnection() {
        return connection;
    }

    @Override
    public void pingFailed() {
        try {
            //todo just try regular connect , i need to login if connect fails
            connection.connect();

        } catch (SmackException | IOException | XMPPException e) {
            e.printStackTrace();
       }
    }
}
