package com.fcpware.languagelink.events;

/**
 * todo
 * Created by eoinfogarty on 16/11/14.
 */
public class ConnectionEvent {

    public final Boolean connected;

    public ConnectionEvent(Boolean connected){
        this.connected = connected;
    }

    public Boolean isConnected() {
        return connected;
    }
}
