package com.fcpware.languagelink.events;

import com.fcpware.languagelink.ORM.ChatSummary;

/**
 * An event fired when a new summary object is created
 */
public class NewSummaryEvent {

    private ChatSummary newSummary;

    public NewSummaryEvent(ChatSummary newSummary) {
        this.newSummary = newSummary;
    }

    public ChatSummary getNewSummary() {
        return newSummary;
    }

}
