package com.fcpware.languagelink.events;

import com.fcpware.languagelink.ORM.ChatSummary;

/**
 * A summary update event holds the new summary object and a tag for what is being updated
 */
public class SummaryUpdateEvent {

    private ChatSummary newSummary;
    private int update_tag;

    public SummaryUpdateEvent(ChatSummary newSummary, int update_tag) {
        this.newSummary = newSummary;
        this.update_tag = update_tag;
    }

    public ChatSummary getNewSummary() {
        return newSummary;
    }

    public int getUpdate_tag() {
        return update_tag;
    }

}
