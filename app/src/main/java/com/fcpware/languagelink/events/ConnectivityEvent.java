package com.fcpware.languagelink.events;

import android.content.Context;
import android.content.Intent;

/**
 * //TODO
 * Created by eoinfogarty on 10/12/14.
 */
public class ConnectivityEvent {

    private Intent connectivityEvent;
    private Context context;

    public ConnectivityEvent(Context context, Intent connectivityEvent) {
        this.connectivityEvent = connectivityEvent;
        this.context = context;
    }

    public Intent getConnectivityEvent() {
        return connectivityEvent;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
