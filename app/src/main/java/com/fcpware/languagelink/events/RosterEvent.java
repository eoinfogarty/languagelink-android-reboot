package com.fcpware.languagelink.events;

import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.roster.packet.RosterPacket;

/**
 * Is fired whenever an event happens to the roster such as enteries added, updated , removed etc
 */
public class RosterEvent {

    private int action;
    private String entry;
    private RosterPacket.ItemStatus entryStatus;
    private RosterPacket.ItemType entryType;
    private Presence presence;


    public RosterEvent(int action, String entries, RosterPacket.ItemStatus entryStatus, RosterPacket.ItemType entryType, Presence presence) {
        this.action = action;
        this.entry = entries;
        this.entryStatus = entryStatus;
        this.entryType = entryType;
        this.presence = presence;
    }

    public Presence getPresence() {
        return presence;
    }

    public RosterPacket.ItemType getEntryType() {
        return entryType;
    }

    public RosterPacket.ItemStatus getEntryStatus() {
        return entryStatus;
    }

    public String getEntry() {
        return entry;
    }

    public int getAction() {
        return action;
    }
}
