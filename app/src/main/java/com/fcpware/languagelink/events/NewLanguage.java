package com.fcpware.languagelink.events;

import com.fcpware.languagelink.models.FCPLanguageModel;

/**
 * Event tht is fired when the user picks a new language
 */
public class NewLanguage {

    public FCPLanguageModel newLanguage;

    public NewLanguage(FCPLanguageModel newLanguage){
        this.newLanguage = newLanguage;
    }

    public FCPLanguageModel getNewLanguage() {
        return newLanguage;
    }
}
