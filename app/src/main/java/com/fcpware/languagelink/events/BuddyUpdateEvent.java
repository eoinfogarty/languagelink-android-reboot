package com.fcpware.languagelink.events;

import org.jivesoftware.smack.packet.Presence;

/**
 * Notifies whoever is listening that something about a buddy has changed
 *
 * Can tell:
 * new avatar
 * new nickname
 * new presence
 */
public class BuddyUpdateEvent {

    private int tag;
    private String jid;
    private String avatarUri;
    private String nickname;
    private Presence presence;

    public BuddyUpdateEvent(int tag, String jid, String avatarUri, String nickname, Presence presence) {
        this.tag = tag;
        this.jid = jid;
        this.avatarUri = avatarUri;
        this.nickname = nickname;
        this.presence = presence;
    }

    public int getTag() {
        return tag;
    }

    public Presence getPresence() {
        return presence;
    }

    public String getNickname() {
        return nickname;
    }

    public String getAvatarUri() {
        return avatarUri;
    }

    public String getJid() {
        return jid;
    }




}
