package com.fcpware.languagelink.events;

import android.net.Uri;

/**
 * Event tht is fired when an avatar id downloaded
 */
public class NewAvatarEvent {
    public final String username;

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public Uri uri;

    public NewAvatarEvent(String name){
        this.username = name;
    }
}
