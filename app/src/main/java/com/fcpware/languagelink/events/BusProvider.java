package com.fcpware.languagelink.events;
/*
 * Copyright (C) 2012 Square, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.squareup.otto.Bus;


/**
 * Maintains a singleton instance for obtaining the bus. Ideally this would be replaced with a more efficient means
 * such as through injection directly into interested classes.
 */
public final class BusProvider extends Bus{

    private static final String TAG = "BusProvider";

    private static BusProvider busHandler = null;

    private Bus bus;
    private final Handler mHandler = new Handler(Looper.getMainLooper());

    public static Bus getInstance() {
        if(busHandler==null){
            Log.d(TAG, "Creating Bus instance");
            busHandler = new BusProvider();
        }

        return busHandler;
    }

    private BusProvider() {
        bus = new Bus();
    }

    @Override public void register(Object obj) {
        /**
         * TODO get the error message
         *  CreateAccount - Exception:
         *
         * CreateAccount - Exception:
         * java.lang.IllegalStateException: Event bus [Bus "default"] accessed from non-main thread null
         */
        bus.register(obj);
    }

    @Override public void unregister(Object obj) {
        bus.unregister(obj);
    }

    @Override public void post(final Object event) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            Log.d(TAG, "Posting event on main thread");
            bus.post(event);
        } else {
            mHandler.post(new Runnable() {
                @Override public void run() {
                    Log.d(TAG, "Posting event normally");
                    bus.post(event);
                }
            });
        }
    }
}
