package com.fcpware.languagelink.events;

import android.net.Uri;

/**
 * When an avatar has successfully been uploaded this event is fired
 * Mostly its to tell the profile avatar pic to update
 * Created by eoinfogarty on 31/10/14.
 */
public class AvatarUploadedEvent {
    public final String key;
    public Uri avatar;

    public AvatarUploadedEvent(String key, Uri avatar){
        this.key = key;
        this.avatar = avatar;
    }
}
