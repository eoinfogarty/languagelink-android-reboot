package com.fcpware.languagelink.events;

/**
 * todo
 * This event will tell anybody who is listening of a subscription a user makes to a buddy
 *
 * For example when you add a new buddy a BuddySubscriptionChange event will be fired with the buddys new status
 *
 * e.g. new BuddySubscriptionChange(foo@bar.com, PersonProfileActivity.PENDING_REQUEST)
 */
public class BuddySubscriptionChange {
    public final String jid;
    public final int newStatus;

    public BuddySubscriptionChange(String jid, int newStatus){
        this.jid = jid;
        this.newStatus = newStatus;
    }

    public String getJid() {
        return jid;
    }

    public int getNewStatus() {
        return newStatus;
    }
}
