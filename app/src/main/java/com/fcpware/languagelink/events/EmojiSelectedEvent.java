package com.fcpware.languagelink.events;

import android.graphics.drawable.Drawable;

public class EmojiSelectedEvent {
    private Drawable emoji;
    private String name;

    public EmojiSelectedEvent(String name, Drawable emoji) {
        this.name = name;

        this.emoji = emoji;
    }

    public Drawable getEmoji() {
        return emoji;
    }

    public String getName() {
        return name;
    }

}
