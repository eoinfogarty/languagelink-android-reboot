package com.fcpware.languagelink.events;

import org.jivesoftware.smack.chat.Chat;

/**
 * Fired when a new chat is created
 */
public class NewChatEvent {
    private String participant;
    private Chat newChat;

    public NewChatEvent(String participant, Chat newChat) {
        this.participant= participant;
        this.newChat=newChat;
    }

    public String getParticipant() {
        return participant;
    }

    public Chat getNewChat() {
        return newChat;
    }
}
