package com.fcpware.languagelink.events;

/**
 * Event that is fired when a new MESSAGE event is received
 */
public class NewMessageEvent {
    public final String username;

    public NewMessageEvent(String name){
        this.username = name;
    }
}
