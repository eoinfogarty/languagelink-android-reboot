package com.fcpware.languagelink;

import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.orm.SugarApp;

import java.io.PrintWriter;
import java.io.StringWriter;

import io.fabric.sdk.android.Fabric;

public class LanguageLinkApplication extends SugarApp {

    private Thread.UncaughtExceptionHandler androidDefaultUEH;

    private Thread.UncaughtExceptionHandler handler = new Thread.UncaughtExceptionHandler() {
        public void uncaughtException(Thread thread, Throwable ex) {
            Log.e("LanguageLinkApplication", "Uncaught exception is: ", ex);
            StringWriter stackTrace = new StringWriter();
            ex.printStackTrace(new PrintWriter(stackTrace));

            // log it & phone home.
            androidDefaultUEH.uncaughtException(thread, ex);
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        androidDefaultUEH = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(handler);
    }
}
