package com.fcpware.languagelink.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.fcpware.languagelink.events.BusProvider;
import com.fcpware.languagelink.events.ConnectivityEvent;

/**
 * //todo
 * <p/>
 * Created by eoinfogarty on 10/12/14.
 */
public class NetworkConnectivityReceiver extends BroadcastReceiver {
    private static final String TAG = "NetworkConnectivityReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        BusProvider.getInstance().register(this);

        BusProvider.getInstance().post(new ConnectivityEvent(context, intent));

        debugIntent(intent);
    }

    private void debugIntent(Intent intent) {
        Log.d(TAG, "action: " + intent.getAction());
        Log.d(TAG, "component: " + intent.getComponent());
        Bundle extras = intent.getExtras();
        if (extras != null) {
            for (String key : extras.keySet()) {
                Log.d(TAG, "key [" + key + "]: " +
                        extras.get(key));
            }
        } else {
            Log.v(TAG, "no extras");
        }
    }

}