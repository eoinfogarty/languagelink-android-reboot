package com.fcpware.languagelink.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.fcpware.languagelink.FCPConnection;

public class OnBootBroadcastReceiver extends BroadcastReceiver {
    public OnBootBroadcastReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Start stop service based on the network connectivity connected or disconnected
        // TODO Note this will not work if the app is on the users sdcard
        if(Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction()))
        {
            /* Only perform this below code if the BroadcastReceiver received the ACTION_BOOT_COMPLETED action.*/
            Log.d("BroadcastReceiver", "on Receive - boot complete sending broadcast");
            // use this to start and trigger a service
            Intent i= new Intent(context, ConnectionService.class);
            // potentially add data to the intent
            context.startService(i);
        } else if(Intent.ACTION_SHUTDOWN.equals(intent.getAction())){
            Log.d("BroadcastReceiver", "on Receive - broadcasting shout down");
            FCPConnection.getInstance().disconnect();
        }

    }


}
