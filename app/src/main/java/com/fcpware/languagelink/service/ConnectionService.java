package com.fcpware.languagelink.service;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.fcpware.languagelink.FCPConnection;
import com.fcpware.languagelink.XMPP.PEP.PEPManager;
import com.fcpware.languagelink.XMPP.vcard.FCPVCardManager;
import com.fcpware.languagelink.XMPP.vcard.packet.VCard;
import com.fcpware.languagelink.XMPP.vcard.provider.VCardProvider;
import com.fcpware.languagelink.amazons3.network.TransferController;
import com.fcpware.languagelink.events.BusProvider;
import com.fcpware.languagelink.events.ConnectionStatusQueryEvent;
import com.fcpware.languagelink.events.LoggedInEvent;
import com.fcpware.languagelink.managers.FCPAvatarPresenceManager;
import com.fcpware.languagelink.managers.FCPChatManager;
import com.fcpware.languagelink.managers.FCPChatSummaryManager;
import com.fcpware.languagelink.managers.FCPNotificationManager;
import com.fcpware.languagelink.managers.FCPRosterManager;
import com.fcpware.languagelink.managers.SharedPreferencesManager;
import com.fcpware.languagelink.models.FCPUserModel;
import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;

import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smackx.caps.EntityCapsManager;
import org.jivesoftware.smackx.chatstates.ChatStateManager;
import org.jivesoftware.smackx.disco.ServiceDiscoveryManager;
import org.jivesoftware.smackx.offline.OfflineMessageManager;
import org.jivesoftware.smackx.vcardtemp.VCardManager;
import org.jxmpp.util.XmppStringUtils;

import java.io.File;

//import com.fcpware.languagelink.XMPP.vcardtemp.packet.VCard;
//import org.apache.harmony.javax.security.sasl.SaslException;
//import org.jivesoftware.smack.SmackAndroid;

public class ConnectionService extends Service {

    public static final int CONNECTED = 1;
    public static final int NOT_CONNECTED = 2;
    private int currentState = NOT_CONNECTED;
    public static final int CONNECTING = 3;
    private static final String TAG = "ConnectionService";
    private SharedPreferencesManager userLoginDetails;
    private FCPAvatarPresenceManager fcpAvatarPresenceManager;
    private FCPNotificationManager fcpNotificationManager;
    private FCPChatSummaryManager fcpChatSummaryManager;
    private OfflineMessageManager offlineMessageManager;
    private FCPConnection fcpConnection;
    private ChatStateManager chatStateManager;
    private FCPRosterManager fcpRosterManager;
    private FCPChatManager fcpChatManager;
    private FCPUserModel fcpUserModel;
    private PEPManager pepManager;

    private EntityCapsManager entityCapsManager;

    //TODO do i need it?
    public ConnectionService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate - Checking userLoginDetails");
        BusProvider.getInstance().register(this);

        Log.d(TAG, "smack - " + SmackConfiguration.isSmackInitialized());

//        PEPProvider prov = new PEPProvider();
//        prov.registerPEPParserExtension("http://jabber.org/protocol/nick", new Nick.Provider());
//
//        // advertise the features I want and can do
//        //TODO maybe dont need
//        ProviderManager.addIQProvider("offline","http://jabber.org/protocol/offline", new OfflineMessageRequest.Provider());
//        //  Offline Message Indicator
//        ProviderManager.addExtensionProvider("offline","http://jabber.org/protocol/offline", new OfflineMessageInfo.Provider());
//
//        ProviderManager.addExtensionProvider("event",
//                "http://jabber.org/protocol/pubsub#event",
//                prov);

        //make sure we are using our own vcard implementation
        ProviderManager.removeIQProvider(VCardManager.ELEMENT, VCardManager.NAMESPACE);
        ProviderManager.addIQProvider(FCPVCardManager.ELEMENT, FCPVCardManager.NAMESPACE, new VCardProvider());
        Log.d(TAG, "Current list of IQ providers after adding custom vcard :  " + ProviderManager.getIQProviders().toString());


        fcpConnection = FCPConnection.getInstance();
        userLoginDetails = new SharedPreferencesManager(this);
        fcpRosterManager = FCPRosterManager.getInstance(getApplicationContext());
        fcpUserModel = FCPUserModel.getInstance();
        VCard usersVCard = userLoginDetails.getVCard();
        fcpUserModel.createFromVCard(usersVCard);
        fcpNotificationManager = FCPNotificationManager.getInstance(getApplicationContext());
        fcpChatManager = FCPChatManager.getInstance(getApplicationContext());
        chatStateManager = ChatStateManager.getInstance(fcpConnection.getConnection());
        pepManager = new PEPManager(fcpConnection.getConnection());
        pepManager.addPEPListener(fcpRosterManager);

        entityCapsManager = EntityCapsManager.getInstanceFor(fcpConnection.getConnection());
        entityCapsManager.enableEntityCaps();
        ServiceDiscoveryManager sdm = ServiceDiscoveryManager.getInstanceFor(fcpConnection.getConnection());

        sdm.addFeature("http://jabber.org/protocol/nick");
        sdm.addFeature("http://jabber.org/protocol/nick+notify");

        //if the user is not logged in we don't need the service
        if (userLoginDetails.hasLoggedIn()) {
            Log.d(TAG, "onCreate - User has already Logged in - starting Connect Task");
            new Connect().execute();
        } else {
            Log.d(TAG, "onCreate - User has not Logged in - stopping service");
            this.stopSelf();
        }
    }

    /**
     * We want the service to remain running as much as possible so we Service.START_STICKY"
     *
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand - Connection Service starting with Service.START_STICKY");

        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind - Package: " + intent.getPackage());
        Log.d(TAG, "onBind - Data: " + intent.getData());
        Log.d(TAG, "onBind - Action: " + intent.getAction());
        Log.d(TAG, "onBind - Scheme: " + intent.getScheme());

        if (fcpConnection.isConnected()) {
            Log.d(TAG, "onBind - Connected and sending bus event");
            // Tell anyone who is listening you are logged in
            BusProvider.getInstance().post(loggedInEvent());
        }

        return new LocalBinder<ConnectionService>(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "on destroy is being called");
        currentState = NOT_CONNECTED;

        BusProvider.getInstance().unregister(this);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                //TODO maybe I dont need to do it
                //TODO probably I have to add methods to recreate
                //todo thread_?
                fcpChatManager = null;
                fcpUserModel = null;
                fcpRosterManager = null;
                pepManager = null;
                userLoginDetails = null;
                fcpConnection.disconnect();


            }
        });
        thread.start();
    }

    @Override
    public boolean onUnbind(final Intent intent) {
        return super.onUnbind(intent);
    }

    public PEPManager getPEPManager() {
        Log.d(TAG, "getPEPManager : " + pepManager.toString());
        return pepManager;
    }


//    public void disconnect() {
//        currentState = NOT_CONNECTED;
//
//        BusProvider.getInstance().unregister(this);
//
//            Thread thread = new Thread(new Runnable(){
//                @Override
//                public void run() {
//                    //TODO maybe I dont need to do it
//                    //TODO probably I have to add methods to recreate
//                    //todo thread_?
//                    fcpChatManager = null;
//                    fcpUserModel = null;
//                    fcpRosterManager = null;
//                    pepManager = null;
//                    userLoginDetails = null;
//                    fcpConnection.disconnect();
//
//
//                }
//            });
//            thread.start();
//    }

    @Produce
    public LoggedInEvent loggedInEvent() {
        // Provide an initial value for location based on the last known position.
        return new LoggedInEvent();
    }

    @Subscribe
    public void statusCheck(ConnectionStatusQueryEvent event) {
        Log.d(TAG, "onLoggedIn - service connected and logged in");
        //if the statusCheck is connected then send a logged in event , if not do nothing because it sould send a logged in event by itself
        if (currentState == CONNECTED) {
            // Tell anyone who is listening you are logged in
            BusProvider.getInstance().post(loggedInEvent());
        }
    }

    private class Connect extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            Boolean didConnect;
            Log.d(TAG, "ConnectTask - Starting");
            currentState = CONNECTING;
            //TODO **important sometimes username can ne lost from preferences
            String username = XmppStringUtils.parseLocalpart(userLoginDetails.getUsername());
            String password = userLoginDetails.getPassword();
            didConnect = fcpConnection.connect(username, password);


            if (didConnect) {

                //fcpConnection.sendPacket(new DiscoverInfo());
                Log.d(TAG, "Connection status for " + username + " - " + fcpConnection.isConnected());

                //TODO move to avatar manager
                // check and see if we need to download the users avatar
                String parsedEntry = XmppStringUtils.parseLocalpart(userLoginDetails.getUsername());
                parsedEntry = XmppStringUtils.unescapeLocalpart(parsedEntry);

                Log.d(TAG, "ConnectTask - Checking for avatar for - " + parsedEntry);
                File avatar = new File(getApplicationContext().getCacheDir(), parsedEntry);
                if (!avatar.exists()) {
                    Log.d(TAG, "checkForWhoNeedsAvatars - File does not exist adding to list for download");
                    TransferController.download(getApplicationContext(), new String[]{parsedEntry});
                }


                didConnect = true;
                Log.d(TAG, "smack - " + SmackConfiguration.isSmackInitialized());
            }

            return didConnect;
        }

        @Override
        protected void onPostExecute(Boolean didConnect) {
            if (didConnect) {
                Log.d(TAG, "ConnectTask - onPostExecute: connected and logged in");
                fcpChatSummaryManager = FCPChatSummaryManager.getInstance(getApplicationContext());
                fcpAvatarPresenceManager = FCPAvatarPresenceManager.getInstance(getApplicationContext());
                currentState = CONNECTED;

                // fcpAvatarPresenceManager.publishAvatarUpdatePresence(fcpUserModel.getAvatar());
                Log.d(TAG, "Connected to Server");
                // Tell anyone who is listening you are logged in
                BusProvider.getInstance().post(loggedInEvent());


                /**
                 * TODo// we need to send a presence when we come online, but when we send a presence we will get all our delayed messages at one
                 * TODO// so we need to make sure our buddy list is fully set before we send our first presence
                 */

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms
                        FCPAvatarPresenceManager.getInstance(getApplicationContext()).publishAvatarUpdatePresence(fcpUserModel.getAvatar());
                    }
                }, 5000);


            }

            //TODO handle the fact you didnt connect
        }
    }
}
