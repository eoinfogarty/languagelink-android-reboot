package com.fcpware.languagelink.UI.friends.actions;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.fcpware.languagelink.R;
import com.fcpware.languagelink.UI.chat.ChatActivity;
import com.fcpware.languagelink.UI.friends.FriendProfileActivity;
import com.fcpware.languagelink.events.BuddySubscriptionChange;
import com.fcpware.languagelink.events.BusProvider;
import com.squareup.otto.Produce;

/**
 * This fragment represents the actions you can perform on a friend when on their profile screen
 *
 * There are 2 options available at the moment, chat and unfriend
 */

public class BuddyActionFragment extends Fragment {

    private String jabberID;


    public static BuddyActionFragment newInstance(String jid){
        Bundle args = new Bundle();
        args.putString("jid", jid);

        BuddyActionFragment frag = new BuddyActionFragment();
        frag.setArguments(args);
        return frag;
    }

    public BuddyActionFragment(){
    }

    @Produce
    public BuddySubscriptionChange subscriptionChange(int buddyStatus) {
        // Provide an initial value for location based on the last known position.
        return new BuddySubscriptionChange(jabberID, buddyStatus);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.jabberID = getArguments().getString("jid", "");


        View root = inflater.inflate(R.layout.fragment_person_profile_friend, null);
        ImageButton remove = (ImageButton) root.findViewById(R.id.removeFriendButton);
        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                try {
//                    //TODO clean, whats happening here?
//                    Toast.makeText(getActivity(), R.string.they_have_been_unfriended, Toast.LENGTH_SHORT).show();
//                    Presence unsubscribe = new Presence(Presence.Type.unsubscribe);
//                    unsubscribe.setTo(jabberID);
//                    getActivity().finish();
//
//                    connection.sendPacket(unsubscribe);
//                    Presence subscribed = new Presence(Presence.Type.subscribed);
//                    subscribed.setTo(jabberID);
//                    connection.sendPacket(subscribed);
//                    fcpBuddyManager.removeBuddy(jabberID);
//                } catch (SmackException.NotConnectedException e) {
//                    e.printStackTrace();
//                }
                BusProvider.getInstance().post(subscriptionChange(FriendProfileActivity.REMOVE));
            }

        });

        ImageButton chat = (ImageButton) root.findViewById(R.id.chatButton);
        chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chatIntent = new Intent(getActivity(), ChatActivity.class);
                chatIntent.putExtra("friendJabberId", jabberID);
                startActivity(chatIntent);
            }
        });
        return root;
    }
}
