package com.fcpware.languagelink.UI.search;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.fcpware.languagelink.BaseActivity;
import com.fcpware.languagelink.R;

/**
 * Holder activity for the search fragment
 */

public class SearchActivity extends BaseActivity {

    private static final String TAG="SearchActivity";

    public static final String TAG_SEARCH_FILTERS = "search_fragment";
    public static final String TAG_SEARCH_RESULTS = "search_results";
    private SearchFilterFragment searchFilterFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setLogo(R.drawable.ic_friend_search);
        toolbar.setTitle(getResources().getString(R.string.search_for_friends));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        FragmentManager fragmentManager = getSupportFragmentManager();

        if (savedInstanceState == null) {
            searchFilterFragment = SearchFilterFragment.newInstance();
            searchFilterFragment.setRetainInstance(true);
            Log.d(TAG, "SavedInstance is null, creating new search fragment , id : " + searchFilterFragment.getId());
        } else {
            searchFilterFragment = (SearchFilterFragment) fragmentManager.findFragmentByTag(TAG_SEARCH_FILTERS);
            Log.d(TAG, "SavedInstance is not null , id :" + searchFilterFragment.getId());
        }

        if (!searchFilterFragment.isAdded()) {
            fragmentManager.beginTransaction()
                    .add(R.id.container, searchFilterFragment, TAG_SEARCH_FILTERS)
                    .commit();
        }
    }

    public void showFragment(String tag){

        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack("filter")
                .setCustomAnimations(R.anim.slide_up_out,R.anim.slide_up_in)
                .replace(R.id.container, searchFilterFragment)
                .commit();

    }
}
