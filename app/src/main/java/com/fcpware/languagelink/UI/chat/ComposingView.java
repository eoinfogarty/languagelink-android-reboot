package com.fcpware.languagelink.UI.chat;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.fcpware.languagelink.R;

/**
 * when the user get a composing chat state notification in chat
 * they will see this view on the bottom of the screen just above the input edit text
 */
public class ComposingView extends LinearLayout{
    public ComposingView(Context context) {
        super(context);

        LayoutInflater.from(context).inflate(R.layout.view_chat_composing, this, true);
    }
}
