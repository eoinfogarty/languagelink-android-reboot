package com.fcpware.languagelink.UI.friends.actions;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.fcpware.languagelink.FCPConnection;
import com.fcpware.languagelink.R;
import com.fcpware.languagelink.UI.friends.FriendProfileActivity;
import com.fcpware.languagelink.events.BuddySubscriptionChange;
import com.fcpware.languagelink.events.BusProvider;
import com.fcpware.languagelink.managers.FCPRosterManager;
import com.squareup.otto.Produce;

import org.jivesoftware.smack.packet.Presence;

/**
 * This fragment represents the actions you get a friend request from somebody
 *
 * There are 2 options available at the moment, accept and reject
 */
public class RequestResponseFragment extends Fragment {

    private String jabberID;
    private FCPConnection fcpConnection;
    private FCPRosterManager fcpRosterManager;

    public static RequestResponseFragment newInstance(String jid){
        Bundle args = new Bundle();
        args.putString("jid", jid);

        RequestResponseFragment frag = new RequestResponseFragment();
        frag.setArguments(args);
        return frag;
    }

    public RequestResponseFragment(){
    }

    @Produce
    public BuddySubscriptionChange subscriptionChange(int buddyStatus) {
        // Provide an initial value for location based on the last known position.
        return new BuddySubscriptionChange(jabberID, buddyStatus);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        jabberID = getArguments().getString("jid");
        fcpRosterManager = FCPRosterManager.getInstance(getActivity());
        fcpConnection = FCPConnection.getInstance();

        View root = inflater.inflate(R.layout.fragment_person_profile_request, null);
        ImageButton approve = (ImageButton) root.findViewById(R.id.approveButton);
        approve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), R.string.friend_request_approved, Toast.LENGTH_SHORT).show();
                Presence subscribe = new Presence(Presence.Type.subscribe);
                subscribe.setTo(jabberID);
                fcpConnection.sendPacket(subscribe);
                Presence subscribed = new Presence(Presence.Type.subscribed);
                subscribed.setTo(jabberID);
                fcpConnection.sendPacket(subscribed);

                // Tell anyone who is listening of this buddies new status
                BusProvider.getInstance().post(subscriptionChange(FriendProfileActivity.FRIEND));
            }
        });

        ImageButton cancel = (ImageButton) root.findViewById(R.id.cancelButton);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                try {
//                    Toast.makeText(getActivity(), R.string.friend_request_rejected, Toast.LENGTH_SHORT).show();
//                    //todo might be of no use
//                    Presence unsubscribe = new Presence(Presence.Type.unsubscribe);
//                    unsubscribe.setTo(jabberID);
//                    connection.sendPacket(unsubscribe);
//
//
//                    Presence unsubscribed = new Presence(Presence.Type.unsubscribed);
//                    unsubscribed.setTo(jabberID);
//                    connection.sendPacket(unsubscribed);
//                    getActivity().finish();
//                    fcpBuddyManager.removeBuddy(jabberID);

                    // Tell anyone who is listening of we want to reject this friend request, send to confirmation
                    BusProvider.getInstance().post(subscriptionChange(FriendProfileActivity.REMOVE));
//                } catch (SmackException.NotConnectedException e) {
//                    e.printStackTrace();
//                }
            }
        });
        return root;
    }
}