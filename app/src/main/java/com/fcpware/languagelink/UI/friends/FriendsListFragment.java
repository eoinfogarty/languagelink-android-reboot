package com.fcpware.languagelink.UI.friends;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import com.fcpware.languagelink.ORM.BuddyModel;
import com.fcpware.languagelink.R;
import com.fcpware.languagelink.UI.ProfileActivity;
import com.fcpware.languagelink.UI.search.SearchActivity;
import com.fcpware.languagelink.events.BuddyListSetEvent;
import com.fcpware.languagelink.events.BuddyListStatusQueryEvent;
import com.fcpware.languagelink.events.BusProvider;
import com.fcpware.languagelink.managers.FCPBuddyManager;
import com.fcpware.languagelink.models.FCPUserModel;
import com.melnykov.fab.FloatingActionButton;
import com.squareup.otto.Subscribe;

import java.util.WeakHashMap;

import se.emilsjolander.stickylistheaders.ExpandableStickyListHeadersListView;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;


/**
 * Created by Eoin Fogarty on 27/08/2014.
 *
 * The class to display a list of your buddies under separate headings
 * The headings are Friend Requests, Friends and Pending ...
 */
public class FriendsListFragment extends Fragment {

    private static final String TAG = "FriendsListFragment";

    private final WeakHashMap<View,Integer> mOriginalViewHeightPool = new WeakHashMap<View, Integer>();
    private ExpandableStickyListHeadersListView mListView;
    private FCPBuddyManager fcpBuddyManager;
    private FriendsListAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        BusProvider.getInstance().register(this); //otto

        fcpBuddyManager = FCPBuddyManager.getInstance(getActivity());

        View root = inflater.inflate(R.layout.fragment_main_friends_list, container, false);
        mListView = (ExpandableStickyListHeadersListView) root.findViewById(R.id.friendsList);

        FloatingActionButton searchBtn = (FloatingActionButton) root.findViewById(R.id.searchButton);
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent searchIntent = new Intent(getActivity(), SearchActivity.class);
                startActivity(searchIntent);
            }
        });

        try{
            FriendListView profileView = new FriendListView(getActivity());
            BuddyModel myProfile = new BuddyModel();
            myProfile.setWithVCard(FCPUserModel.getInstance().getVCard());
            profileView.setBuddy(myProfile);

            profileView.setIntentClass(ProfileActivity.class);
            profileView.setPresenceVisibility(View.INVISIBLE);
            mListView.addHeaderView(profileView);
            // show buddies list
            mAdapter = new FriendsListAdapter(getActivity(), fcpBuddyManager.getBuddyMap().values()); //TODO might not be the best
            mListView.setAdapter(mAdapter);
            mListView.setAnimExecutor(new AnimationExecutor()); //custom expand/collapse animation
            mListView.setOnHeaderClickListener(headerClickListener);

        } catch (NullPointerException e){
            Log.e(TAG, "Buddy manager null - probably not initialized yet");
            e.printStackTrace();
            //check the buddy list status if its not ready it will be soon and we will get an event anway
            BusProvider.getInstance().post(new BuddyListStatusQueryEvent());
        }



        return root;
    }

    StickyListHeadersListView.OnHeaderClickListener headerClickListener = new StickyListHeadersListView.OnHeaderClickListener() {
        @Override
        public void onHeaderClick(StickyListHeadersListView l, View header, int itemPosition, long headerId, boolean currentlySticky) {
            ImageView chevron = (ImageView) header.findViewById(R.id.chevron);
            if (mListView.isHeaderCollapsed(headerId)) {
                mListView.expand(headerId);
                rotate(chevron, -90, 0);
            } else {
                mListView.collapse(headerId);
                rotate(chevron, 0, -90);
            }

        }

        // the position to rotate to,  not the amount of rotation
    private void rotate(ImageView chevron, float from, float degree) {

        final RotateAnimation rotateAnim = new RotateAnimation(from, degree,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f);

        rotateAnim.setDuration(200);
        rotateAnim.setFillAfter(true);
        chevron.startAnimation(rotateAnim);
    }
};


    @Subscribe
    public void onBuddyListSet(BuddyListSetEvent event) {
        Log.d(TAG, "onBuddyListSet - buddy list has been set or changed");
        mAdapter.set(fcpBuddyManager.getBuddyMap().values());
    }

    //expand and collapse animations
    private class AnimationExecutor implements ExpandableStickyListHeadersListView.IAnimationExecutor {

        @Override
        public void executeAnim(final View target, final int animType) {
            if(ExpandableStickyListHeadersListView.ANIMATION_EXPAND==animType&&target.getVisibility()==View.VISIBLE){
                return;
            }
            if(ExpandableStickyListHeadersListView.ANIMATION_COLLAPSE==animType&&target.getVisibility()!=View.VISIBLE){
                return;
            }
            if(mOriginalViewHeightPool.get(target)==null){
                mOriginalViewHeightPool.put(target,target.getHeight());
            }
            final int viewHeight = mOriginalViewHeightPool.get(target);
            float animStartY = animType == ExpandableStickyListHeadersListView.ANIMATION_EXPAND ? 0f : viewHeight;
            float animEndY = animType == ExpandableStickyListHeadersListView.ANIMATION_EXPAND ? viewHeight : 0f;
            final ViewGroup.LayoutParams lp = target.getLayoutParams();
            ValueAnimator animator = ValueAnimator.ofFloat(animStartY, animEndY);
            animator.setDuration(200);
            target.setVisibility(View.VISIBLE);
            animator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {
                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    if (animType == ExpandableStickyListHeadersListView.ANIMATION_EXPAND) {
                        target.setVisibility(View.VISIBLE);
                    } else {
                        target.setVisibility(View.GONE);
                    }
                    target.getLayoutParams().height = viewHeight;
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    lp.height = ((Float) valueAnimator.getAnimatedValue()).intValue();
                    target.setLayoutParams(lp);
                    target.requestLayout();
                }
            });
            animator.start();

        }
    }
}
