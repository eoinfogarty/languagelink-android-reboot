package com.fcpware.languagelink.UI.chat;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.fcpware.languagelink.ORM.ChatSummary;
import com.fcpware.languagelink.R;
import com.fcpware.languagelink.events.BusProvider;
import com.fcpware.languagelink.events.SummaryUpdateEvent;
import com.fcpware.languagelink.managers.FCPChatSummaryManager;
import com.nineoldandroids.animation.Animator;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * This view is the onscreen representation of a chat summary
 * It listens for chat summary updates from FCPChatManager and updates on screen
 */
public class ChatSummaryView extends RelativeLayout implements View.OnClickListener{

    private static final String TAG = "ChatSummaryView";

    private static final int ANIMATION_DURATION = 700;

    private ChatSummary chatSummary;

    private ImageView avatar;
    private TextView nickname;
    private TextView lastMessage;
    private TextView lastMessageDate;
    private TextView unreadMessageCount;
    private FrameLayout unreadCountFrame;

    private Context context;
    private LayoutInflater layoutInflater;

    public ChatSummaryView(Context context) {
        super(context);
        this.context = context;
        init(context);
    }

    public ChatSummaryView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ChatSummaryView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {
        layoutInflater = LayoutInflater.from(context);
        View root = layoutInflater.inflate(R.layout.view_chat_summary, this, true);

        BusProvider.getInstance().register(this); //otto

        avatar = (ImageView) root.findViewById(R.id.avatar);
        nickname = (TextView) root.findViewById(R.id.nicknameTv);
        lastMessage = (TextView) root.findViewById(R.id.lastMessageTv);
        lastMessageDate = (TextView) root.findViewById(R.id.dateTv);
        unreadCountFrame = (FrameLayout) root.findViewById(R.id.unreadCountFrame);
        unreadMessageCount = (TextView) root.findViewById(R.id.unreadMessageCountTv);

    }

    //No animations
    public void setSummary(ChatSummary summary) {
        this.chatSummary = summary;

        Log.d(TAG, "setSummary - avatar : " + chatSummary.getAvatar());
        Picasso.with(context).load(chatSummary.getAvatar()).noFade().skipMemoryCache().into(avatar);

        nickname.setText(chatSummary.getNickname());

        lastMessage.setText(chatSummary.getLastMessageBody());
        lastMessageDate.setText(getDateFormat(chatSummary.getLastMessageDate()));

        unreadMessageCount.setVisibility(chatSummary.getUnreadMessages() > 0 ? View.VISIBLE : View.INVISIBLE);
        unreadCountFrame.setVisibility(chatSummary.getUnreadMessages() > 0 ? View.VISIBLE : View.INVISIBLE);
        setUnreadCountWithLimit();
        setOnClickListener(this);
    }

    //if the user has over 99 unread message its shows up as 99+
    private void setUnreadCountWithLimit() {
        String unreadCount = chatSummary.getUnreadMessages() > 99 ? "99+" : "" + chatSummary.getUnreadMessages();
        unreadMessageCount.setText(unreadCount);
    }

    private void loadAvatarIntoImageView() {
        Picasso.with(context).load(chatSummary.getAvatar()).skipMemoryCache().into(avatar);
    }

    /**
     * this function will return the date formatted into one of three ways
     *
     * HH/mm if the message was today
     * MM/dd if the day was before today
     * YY/MM if the day was in the last year
     *
     *
     * @param time - the time of the message
     * @return formattedDate - the formatted date
     */
    private String getDateFormat(Date time) {
        Calendar now = Calendar.getInstance();
        now.setTime(Calendar.getInstance().getTime());

        Calendar messageReceivedTime = Calendar.getInstance();
        messageReceivedTime.setTime(time);

        //TODO add yesterday, and day like wednesday, thursday etc
        //if the year is the same
        if(now.get(Calendar.YEAR) == messageReceivedTime.get(Calendar.YEAR)){
            //if the year and day are the same
            if(now.get(Calendar.DAY_OF_WEEK) == messageReceivedTime.get(Calendar.DAY_OF_WEEK)){
                //its today
                return new SimpleDateFormat("HH:mm").format(time);
            } else {
                //the years are the same but the days are different
                return new SimpleDateFormat("MM/dd").format(time);
            }
        } else {
            return new SimpleDateFormat("yy/MM").format(time);
        }
    }

    @Override
    public void onClick(View v) {
        Intent chatIntent = new Intent(context, ChatActivity.class);
        chatIntent.putExtra("friendJabberId", chatSummary.getBuddyJid());
        context.startActivity(chatIntent);
    }

    /**
     * Listens for summary update and checks if it needs to update
     * it uses the tag to decide what should be updated on screen
     *
     * @param summaryUpdate - the summary update with a tag
     */
    @Subscribe
    public void onSummaryUpdated(SummaryUpdateEvent summaryUpdate) {
        Log.d(TAG, "onSummaryUpdated - " + summaryUpdate.getUpdate_tag());
        Log.d(TAG, "onSummaryUpdated - " + summaryUpdate.getNewSummary().getNickname());

        if(summaryUpdate.getNewSummary().getBuddyJid().equals(chatSummary.getBuddyJid())){
            handleUpdate(summaryUpdate);
        }

    }

    //with animations
    private void handleUpdate(SummaryUpdateEvent summaryUpdate) {

        switch (summaryUpdate.getUpdate_tag()){
            case FCPChatSummaryManager.UPDATE_AVATAR:
                Log.d(TAG, "onSummaryUpdated - We got an avatar update");
                loadAvatarIntoImageView();
                break;
            case FCPChatSummaryManager.UPDATE_MESSAGE:
                Log.d(TAG, "onSummaryUpdated - We got a message update");
                showMessageBody();
                showMessageDate();
                showUnreadCount();
                break;
            case FCPChatSummaryManager.UPDATE_NICKNAME:
                //TODO
                break;
            default:
                Log.d(TAG, "onSummaryUpdated - No recognisable update");
        }

    }

    private void showUnreadCount() {
        //set view visible if the unread message count is greater than 0
        unreadMessageCount.setVisibility(chatSummary.getUnreadMessages() > 0 ? View.VISIBLE : View.INVISIBLE);
        unreadCountFrame.setVisibility(chatSummary.getUnreadMessages() > 0 ? View.VISIBLE : View.INVISIBLE);

        YoYo.with(Techniques.FlipOutX).duration(ANIMATION_DURATION/2).withListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                setUnreadCountWithLimit();
                YoYo.with(Techniques.FlipInX).duration(ANIMATION_DURATION/2).playOn(unreadMessageCount);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        }).playOn(unreadMessageCount);


    }

    private void showMessageDate() {
        YoYo.with(Techniques.FadeOut).duration(ANIMATION_DURATION/2).withListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                lastMessageDate.setText(getDateFormat(chatSummary.getLastMessageDate()));
                YoYo.with(Techniques.FadeIn).duration(ANIMATION_DURATION/2).playOn(lastMessageDate);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        }).playOn(lastMessageDate);
    }

    private void showMessageBody() {
        YoYo.with(Techniques.FadeOut).duration(ANIMATION_DURATION/2).withListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                lastMessage.setText(chatSummary.getLastMessageBody());
                YoYo.with(Techniques.FadeIn).duration(ANIMATION_DURATION/2).playOn(lastMessage);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        }).playOn(lastMessage);

    }
}
