package com.fcpware.languagelink.UI.friends;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.fcpware.languagelink.ORM.BuddyModel;
import com.fcpware.languagelink.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by Eoin Fogarty on 28/08/2014.
 *
 * This class is for displaying each row in your buddy list
 * It will also display the section headers
 */

public class FriendsListAdapter extends BaseAdapter implements StickyListHeadersAdapter,SectionIndexer {

    private static final String TAG ="FriendsListAdapter";

    private Context context;
    private ArrayList<BuddyModel> buddies;

    private ArrayList<Integer> friendStates = new ArrayList<Integer>();
    private int[] mSectionIndices;

    public FriendsListAdapter(Context context, Collection<BuddyModel> buddies){
        this.context = context;

        setHeaderInfo(new ArrayList<BuddyModel>(buddies));
    }

    @Override
    public int getCount() {
        return buddies.size();
    }

    @Override
    public Object getItem(int position) {
        return buddies.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        BuddyModel buddy = buddies.get(position);
        FriendListView friendListView;


        if(convertView==null){
            friendListView = new FriendListView(context);
        } else {
            friendListView = (FriendListView) convertView;
        }

        friendListView.setBuddy(buddy);


        return friendListView;
    }

    // sticky header stuff

    public void set(Collection<BuddyModel> buddies) {
        Log.d(TAG,"set is being called");
        setHeaderInfo(new ArrayList<BuddyModel>(buddies));
        notifyDataSetChanged();
    }

    private void setHeaderInfo(ArrayList<BuddyModel> buddies) {
        this.buddies=buddies;
        friendStates.clear();

        //split my buddies into 3 groups
        ArrayList<BuddyModel> requests = extractFriendRequestsFrom(this.buddies);
        ArrayList<BuddyModel> pending = extractPendingRequestsFrom(this.buddies);

        mSectionIndices = getSectionIndices(requests, this.buddies, pending);

        requests.addAll(this.buddies);
        requests.addAll(pending);

        this.buddies = requests;

        Log.d(TAG, Arrays.toString(mSectionIndices));
        for(int state: friendStates){
            Log.d(TAG, context.getResources().getString(state));
        }
    }

    //TODO horrible
    private int[] getSectionIndices(ArrayList<BuddyModel> requests, Collection<BuddyModel> friends, ArrayList<BuddyModel> pending) {
        ArrayList<Integer> sectionIndices = new ArrayList<Integer>();

        int sectionHeaderMarker = 0;

        if(requests.size()>0){ //if you have friend requests
            friendStates.add(R.string.requests);
            sectionIndices.add(sectionHeaderMarker);
            sectionHeaderMarker+=requests.size();
        }
        if(friends.size()>0){ //if you have friends
            friendStates.add(R.string.friends);
            sectionIndices.add(sectionHeaderMarker);
            sectionHeaderMarker+=friends.size();
        }
        if(pending.size()>0){ // if you have friend requests pending
            friendStates.add(R.string.pending);
            sectionIndices.add(sectionHeaderMarker);
        }

        int[] sections = new int[sectionIndices.size()];
        for (int i = 0; i < sectionIndices.size(); i++) {
            sections[i] = sectionIndices.get(i);
        }

        return sections;
    }

    private ArrayList<BuddyModel> extractPendingRequestsFrom(ArrayList<BuddyModel> friends) {
        ArrayList<BuddyModel> copy = new ArrayList<BuddyModel>(friends);
        ArrayList<BuddyModel> results = new ArrayList<BuddyModel>();

        for(BuddyModel buddy : copy){
            if(buddy.getFriendStatus()==BuddyModel.PENDING_REQUEST)
            {
                results.add(buddy);
                buddies.remove(buddy);
                Log.d(TAG, "extractPendingRequestsFrom - extracting pending");
            }
        }

        return results;
    }
    //TODO can sort out in one methos, use a switch in a loop
    private ArrayList<BuddyModel> extractFriendRequestsFrom(ArrayList<BuddyModel> friends) {
        ArrayList<BuddyModel> copy = new ArrayList<BuddyModel>(friends);
        ArrayList<BuddyModel> results = new ArrayList<BuddyModel>();

        for(BuddyModel buddy : copy){
            //TODO some of these sates are not needed
            if(buddy.getFriendStatus()==BuddyModel.FRIEND_REQUEST)
            {
                results.add(buddy);
                buddies.remove(buddy);
                Log.d(TAG, "extractFriendRequestsFrom - extracting request");
            }
        }
        return results;
    }


    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderViewHolder holder;
        if (convertView == null) {
            holder = new HeaderViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.view_friends_list_category, parent, false);
            holder.text = (TextView) convertView.findViewById(R.id.learnTv);
            holder.chevron = (ImageView) convertView.findViewById(R.id.chevron);
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }

        //TODO get friend state based on if the position is less than section index
        if(position >= friendStates.size()){
            position = friendStates.size()-1;
        }
        String headerText = "" + context.getResources().getString(friendStates.get(position));
        holder.text.setText(headerText);

        return convertView;
    }


    @Override
    public Object[] getSections() {
        Log.d(TAG,"getSections - " + Arrays.toString(friendStates.toArray()));
        return friendStates.toArray();
    }

    @Override
    public int getPositionForSection(int section) {
        if (mSectionIndices.length == 0) {
            return 0;
        }

        if (section >= mSectionIndices.length) {
            section = mSectionIndices.length - 1;
        } else if (section < 0) {
            section = 0;
        }
        return mSectionIndices[section];
    }

    @Override
    public int getSectionForPosition(int position) {
        for (int i = 0; i < mSectionIndices.length; i++) {
            if (position < mSectionIndices[i]) {
                return i;
            }
        }
        return mSectionIndices.length - 1;
    }


    @Override
    public long getHeaderId(int position) {
        int headerId = 0;
        for(int i = 0; i < mSectionIndices.length; i++){
            if(i+1>=mSectionIndices.length){
                return mSectionIndices[i];
            }
            if(position<mSectionIndices[i+1]){
                return mSectionIndices[i];
            }
        }
        return mSectionIndices[headerId];
    }

    class HeaderViewHolder {
        TextView text;
        ImageView chevron;
    }
}