package com.fcpware.languagelink.UI;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.fcpware.languagelink.BaseActivity;
import com.fcpware.languagelink.R;
import com.fcpware.languagelink.XMPP.XMPPClient;
import com.fcpware.languagelink.events.ConnectionStatusQueryEvent;
import com.fcpware.languagelink.events.LoggedInEvent;
import com.fcpware.languagelink.managers.SharedPreferencesManager;
import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;

import java.security.MessageDigest;
import java.util.Arrays;

/**
 * Created by eoinfogarty on 27/08/2014.
 *
 * This activity checks if you are logged in into facebook and the xmpp server
 * if you are not logged into facebook then the facebook button is displayed
 * if you are not logged into the xmpp server then it will try to connect
 * after you are connected and logged in a new intent will send you to the MainActivty
 */

public class SplashActivity extends BaseActivity {

    private static final String TAG = "SplashActivity";

    private XMPPClient mXMPPClient;
    private SharedPreferencesManager userLoginDetails;

    private LoginButton loginButton;

    //Facebook session management
    private boolean isResumed = false;
    private UiLifecycleHelper uiHelper;
    private Session.StatusCallback callback =
            new Session.StatusCallback() {
                @Override
                public void call(Session session,
                                 SessionState state, Exception exception) {
                    onSessionStateChange(session, state, exception);
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        userLoginDetails = new SharedPreferencesManager(this);
        mXMPPClient = XMPPClient.getInstance(this);

        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList("public_profile", "email"));

        Log.d(TAG, "onCreate - is Facebook logged in  : " + isFacebookLoggedIn());
        uiHelper = new UiLifecycleHelper(this, callback);
        uiHelper.onCreate(savedInstanceState);

        printHashKey();

        if(isFacebookLoggedIn()){
            hideLogin();
        } else {
            showLogin();
        }
    }

    public void showLogin(){
        loginButton.setVisibility(View.VISIBLE);
    }

    public void hideLogin() {
        loginButton.setVisibility(View.INVISIBLE);
    }

    public void printHashKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.redstar.language", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i(TAG, "printHashKey() Hash Key: " + hashKey);
            }
        } catch (Exception e) {
            Log.e(TAG, "printHashKey()", e);
        }
    }


    private void startMainActivity() {
        Log.d(TAG, "Starting Main Activity");
        Intent i = new Intent(this, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        finish();
    }

    @Subscribe
    public void onLoggedIn(LoggedInEvent event) {
        Log.d(TAG, "onLoggedIn - service connected and logged in");
        startMainActivity();
    }

    @Produce
    public ConnectionStatusQueryEvent connectionStatusQuery() {
        // Provide an initial value for location based on the last known position.
        return new ConnectionStatusQueryEvent();
    }

    @Override
    public void onResume() {
        super.onResume();
        uiHelper.onResume();
        isResumed = true;
        Log.d(TAG, "onResume - isResumed set to true");

    }

    private void connectWithFacebook() {
        Log.d(TAG, "connectWithFacebook - Getting facebook details");
        final Session session = Session.getActiveSession();
        // Make an API call to get user data and define a
        // new callback to handle the response.
        Request request = Request.newMeRequest(session,
                new Request.GraphUserCallback() {
                    @Override
                    public void onCompleted(GraphUser user, Response response) {
                        // If the response is successful
                        if (session == Session.getActiveSession()) {
                            if (user != null) {
                                mXMPPClient.connectWithFacebook(user);
                            }
                        }
                        if (response.getError() != null) {
                            // Handle errors, will do so later.
                        }
                    }
                });
        request.executeAsync();
    }

    public boolean isFacebookLoggedIn() {
        Session session = Session.getActiveSession();
        return (session != null && session.isOpened());
    }

    @Override
    public void onPause() {
        super.onPause();

        uiHelper.onPause();
        isResumed = false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data);
    }

    private void onSessionStateChange(Session session, SessionState state, Exception exception) {
        // Only make changes if the activity is visible
        if (isResumed) {
            if (isFacebookLoggedIn()) {
                hideLogin();
                if(!userLoginDetails.hasLoggedIn()){
                    connectWithFacebook();
                }
            } else {
                showLogin();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }
}
