package com.fcpware.languagelink.UI.search;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fcpware.languagelink.FCPConnection;
import com.fcpware.languagelink.R;
import com.fcpware.languagelink.events.BusProvider;
import com.fcpware.languagelink.events.NewLanguage;
import com.fcpware.languagelink.managers.SharedPreferencesManager;
import com.fcpware.languagelink.models.FCPLanguageModel;
import com.fcpware.languagelink.models.FCPUserModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Lets the user pick a language from a dialog to add to their own list of languages
 */
public class FilterHeaderView extends LinearLayout implements View.OnClickListener {

    private FragmentManager fragmentManager;

    private int actionToPerform; //eg language to share, language to learn

    public FilterHeaderView(Context context) {
        super(context);
        init(context);
    }

    public FilterHeaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public FilterHeaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        View root = LayoutInflater.from(context).inflate(R.layout.view_header_add_language, this);

        fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();

        TextView addLanguage = (TextView) root.findViewById(R.id.addLanguage);
        ImageButton add = (ImageButton) root.findViewById(R.id.add);

        addLanguage.setOnClickListener(this);
        add.setOnClickListener(this);
    }

    public void setType(int actionToPerform) {
        this.actionToPerform = actionToPerform;
    }

    @Override
    public void onClick(View v) {
        DialogFragment addLanguageDialog = new AddLanguageDialog(actionToPerform);
        addLanguageDialog.show(fragmentManager, "AddLanguage");
    }

    public class AddLanguageDialog extends DialogFragment implements DialogInterface.OnClickListener {

        private int action;
        private String[] allLanguages;
        private ArrayList<FCPLanguageModel> userLanguages;
        private String[] languagesUserCanChoose;

        public AddLanguageDialog(int action) {
            this.action = action;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            userLanguages = FCPUserModel.getInstance().getLanguages();
            allLanguages = getResources().getStringArray(R.array.language_list);
            languagesUserCanChoose = getPickableLanguages(allLanguages, userLanguages);


            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            int title = languagesUserCanChoose.length == 0 ? R.string.no_more_languages_left : R.string.add_a_language;
            builder.setTitle(title).setItems(languagesUserCanChoose, this);

            return builder.create();
        }

        private String[] getPickableLanguages(String[] languages, ArrayList<FCPLanguageModel> userLanguages) {

            List<String> languagesList = new ArrayList<>(Arrays.asList(languages));
            for (FCPLanguageModel userLanguage : userLanguages) {
                languagesList.remove(userLanguage.getName());
            }

            return languagesList.toArray(new String[languagesList.size()]);
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            int fluency = action == R.string.i_can_share ? FCPLanguageModel.FLUENT : FCPLanguageModel.BEGINNER;
            FCPLanguageModel language = new FCPLanguageModel(languagesUserCanChoose[which], fluency);
            userLanguages.add(language);

            //TODO saving evertime is not efficient so should just save one
            FCPUserModel.getInstance().save(FCPConnection.getInstance().getConnection(), getActivity());
            //TODO move into FCPUserModel
            SharedPreferencesManager preferencesManager = new SharedPreferencesManager(getActivity());
            preferencesManager.saveLanguageFile(userLanguages);


            BusProvider.getInstance().post(new NewLanguage(language));
        }
    }
}
