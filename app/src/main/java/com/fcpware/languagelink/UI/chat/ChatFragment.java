package com.fcpware.languagelink.UI.chat;

import android.app.Fragment;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.fcpware.languagelink.FCPConnection;
import com.fcpware.languagelink.ORM.BuddyModel;
import com.fcpware.languagelink.ORM.SavableMessage;
import com.fcpware.languagelink.R;
import com.fcpware.languagelink.events.BusProvider;
import com.fcpware.languagelink.events.EmojiSelectedEvent;
import com.fcpware.languagelink.events.NewChatEvent;
import com.fcpware.languagelink.managers.FCPBuddyManager;
import com.fcpware.languagelink.managers.FCPChatManager;
import com.fcpware.languagelink.models.FCPUserModel;
import com.squareup.otto.Subscribe;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smackx.chatstates.ChatState;
import org.jivesoftware.smackx.chatstates.ChatStateListener;
import org.jivesoftware.smackx.chatstates.ChatStateManager;
import org.jxmpp.util.XmppStringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ChatFragment extends Fragment implements ChatMessageListener, ChatStateListener, View.OnClickListener {

    private static final String TAG = "ChatFragment";

    public static final int COMPOSING_TIMEOUT = 3000;

    private List<SavableMessage> messages = new ArrayList<SavableMessage>();
    private ChatStateManager chatStateManager;
    private FCPChatManager fcpChatManager;
    private FCPConnection fcpConnection;
    private FCPUserModel userModel;
    private BuddyModel buddy;
    private Chat chat;
    private EditText message;
    private ListView listview;
    private ChatAdapter chatAdapter;
    private ComposingView composingView;
    private Handler composingHandler;

    public ChatFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        BusProvider.getInstance().register(this);
        View rootView = inflater.inflate(R.layout.fragment_chat, container, false);

        fcpConnection = FCPConnection.getInstance();
        chatStateManager = ChatStateManager.getInstance(fcpConnection.getConnection());
        FCPBuddyManager fcpBuddyManager = FCPBuddyManager.getInstance(getActivity());
        fcpChatManager = FCPChatManager.getInstance(getActivity());

        buddy = fcpBuddyManager.getBuddy(getActivity().getIntent().getStringExtra("friendJabberId"));
        userModel = FCPUserModel.getInstance();

        //Set a listener to send a chat text message
        message = (EditText) rootView.findViewById(R.id.chatET);
        message.addTextChangedListener(composingManager);

        composingView = new ComposingView(getActivity());
        composingView.setVisibility(View.GONE);
        composingHandler = new Handler();

        listview = (ListView) rootView.findViewById(R.id.listMessages);
        listview.addFooterView(composingView);

        ImageButton send = (ImageButton) rootView.findViewById(R.id.sendBtn);
        send.setOnClickListener(this);

        messages = SavableMessage.find(SavableMessage.class,
                "(sent_to = ? and received_from = ?) or (sent_to = ? and received_from = ?)",
                buddy.getJid(), userModel.getJabberId(), userModel.getJabberId(), buddy.getJid());

        chatAdapter = new ChatAdapter(getActivity(), messages, buddy.getJid());
        listview.setAdapter(chatAdapter);
        listview.setSelection(chatAdapter.getCount());

        chat = fcpChatManager.getChat(buddy.getJid());
        chat.removeMessageListener(fcpChatManager);
        chat.addMessageListener(this);
        setChatState(ChatState.active);

        ((ActionBarActivity) getActivity()).getSupportActionBar().setTitle(buddy.getNickName());

        return rootView;
    }

    @Override
    public void onStop() {
        super.onStop();
        finishChatWithSate(ChatState.inactive);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        finishChatWithSate(ChatState.gone);
    }

    private void finishChatWithSate(ChatState state) {
        composingHandler.removeCallbacksAndMessages(null);
        chat.addMessageListener(fcpChatManager);
        chat.removeMessageListener(this);
        setChatState(state);
    }

    private void setChatState(ChatState state) {
        try {
            //todo only send chat states if your firends is online
            chatStateManager.setCurrentState(state, chat);
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void stateChanged(Chat chat, ChatState chatState) {
        Log.d("CHAT", "chat state changed :" + chat.getParticipant() + " " + chatState.toString());
        //TODO make sure I am only getting chat updates for the person I am talking to
        switch (chatState) {
            case active:
               // setActionBarSubtitle(getResources().getString(R.string.active));
                break;
            case composing:
                setComposingVisibility(View.VISIBLE);
                break;
            case paused:
                setComposingVisibility(View.INVISIBLE);
                break;
            case inactive:
               // setActionBarSubtitle(getResources().getString(R.string.inactive));
                break;
            case gone:
                //setActionBarSubtitle(getResources().getString(R.string.gone));
                break;
        }
    }

    private void setComposingVisibility(final int visible) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                composingView.setVisibility(visible);
            }
        });
    }

    @Override
    public void processMessage(Chat chat, Message message) {
        Log.d(TAG, "Message processed : " + chat.getParticipant() + " " + message.getBody());
        if (message.getBody() != null) {

            addNewMessage(message.getFrom(), message, message.getFrom());
        }
    }

    public void addNewMessage(String from, Message message, String chatPartner) {
        Log.d(TAG, " Message from : " + message.getFrom() + " to : " + message.getTo());
        Log.d(TAG, "Chat partner : " + chatPartner);
        SavableMessage messageORM = fcpChatManager.saveMessage(message,from,true, XmppStringUtils.parseBareJid(chatPartner));
        updateListView(messageORM);
    }

    private void updateListView(final SavableMessage message) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                messages.add(message);
                chatAdapter.updateMessages(messages);
                chatAdapter.notifyDataSetChanged();
                listview.setSelection(chatAdapter.getCount() - 1); // move to the bottom of the list
            }
        });
    }

    @Subscribe
    public void newChat(NewChatEvent event) {
        Log.d(TAG, "newChat - username : " + event.getParticipant());

        if (buddy.getJid().equals(XmppStringUtils.parseBareJid(event.getParticipant()))) {
            Log.d(TAG, "newChat - chat is matched");
            chat = event.getNewChat();
            chat.addMessageListener(this);
            chat.removeMessageListener(fcpChatManager);
        }
    }

    @Subscribe
    public void emojiSelected(EmojiSelectedEvent event){
        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append("["+event.getName()+"]");
        builder.setSpan(new ImageSpan(event.getEmoji()),
                                    builder.length() - ("["+event.getName()+"]").length(),
                                    builder.length(),
                                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        //remove last word we were typing
        int lastSpace = String.valueOf(message.getText()).lastIndexOf(" ")+1;

        String text = lastSpace == 0 ? "" : String.valueOf(message.getText().subSequence(0, lastSpace));
        message.setText(text);
        message.append(builder);
    }


    //implements send button
    @Override
    public void onClick(View v) {
        String text = message.getText().toString();
        //TODO move to client
        Message msg = new Message(chat.getParticipant(), Message.Type.chat);
        msg.setBody(text);

        if (fcpConnection.isConnected() && !message.getText().toString().equals("")) {

            addNewMessage(userModel.getJabberId(), msg, msg.getTo());

            try {
                message.setText("");
                composingHandler.removeCallbacksAndMessages(null);
                setChatState(ChatState.active);
                chat.sendMessage(text); //todo maybe this chat is the problem?
            } catch (SmackException.NotConnectedException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.no_connection), Toast.LENGTH_SHORT).show();
        }
    }

    private ArrayList<String> listAssetFiles(CharSequence input) {

        ArrayList<String> filesToUse = new ArrayList<>();

        try {
            AssetManager assetManager = getActivity().getAssets();
            String[] files = assetManager.list("emoji");

            List<String> it= Arrays.asList(files);
            for(String file: it){
                if(file.startsWith(String.valueOf(input))){
                    filesToUse.add(file);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return filesToUse;
    }

        private TextWatcher composingManager = new TextWatcher() {

        Runnable userStoppedTyping = new Runnable() {
            @Override
            public void run() {
                setChatState(ChatState.paused);
            }
        };

        public void afterTextChanged(Editable input) {
            composingHandler.removeCallbacksAndMessages(null); //make sure we have only one callback running
            composingHandler.postDelayed(userStoppedTyping, COMPOSING_TIMEOUT);
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            setChatState(ChatState.composing);

            //Log.d(TAG, " on " + s.charAt(count));
        }
    };
}