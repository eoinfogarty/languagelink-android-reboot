package com.fcpware.languagelink.UI.friends;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.fcpware.languagelink.Constants;
import com.fcpware.languagelink.ORM.BuddyModel;
import com.fcpware.languagelink.R;
import com.fcpware.languagelink.events.BuddyUpdateEvent;
import com.fcpware.languagelink.events.BusProvider;
import com.fcpware.languagelink.managers.FCPBuddyManager;
import com.makeramen.RoundedImageView;
import com.nineoldandroids.animation.Animator;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import org.jivesoftware.smack.packet.Presence;
import org.jxmpp.util.XmppStringUtils;

/**
 * This view represent a friends seen on the friends list
 *
 * The view maps a buddy object.
 * It will listen for:
 * avatar updates
 * presence updates
 * nickname updates
 * passes the avatars dimensions for the profile activity to share
 */
public class FriendListView extends LinearLayout implements  View.OnClickListener{

    private static final String TAG = "FriendListView";

    private static final int ANIMATION_DURATION = 2000;

    private FCPBuddyManager fcpBuddyManager;
    private BuddyModel buddy;

    private RoundedImageView avatar;
    private ImageView availability;
    private TextView nickname;

    private Context context;
    private Class intentClass;

    public FriendListView(Context context) {
        super(context);
        init(context);
    }

    public FriendListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public FriendListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {
        BusProvider.getInstance().register(this); //otto
        this.context = context;

        intentClass = FriendProfileActivity.class;

        View root = LayoutInflater.from(context).inflate(R.layout.view_friends_list_friend, this, true);

        fcpBuddyManager = FCPBuddyManager.getInstance(context);

        availability = (ImageView) root.findViewById(R.id.availability);
        avatar = (RoundedImageView) root.findViewById(R.id.avatar);
        nickname = (TextView) root.findViewById(R.id.nickname);

        setOnClickListener(this);
    }

    public void setPresenceVisibility(int visibility){
        availability.setVisibility(visibility);
    }

    //no animations
    public void setBuddy(BuddyModel buddy){
        this.buddy = buddy;

        String avatarUri = fcpBuddyManager.getAvatarUriString(buddy.getJid());
        Picasso.with(context).load(avatarUri).noFade().into(avatar);

        availability.setImageDrawable(getAvailabilityIcon());
        nickname.setText(buddy.getNickName());
    }

    @Override
    public void onClick(View v) {
        // todo pass image into next activity so we dont have to download image
        // Interesting data to pass across are the thumbnail size/location, the
        // orientation (to avoid returning back to an obsolete configuration if
        // the device rotates again in the meantime)
        int[] screenLocation = new int[2];
        avatar.getLocationOnScreen(screenLocation);
        int orientation = getResources().getConfiguration().orientation;

        Intent subActivity = new Intent(context, intentClass);

        subActivity.
                putExtra(Constants.PACKAGE + ".orientation", orientation).
                putExtra(Constants.PACKAGE  + ".left", screenLocation[0]).
                putExtra(Constants.PACKAGE  + ".top", screenLocation[1]).
                putExtra(Constants.PACKAGE  + ".width", avatar.getWidth()).
                putExtra(Constants.PACKAGE  + ".height", avatar.getHeight());

        subActivity.putExtra("resultEmailAddress", XmppStringUtils.parseLocalpart(buddy.getJid()));

        //subActivity.putExtra(Constants.PACKAGE + ".bitmap", bitmap);

        ((Activity)context).overridePendingTransition(0, 0);

        context.startActivity(subActivity);
    }

    @Subscribe
    public void buddyUpdated(BuddyUpdateEvent buddyUpdate) {

        if(buddyUpdate.getJid().equals(buddy.getJid())){
            switch (buddyUpdate.getTag()){     //with animations
                case FCPBuddyManager.BUDDY_UPDATE_NICK:
                    Log.d(TAG, "handleUpdate -  nickname");
                    //TODO
                    break;
                case FCPBuddyManager.BUDDY_UPDATE_AVATAR:
                    showNewAvatar(buddyUpdate.getAvatarUri());
                    break;
                case FCPBuddyManager.BUDDY_UPDATE_PRESENCE:
                    changePresence(buddyUpdate.getPresence());
                    break;
                default:
                    Log.d(TAG, "handleUpdate - cant handle this update");
            }
        }
    }

    private void changePresence(Presence presence) {

        YoYo.with(Techniques.Flash).duration(ANIMATION_DURATION/2).withListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) { }

            @Override
            public void onAnimationEnd(Animator animator) {
                availability.setImageDrawable(getAvailabilityIcon());
            }

            @Override
            public void onAnimationCancel(Animator animator) { }

            @Override
            public void onAnimationRepeat(Animator animator) { }
        }).playOn(availability);


    }

    private Drawable getAvailabilityIcon() {

        //if it is a pending request we don't want the user to be able to see availability
        if(buddy.getFriendStatus()==BuddyModel.PENDING_REQUEST){
            return context.getResources().getDrawable(R.drawable.ic_offline_indicator);
        }

        int iconResource = buddy.getPresence().getType() == Presence.Type.available ? R.drawable.ic_online_indicator
                                                                                    : R.drawable.ic_offline_indicator;
        return context.getResources().getDrawable(iconResource);

    }

    private void showNewAvatar(final String avatarUri) {

        YoYo.with(Techniques.TakingOff).duration(ANIMATION_DURATION/2).withListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) { }

            @Override
            public void onAnimationEnd(Animator animator) {
                Picasso.with(context).load(avatarUri).noFade().skipMemoryCache().into(avatar);
                YoYo.with(Techniques.Landing).duration(ANIMATION_DURATION/2).playOn(avatar);
            }

            @Override
            public void onAnimationCancel(Animator animator) { }

            @Override
            public void onAnimationRepeat(Animator animator) { }

        }).playOn(avatar);
    }

    public void setIntentClass(Class intentClass) {
        this.intentClass = intentClass;
    }
}
