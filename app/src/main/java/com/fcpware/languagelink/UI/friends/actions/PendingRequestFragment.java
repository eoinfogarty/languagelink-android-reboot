package com.fcpware.languagelink.UI.friends.actions;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.fcpware.languagelink.R;
import com.fcpware.languagelink.UI.friends.FriendProfileActivity;
import com.fcpware.languagelink.events.BuddySubscriptionChange;
import com.fcpware.languagelink.events.BusProvider;
import com.squareup.otto.Produce;

/**
 * This fragment represents the actions you have a friend request pending
 *
 * There are 2 options available at the moment, accept and reject
 *
 * TODO remove the reject option as its buggy and doesnt work properly
 */

public class PendingRequestFragment extends Fragment {

    private String jabberID;

    public static PendingRequestFragment newInstance(String jid){

        Bundle args = new Bundle();
        args.putString("jid", jid);

        PendingRequestFragment frag = new PendingRequestFragment();
        frag.setArguments(args);
        return frag;
    }

    public PendingRequestFragment(){
    }

    @Produce
    public BuddySubscriptionChange subscriptionChange(int buddyStatus) {
        // Provide an initial value for location based on the last known position.
        return new BuddySubscriptionChange(jabberID, buddyStatus);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.jabberID = getArguments().getString("jid");


        View root = inflater.inflate(R.layout.fragment_person_profile_pending, null);
        TextView pending = (TextView) root.findViewById(R.id.pendingButton);
        pending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), R.string.friend_request_pending, Toast.LENGTH_SHORT).show();
            }
        });

        ImageButton cancel = (ImageButton) root.findViewById(R.id.cancelButton);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                try {
//                    Toast.makeText(getActivity(), R.string.friend_request_cancelled, Toast.LENGTH_SHORT).show();
//
//                    Presence unsubscribe = new Presence(Presence.Type.unsubscribe);
//                    unsubscribe.setTo(jabberID);
//                    connection.sendPacket(unsubscribe);
//                    Presence unsubscribed = new Presence(Presence.Type.unsubscribed);
//                    unsubscribed.setTo(jabberID);
//                    connection.sendPacket(unsubscribed);
//
//                    fcpBuddyManager.removeBuddy(jabberID);
//
//                    getActivity().finish();
//                    //TODO  roster  make sure they are gone, buddy list too
//                } catch (SmackException.NotConnectedException e) {
//                    e.printStackTrace();
//                }
                BusProvider.getInstance().post(subscriptionChange(FriendProfileActivity.REMOVE));
            }
        });
        return root;
    }


}
