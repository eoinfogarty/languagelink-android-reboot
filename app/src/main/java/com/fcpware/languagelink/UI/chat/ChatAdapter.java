package com.fcpware.languagelink.UI.chat;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.fcpware.languagelink.ORM.SavableMessage;
import com.fcpware.languagelink.R;
import com.fcpware.languagelink.events.BusProvider;
import com.fcpware.languagelink.events.NewMessageEvent;
import com.fcpware.languagelink.managers.FCPAvatarManager;
import com.fcpware.languagelink.models.FCPUserModel;
import com.makeramen.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

/**
 * inflates message view based on type, sent, recieved, pictures , etc
 *
 * Created by eoinfogarty on 29/08/2014.
 */
public class ChatAdapter extends BaseAdapter {

    public static final String TAG = "ChatAdapter";

    public static final int TYPE_SENT = 0;
    public static final int TYPE_RECEIVED = 1;
    private static final int TYPE_MAX_COUNT = TYPE_RECEIVED + 1;

    private List<SavableMessage> messages;
    private File friendsAvatar;
    private Context context;

    public ChatAdapter(Context context, List<SavableMessage> messages, String JID) {
        this.messages = messages;
        this.context = context;
        friendsAvatar= FCPAvatarManager.getInstance(context).getAvatar(JID);
    }

    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public Object getItem(int position) {
        return messages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {

        String from = messages.get(position).getReceivedFrom();

        //todo from!=null
        if(from!=null&&!from.equals(FCPUserModel.getInstance().getJabberId())){
           //|| messages.get(position).getType().equals("composing")){ //TODO dont need composing?
            return TYPE_RECEIVED;
        } else {
            return TYPE_SENT;
        }
    }

    @Override
    public int getViewTypeCount() {
        return TYPE_MAX_COUNT;
    }

    public void updateMessages(List<SavableMessage> messages) {
        this.messages = messages;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int type = getItemViewType(position);
        ViewHolder viewHolder;

        if(convertView==null){
            viewHolder = new ViewHolder();

            switch (type) {
                case TYPE_RECEIVED:
                    convertView = LayoutInflater.from(context).inflate(R.layout.view_chat_received_message, null);
                    viewHolder.profilePic = (RoundedImageView) convertView.findViewById(R.id.profilePic);
                    Picasso.with(context).load(friendsAvatar).skipMemoryCache().into(viewHolder.profilePic);
                    break;
                case TYPE_SENT:
                    convertView = LayoutInflater.from(context).inflate(R.layout.view_chat_sent_message, null);
                    break;
            }


            viewHolder.message = (TextView) convertView.findViewById(R.id.messageTv);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        viewHolder.message.setText(messages.get(position).getBody());

        //it should be noted that if a message is too far off screen then it will not be counted as read!
        if(!messages.get(position).getRead()){
            Log.d(TAG, "Setting a message to read");
            messages.get(position).setRead(true);
            messages.get(position).save();
            //if it is an unread message then we must of received it
            BusProvider.getInstance().post(new NewMessageEvent(messages.get(position).getReceivedFrom()));
        }

        return convertView;
    }

    static class ViewHolder {
        TextView message;
        RoundedImageView profilePic;
    }
}
