package com.fcpware.languagelink.UI.friends.actions;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.fcpware.languagelink.R;
import com.fcpware.languagelink.UI.friends.FriendProfileActivity;
import com.fcpware.languagelink.events.BuddySubscriptionChange;
import com.fcpware.languagelink.events.BusProvider;
import com.fcpware.languagelink.managers.FCPRosterManager;
import com.squareup.otto.Produce;

/**
 * This fragment represents the actions when you want to reject/cancel/remove a buddy
 *
 * There are 2 options available at the moment, confirm reject/cancel/remove or cancel
 *
 */
public class RemoveFragment extends Fragment {

    private String jabberID;
    private FCPRosterManager fcpRosterManager;

    public static RemoveFragment newInstance(String jid){
        Bundle args = new Bundle();
        args.putString("jid", jid);

        RemoveFragment frag = new RemoveFragment();
        frag.setArguments(args);
        return frag;
    }

    public RemoveFragment(){
    }

    @Produce
    public BuddySubscriptionChange subscriptionChange(int buddyStatus) {
        // Provide an initial value for location based on the last known position.
        return new BuddySubscriptionChange(jabberID, buddyStatus);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.jabberID = getArguments().getString("jid");
        this.fcpRosterManager = FCPRosterManager.getInstance(getActivity());

        View root = inflater.inflate(R.layout.fragment_person_profile_remove, null);
        ImageButton confirm = (ImageButton) root.findViewById(R.id.confirmButton);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                   Toast.makeText(getActivity(), R.string.friend_deleted, Toast.LENGTH_SHORT).show();
                    //todo might be of no use

                    getActivity().finish();
                    fcpRosterManager.removeBuddy(jabberID);
            }
        });

        ImageButton cancel = (ImageButton) root.findViewById(R.id.cancelButton);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //cancel and go back to the original status
//                try {
//                    Toast.makeText(getActivity(), R.string.friend_request_rejected, Toast.LENGTH_SHORT).show();
//                    //todo might be of no use
//                    Presence unsubscribe = new Presence(Presence.Type.unsubscribe);
//                    unsubscribe.setTo(jabberID);
//                    connection.sendPacket(unsubscribe);
//
//
//                    Presence unsubscribed = new Presence(Presence.Type.unsubscribed);
//                    unsubscribed.setTo(jabberID);
//                    connection.sendPacket(unsubscribed);
//                    getActivity().finish();
//                    fcpBuddyManager.removeBuddy(jabberID);
//                } catch (SmackException.NotConnectedException e) {
//                    e.printStackTrace();
//                }
                BusProvider.getInstance().post(subscriptionChange(FriendProfileActivity.CANCEL));
            }
        });
        return root;
    }
}