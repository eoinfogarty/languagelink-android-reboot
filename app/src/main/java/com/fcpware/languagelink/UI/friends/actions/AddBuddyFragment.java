package com.fcpware.languagelink.UI.friends.actions;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.fcpware.languagelink.FCPConnection;
import com.fcpware.languagelink.R;
import com.fcpware.languagelink.UI.friends.FriendProfileActivity;
import com.fcpware.languagelink.events.BuddySubscriptionChange;
import com.fcpware.languagelink.events.BusProvider;
import com.squareup.otto.Produce;

import org.jivesoftware.smack.packet.Presence;

/**
 * This fragment represents the actions you can perform when somebody is not your friend
 *
 * There is only one option, add
 */

public class AddBuddyFragment extends Fragment {

    private FCPConnection fcpConnection;
    private String jabberID;

    public static AddBuddyFragment newInstance(String jid){
        Bundle args = new Bundle();
        args.putString("jid", jid);

        AddBuddyFragment frag = new AddBuddyFragment();
        frag.setArguments(args);
        return frag;
    }

    public AddBuddyFragment(){
    }

    @Produce
    public BuddySubscriptionChange subscriptionChange() {
        // Provide an initial value for location based on the last known position.
        return new BuddySubscriptionChange(jabberID, FriendProfileActivity.PENDING_REQUEST);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.jabberID = getArguments().getString("jid", "");
        fcpConnection = FCPConnection.getInstance();
        View root = inflater.inflate(R.layout.fragment_person_profile_add, null);
        ImageButton add = (ImageButton) root.findViewById(R.id.addBtn);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), R.string.friend_request_sent, Toast.LENGTH_SHORT).show();

                //TODO clean , might not need so much, can move to presence manager?
                Presence subscribe = new Presence(Presence.Type.subscribe);
                subscribe.setTo(jabberID);
                fcpConnection.sendPacket(subscribe);

                // Tell anyone who is listening of this buddies new status
                BusProvider.getInstance().post(subscriptionChange());
            }
        });
        return root;
    }
}