package com.fcpware.languagelink.UI.chat;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.fcpware.languagelink.BaseActivity;
import com.fcpware.languagelink.R;
import com.fcpware.languagelink.UI.MainActivity;

public class ChatActivity extends BaseActivity {

    private static final String TAG = "ChatActivity";

    private boolean launchedFromNotification = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        Log.d(TAG, "onCreate called : from intent = " + getIntent().getExtras().containsKey("EXTRA_LAUNCHED_BY_NOTIFICATION"));


        //toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ChatFragment chatFragment = new ChatFragment();

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, chatFragment)
                    .commit();
        }
    }

    @Override
    public void onBackPressed()
    {
        launchedFromNotification = getLaunchSource();

        // Launched from notification, handle as special case
        if (launchedFromNotification)
            intentToMainActivity();
        else
            super.onBackPressed();
    }

    private boolean getLaunchSource() {
        Bundle extras = getIntent().getExtras();

        return extras.containsKey("EXTRA_LAUNCHED_BY_NOTIFICATION") && extras.getBoolean("EXTRA_LAUNCHED_BY_NOTIFICATION");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                launchedFromNotification = getLaunchSource();

                if (launchedFromNotification){
                    intentToMainActivity();
                }
                else{
                    Intent intent = new Intent(this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    NavUtils.navigateUpTo(this, intent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // Launched from notification, so we should create an intent to send them to the main activity
    private void intentToMainActivity() {

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
        finish();
    }
}

