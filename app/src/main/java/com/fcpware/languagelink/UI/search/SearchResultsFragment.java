package com.fcpware.languagelink.UI.search;


import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.fcpware.languagelink.Constants;
import com.fcpware.languagelink.R;
import com.fcpware.languagelink.UI.friends.FriendProfileActivity;
import com.fcpware.languagelink.managers.FCPBuddyManager;
import com.nhaarman.listviewanimations.appearance.simple.SwingBottomInAnimationAdapter;
import com.nineoldandroids.animation.Animator;
import com.squareup.picasso.Picasso;

import org.jivesoftware.smackx.search.ReportedData;
import org.jxmpp.util.XmppStringUtils;

import java.util.List;

/**
 * displays the results of a searchManager result
 */

public class SearchResultsFragment extends Fragment {

    private static final String TAG = "SearchFragment";

    private SearchResultsAdapter resultsAdapter;
    private String learn , share;

    private ProgressBar progress;
    private Button newSearch;

    public SearchResultsFragment(String learn, String share) {
        this.learn = learn;
        this.share = share;
    }

    public static SearchResultsFragment newInstance(String learn, String share){
        Log.d(TAG, "Creating a new instance of SearchFragment");
        return new SearchResultsFragment(learn, share);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_search_screen, container, false);

        ListView listView = (ListView) root.findViewById(R.id.list);

        //fancy animate in views of list
        resultsAdapter = new SearchResultsAdapter(getActivity());
        SwingBottomInAnimationAdapter animationAdapter = new SwingBottomInAnimationAdapter(resultsAdapter);
        animationAdapter.setAbsListView(listView);

        listView.setAdapter(animationAdapter);
        newSearch = (Button) root.findViewById(R.id.new_search);
        newSearch.setOnClickListener(null);
        newSearch.setVisibility(View.INVISIBLE);

        progress = (ProgressBar) root.findViewById(R.id.progress);
        progress.setVisibility(View.VISIBLE);

        new GetSearchResults().execute();

        return root;
    }

    View.OnClickListener newSearchListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ((SearchActivity)getActivity()).showFragment(SearchActivity.TAG_SEARCH_FILTERS);
        }
    };

    // thread so we don't block the ui when we are waiting for results
    private class GetSearchResults extends AsyncTask<Void, Void, Boolean> {

        private FCPUserSearch searchManager;
        private ReportedData results;

        public GetSearchResults(){
            searchManager = new FCPUserSearch(getActivity());
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                Log.d(TAG, "learn : " + learn + " share : " + share );
                results = searchManager.searchByLanguages(learn, share);
                results = filterFriendsFromResults(results);
            } catch (NullPointerException e){

                e.printStackTrace();
                return false;
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean gotResults) {

            YoYo.with(Techniques.SlideOutUp).withListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {



                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    progress.setVisibility(View.INVISIBLE);
                    newSearch.setVisibility(View.VISIBLE);
                    YoYo.with(Techniques.SlideInDown).playOn(newSearch);
                    newSearch.setOnClickListener(newSearchListener);
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            }).playOn(progress);

            if(gotResults){
                resultsAdapter.setSearchResultsList(results.getRows());
                resultsAdapter.notifyDataSetChanged();
            } else {
                //TODO display error message
            }


        }

        /**
         * this method removes all your existing friends from the reported that you received from a search result
         * @param results - unfiltered results
         * @return filteredResults
         */
        private ReportedData filterFriendsFromResults(ReportedData results) throws NullPointerException{

            FCPBuddyManager fcpBuddyManager = FCPBuddyManager.getInstance(getActivity());
            ReportedData filteredResults = new ReportedData();

            for(ReportedData.Row row : results.getRows()){
                Log.d(TAG, "filterFriendsFromResults - Result Jid : " + row.getValues("jid").get(0));
                String rowJid = row.getValues("jid").get(0);
                boolean friendInSearch = false;

                if(fcpBuddyManager.getBuddyMap().get(rowJid)!=null){
                    Log.d(TAG, "filterFriendsFromResults - your buddy is in the result set");
                    friendInSearch = true;
                }

                if(!friendInSearch){
                    filteredResults.addRow(row);
                }
            }

            return filteredResults;
        }
    }

    public class SearchResultsAdapter extends BaseAdapter {

        private static final String TAG = "SearchResultsAdapter";

        private Context context;
        private LayoutInflater inflater;

        private List searchResults;

        public SearchResultsAdapter(Context context) {
            this.context = context;

            inflater = LayoutInflater.from(this.context);
            searchResults = new ReportedData().getRows();

        }
        @Override
        public int getCount() {
            return searchResults.size();
        }

        @Override
        public Object getItem(int position) {
            return searchResults.get(searchResults.size()%position);
        }

        @Override
        public long getItemId(int position) {

            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final ViewHolder holder;

             ReportedData.Row row = (ReportedData.Row) searchResults.get(position);

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.view_search_result, null);
                holder = new ViewHolder();
                holder.name = (TextView) convertView.findViewById(R.id.nameTv);
                holder.profilePic = (ImageView) convertView.findViewById(R.id.profilePic);
                holder.intro = (TextView) convertView.findViewById(R.id.introtv);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            Log.d(TAG, "getView - result position : " + position);
            Log.d(TAG, "getView - result jid  : " + row.getValues("jid"));
            Log.d(TAG, "getView - result nickname  : " + row.getValues("nick").get(0));
            Log.d(TAG, "getView - result intro  : " + row.getValues("intro").get(0));

            String nickname = row.getValues("nick").get(0);
            String intro = row.getValues("intro").get(0);
            //TODO clean
            final String JID = XmppStringUtils.unescapeLocalpart(XmppStringUtils.parseLocalpart(row.getValues("jid").get(0)));

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //todo pass image into next activity so we dont have to download image

                    // Interesting data to pass across are the thumbnail size/location, the
                    // resourceId of the source bitmap, the picture description, and the
                    // orientation (to avoid returning back to an obsolete configuration if
                    // the device rotates again in the meantime)
                    int[] screenLocation = new int[2];
                    holder.profilePic.getLocationOnScreen(screenLocation);
                   // Bitmap bitmap = ((RoundedDrawable) holder.profilePic.getDrawable()).toBitmap();
                   // PictureData info = new PictureData(holder.profilePic.getId(), "Some description", getThumbnail(bitmap, 2));
                    Intent subActivity = new Intent(context, FriendProfileActivity.class);
                    int orientation = getResources().getConfiguration().orientation;
                    subActivity.
                            putExtra(Constants.PACKAGE  + ".orientation", orientation).
                           // putExtra(PACKAGE + ".resourceId", info.resourceId).
                            putExtra(Constants.PACKAGE  + ".left", screenLocation[0]).
                            putExtra(Constants.PACKAGE  + ".top", screenLocation[1]).
                            putExtra(Constants.PACKAGE  + ".width", holder.profilePic.getWidth()).
                            putExtra(Constants.PACKAGE  + ".height", holder.profilePic.getHeight()).
                           // putExtra(PACKAGE + ".description", info.description).
                            putExtra("resultEmailAddress",  XmppStringUtils.escapeLocalpart(JID));
                    startActivity(subActivity);

                    // Override transitions: we don't want the normal window animation in addition
                    // to our custom one
                    getActivity().overridePendingTransition(0, 0);
                }
            });

            holder.name.setText(nickname);
            holder.intro.setText(intro);
            //TODO this needs to be moved into a global constant the amazon s3 address
            Picasso.with(context).load(String.valueOf("https://s3.amazonaws.com/language-exchange/avatars/" + JID)).into(holder.profilePic);
            //TODO cache the next avatar

            return convertView;
        }

        public void setSearchResultsList(List searchResults) {
            this.searchResults = searchResults;
        }

        class ViewHolder {
            TextView name;
            ImageView profilePic;
            TextView intro;
        }
    }
}