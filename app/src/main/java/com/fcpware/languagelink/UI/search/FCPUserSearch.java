package com.fcpware.languagelink.UI.search;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.fcpware.languagelink.FCPConnection;
import com.fcpware.languagelink.R;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.search.ReportedData;
import org.jivesoftware.smackx.search.UserSearchManager;
import org.jivesoftware.smackx.xdata.Form;

/**
 * This class gets a search orm from the server and fill it in with either
 * 1. a users Jid
 * 2. a combination of a language to share and a language to learn
 *
 * It passes the results back to the calling object
 */
public class FCPUserSearch {

    private static final String TAG = "FCPUserSearch";

    private FCPConnection fcpConnection;
    private Context context;
    private UserSearchManager search;
    private Form searchForm;


    public FCPUserSearch(Context context){
        this.context = context;

        Log.d(TAG, "Creating User Search");
        fcpConnection = FCPConnection.getInstance();

        search = new UserSearchManager(fcpConnection.getConnection());
    }

    /**
     * The user searches for another user based on the languages they can share and want to learn
     *  @param share - the language the user wants to share
     * @param learn - the language the user wants to learn
     */
    public ReportedData searchByLanguages(String share, String learn){
        searchForm = getSearchForm();
        Form answerForm = searchForm.createAnswerForm();

        answerForm.setAnswer("langusercanshare",share);
        answerForm.setAnswer("languserwantstospeak",learn);

        return getSearchResults(answerForm);

    }

    /**
     * The users email is kept as their JabberId so we have to escape the email string.
     * Then we search for users with that JabberId.
     * We return true or false  if the user exists.
     *
     * @param escapedEmail
     */
    public Boolean searchByEMail(String escapedEmail){
        //TODO first check you all ready existing buddies then check online
        //TODO handle null returns
        searchForm = getSearchForm();
        Form answerForm = searchForm.createAnswerForm();
        answerForm.setAnswer("user", escapedEmail);
        ReportedData data = getSearchResults(answerForm);

        Boolean doesUserExists = !data.getRows().isEmpty();

        return doesUserExists;
    }

    private ReportedData getSearchResults(Form answerForm) {

        try {
            return search.getSearchResults(answerForm, "vjud." + fcpConnection.getConnection().getServiceName());
            //TODO change toast to red for errors
            }
            catch (SmackException.NoResponseException e) {
                e.printStackTrace();
                Toast.makeText(context,R.string.error_server,Toast.LENGTH_SHORT).show();
                return null;
            } catch (XMPPException.XMPPErrorException e) {
                e.printStackTrace();
                Toast.makeText(context,R.string.error,Toast.LENGTH_SHORT).show();
                return null;
            } catch (SmackException.NotConnectedException e) {
                e.printStackTrace();
                Toast.makeText(context,R.string.not_connected,Toast.LENGTH_SHORT).show();
                return null;
            }
    }



    public Form getSearchForm() {

        Form searchForm = null;

        try {
            searchForm = search.getSearchForm("vjud." + fcpConnection.getConnection().getServiceName());
        } catch (SmackException.NoResponseException |
                 XMPPException.XMPPErrorException |
                 SmackException.NotConnectedException |
                 NullPointerException e) {
            e.printStackTrace();
        }

        return searchForm;
    }
}
