package com.fcpware.languagelink.UI.friends;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.fcpware.languagelink.Constants;
import com.fcpware.languagelink.ORM.BuddyModel;
import com.fcpware.languagelink.R;
import com.fcpware.languagelink.UI.SharedElementActivity;
import com.fcpware.languagelink.UI.friends.actions.AddBuddyFragment;
import com.fcpware.languagelink.UI.friends.actions.BuddyActionFragment;
import com.fcpware.languagelink.UI.friends.actions.PendingRequestFragment;
import com.fcpware.languagelink.UI.friends.actions.RemoveFragment;
import com.fcpware.languagelink.UI.friends.actions.RequestResponseFragment;
import com.fcpware.languagelink.events.BuddySubscriptionChange;
import com.fcpware.languagelink.events.BusProvider;
import com.fcpware.languagelink.events.NewAvatarEvent;
import com.fcpware.languagelink.managers.FCPAvatarManager;
import com.fcpware.languagelink.managers.FCPBuddyManager;
import com.fcpware.languagelink.models.FCPLanguageModel;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import org.jxmpp.util.XmppStringUtils;

import java.io.File;
import java.util.ArrayList;

/**
 * //TODO
 */
public class FriendProfileActivity extends SharedElementActivity {

    //TODO maybe can move to a manager class
    public static final int NOT_A_FRIEND = 0;
    public static final int PENDING_REQUEST = 1;
    public static final int FRIEND_REQUEST = 2;
    public static final int FRIEND = 3;
    public static final int REMOVE = 4;
    public static final int CANCEL = 5;
    public static final int[] SHARING_CONDITIONS = {FCPLanguageModel.FLUENT};
    public static final int[] LEARNING_CONDITIONS = {FCPLanguageModel.ADVANCED, FCPLanguageModel.INTERMEDIATE, FCPLanguageModel.BEGINNER};
    private static final String TAG = "PersonProfileActivity";
    //we keep track of the persons buddy status to revert if a remove has been cancelled
    private static int personBuddyStatus = 0;
    private FragmentManager fragmentManager;
    private FrameLayout container;
    private RequestResponseFragment requestResponseFragment;
    private PendingRequestFragment pendingRequestFragment;
    private BuddyActionFragment buddyActionFragment;
    private AddBuddyFragment addBuddyFragment;
    private RemoveFragment removeFragment;

    private FCPAvatarManager fcpAvatarManager;
    private ImageView profileImage;
    private String buddyJabberId;

    private LinearLayout background;
    private TextView intro;
    private LinearLayout listHolder;
    private ListView shareListView, learnListView;

    private BuddyModel buddy;
    private Runnable sharedElementAnim;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BusProvider.getInstance().register(this);
        setContentView(R.layout.activity_person_profile);

        //todo pass just jabberid Get the results jabberId
        buddyJabberId = getIntent().getStringExtra("resultEmailAddress") + "@" + Constants.HOST;

        buddy = FCPBuddyManager.getInstance(this).getModel(buddyJabberId);

        // UI stuff
        profileImage = (ImageView) findViewById(R.id.selection_profile_pic);
        background = (LinearLayout) findViewById(R.id.background);
        // server connections
        fcpAvatarManager = FCPAvatarManager.getInstance(this);

        intro = (TextView) findViewById(R.id.intro);

        File avatar = fcpAvatarManager.getAvatar(buddyJabberId);
        if (avatar.exists()) {
            Picasso.with(this).load(fcpAvatarManager.getAvatar(buddyJabberId)).skipMemoryCache().into(profileImage);
        } else {
            String JID = XmppStringUtils.unescapeLocalpart(XmppStringUtils.parseLocalpart(buddyJabberId));
            Picasso.with(getApplicationContext()).load(String.valueOf("https://s3.amazonaws.com/language-exchange/avatars/" + JID)).into(profileImage);
        }

        intro.setText(buddy.getIntro());

        //toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setLogo(R.drawable.ic_friend_search);
        toolbar.setTitle(buddy.getNickName());
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // display the appropriate UI based on the subscription
        fragmentManager = getSupportFragmentManager();
        container = (FrameLayout) findViewById(R.id.action_container);
        requestResponseFragment = RequestResponseFragment.newInstance(buddyJabberId);
        pendingRequestFragment = PendingRequestFragment.newInstance(buddyJabberId);
        buddyActionFragment = BuddyActionFragment.newInstance(buddyJabberId);
        addBuddyFragment = AddBuddyFragment.newInstance(buddyJabberId);
        removeFragment = RemoveFragment.newInstance(buddyJabberId);

        personBuddyStatus = buddy.getFriendStatus();
        showActionFragment(personBuddyStatus);

        mBackground = new ColorDrawable(Color.WHITE);
        background.setBackground(mBackground);

        listHolder = (LinearLayout) findViewById(R.id.listHolder);

        shareListView = (ListView) findViewById(R.id.shareListview);
        setupListView(shareListView, SHARING_CONDITIONS, R.string.can_share);

        learnListView = (ListView) findViewById(R.id.learnListview);
        setupListView(learnListView, LEARNING_CONDITIONS, R.string.wants_to_learn);

        shouldRunAnimation(savedInstanceState, profileImage);
    }

    public void setupListView(ListView listView, int conditions[], int headerTextString) {

        //TODO just inflate a textview
        LinearLayout header = (LinearLayout) View.inflate(this, R.layout.view_profile_listview_header, null);
        TextView headerTv = (TextView) header.findViewById(R.id.learnTv);
        headerTv.setText(getResources().getString(headerTextString));

        listView.addHeaderView(header);

        String[] languages = getLanguages(buddy.getLanguages(), conditions);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.textview_language, languages);
        listView.setAdapter(adapter);

    }

    /**
     * This method filters an arraylist of languages based on the levels given in the parameter
     * It then returns the filtered result
     *
     * @param languages
     * @param levels
     * @return filtered
     */
    private String[] getLanguages(ArrayList<FCPLanguageModel> languages, int[] levels) {
        ArrayList<String> filtered = new ArrayList<String>();

        for (FCPLanguageModel language : languages) {
            //check if the language proficiency matches one of the levels
            for (int level : levels) {
                if (language.getProficiencyCode() == level) {
                    filtered.add(language.getName());
                }
            }
        }

        return filtered.toArray(new String[filtered.size()]);
    }

    @Override
    public void setViewsForEntranceAnim() {
        intro.setTranslationY(-intro.getHeight());
        intro.setVisibility(View.INVISIBLE);

        container.setVisibility(View.INVISIBLE);
        listHolder.setVisibility(View.INVISIBLE);
    }

    @Override
    public Runnable getEntranceAnimations() {
        return new Runnable() {
            @Override
            public void run() {
                final long duration = (long) (ANIM_DURATION * sAnimatorScale);
                // Animate the description in after the image animation
                // is done. Slide and fade the text in from underneath
                // the picture.

                intro.setVisibility(View.VISIBLE);
                intro.animate().setDuration(duration / 2).
                        translationY(0).alpha(1).
                        setInterpolator(sDecelerator);

                Animation fadeIn = new AlphaAnimation(0, 1);
                fadeIn.setInterpolator(new AccelerateInterpolator()); //add this
                fadeIn.setDuration(duration / 2);

                container.setVisibility(View.VISIBLE);
                container.setAnimation(fadeIn);
                container.animate();

                Animation slideUp = new TranslateAnimation(0, 0, shareListView.getHeight(), 0);
                slideUp.setInterpolator(new AccelerateInterpolator());
                slideUp.setDuration(duration);

                listHolder.setVisibility(View.VISIBLE);
                listHolder.setAnimation(slideUp);
                listHolder.animate();
            }

        };
    }

    @Override
    public ImageView getSharedElement(){
        return profileImage;
    }

    public void runExitAnimation() {

        listHolder.animate().translationY(listHolder.getHeight());
        container.animate().alpha(0);
        //slide/fade text out of the way
        intro.animate()
                .translationY(-intro.getHeight())
                .alpha(0)
                .setInterpolator(sAccelerator)
                .withEndAction(getSharedElementAnim());
    }

    /**
     * This class shows the appropriate actions that can be performed based on the subscription status of the persons
     * profile you are looking at.
     * <p/>
     * Special conditions is CANCEL, when we cancel a defriend/cancel/remove we the show the action based on the persons status
     *
     * @param status
     */

    private void showActionFragment(int status) {
        switch (status) {
            case NOT_A_FRIEND:
                showFragment(addBuddyFragment, "Add");
                break;
            case PENDING_REQUEST:
                showFragment(pendingRequestFragment, "Pending");
                break;
            case FRIEND_REQUEST:
                showFragment(requestResponseFragment, "Friend");
                break;
            case FRIEND:
                showFragment(buddyActionFragment, "Request");
                break;
            case REMOVE:
                showFragment(removeFragment, "Remove");
                break;
            //if a cancel has been called we go back to the original fragment before remove was called
            case CANCEL:
                showActionFragment(personBuddyStatus);
                break;
        }
    }

    private void showFragment(Fragment frag, String tag) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.replace(R.id.action_container, frag, tag);
        fragmentTransaction.commit();
    }

    //if we don't have the users avatar on file we have to wait an event telling us it has been downloaded
    @Subscribe
    public void avatarDownloaded(NewAvatarEvent event) {
        Log.d(TAG, "avatarDownloaded - " + event.username);

        String parsedUsername = XmppStringUtils.unescapeLocalpart(XmppStringUtils.parseLocalpart(buddyJabberId));

        if (event.username.equals(parsedUsername)) {
            File download = fcpAvatarManager.getAvatar(buddyJabberId);
            Picasso.with(this).load(download).skipMemoryCache().into(profileImage);
        }
    }

    //TODO
    // When a persons subscription status changes we have to update the UI to reflect this
    @Subscribe
    public void subscriptionChangeEvent(BuddySubscriptionChange event) {
        Log.d(TAG, "subscriptionChangeEvent - " + event.getJid());
        Log.d(TAG, "subscriptionChangeEvent - " + event.getNewStatus());

        if (event.jid.equals(buddyJabberId)) {
            // To make sure we can go back if a remove is cancelled
            if (event.getNewStatus() != REMOVE && event.getNewStatus() != CANCEL) {
                //TODO might not work
                personBuddyStatus = buddy.getFriendStatus();

                //personBuddyStatus = event.getNewStatus();
            }
            showActionFragment(event.getNewStatus());
        }
    }
}


