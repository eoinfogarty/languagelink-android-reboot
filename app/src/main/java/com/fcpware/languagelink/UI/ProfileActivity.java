package com.fcpware.languagelink.UI;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.facebook.Session;
import com.fcpware.languagelink.FCPConnection;
import com.fcpware.languagelink.R;
import com.fcpware.languagelink.XMPP.PEP.PEPManager;
import com.fcpware.languagelink.XMPP.PEP.bak_packet.NickItem;
import com.fcpware.languagelink.events.AvatarUploadedEvent;
import com.fcpware.languagelink.events.NewAvatarEvent;
import com.fcpware.languagelink.managers.FCPAvatarManager;
import com.fcpware.languagelink.managers.SharedPreferencesManager;
import com.fcpware.languagelink.models.FCPUserModel;
import com.fcpware.languagelink.service.ConnectionService;
import com.fcpware.languagelink.utilities.ImageResize;
import com.nineoldandroids.animation.Animator;
import com.soundcloud.android.crop.Crop;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smackx.nick.packet.Nick;
import org.jxmpp.util.XmppStringUtils;

import java.io.File;
import java.io.IOException;

public class ProfileActivity extends SharedElementActivity{

    private static final String TAG = ProfileActivity.class.getSimpleName();

    private static final int ANIMATION_DURATION = 2000;

    public static final int REQUEST_IMAGE_CAPTURE = 5;
    private String uncroppedImageLocationPath = "";

    private SharedPreferencesManager preferencesManager;
    private FCPAvatarManager fcpAvatarManager;
    private FCPUserModel userModel;
    private FCPConnection fcpConnection;

    private TextView editNickName;
    private TextView editUserIntro;
    private TextView rateText;
    private TextView shareText;
    private TextView logoutText;

    private ImageView profilePictureView;
    private LinearLayout pictureActionsLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        preferencesManager = new SharedPreferencesManager(this);
        fcpAvatarManager = FCPAvatarManager.getInstance(this);
        fcpConnection = FCPConnection.getInstance();
        userModel = FCPUserModel.getInstance();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setLogo(R.drawable.ic_friend_search);
        toolbar.setTitle(getResources().getString(R.string.title_my_profile));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mBackground = new ColorDrawable(Color.WHITE);
        RelativeLayout background = (RelativeLayout) findViewById(R.id.background);
        background.setBackground(mBackground);

        editNickName = (TextView) findViewById(R.id.editNickName);
        editNickName.setText(userModel.getNickName());
        editUserIntro = (TextView) findViewById(R.id.editIntro);
        editUserIntro.setText(userModel.getIntro());

        profilePictureView = (ImageView) findViewById(R.id.selection_profile_pic);
        File avatar = fcpAvatarManager.getAvatar(userModel.getJabberId());
        Picasso.with(this).load(avatar).skipMemoryCache().into(profilePictureView);

        rateText = (TextView) findViewById(R.id.rate_me);
        shareText = (TextView) findViewById(R.id.share);
        logoutText = (TextView) findViewById(R.id.logout);

        //todo activity onclicklistener
        rateText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToStore();
            }
        });

        shareText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                share();
            }
        });

        logoutText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logOut();
            }
        });

        pictureActionsLayout = (LinearLayout) findViewById(R.id.picture_options_layout);
        findViewById(R.id.galleryImageButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Crop.pickImage((ProfileActivity) v.getParent().getParent());
            }
        });

        // check to see if the user has a camera, if they do set the camera button enabled and visible
        Boolean userHasCamera = getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);

        if(userHasCamera){
            findViewById(R.id.cameraImageButton).setVisibility(View.VISIBLE);
            findViewById(R.id.cameraImageButton).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, ((ProfileActivity) getParent()).setImageUri());
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                }
            });
        }

        shouldRunAnimation(savedInstanceState, profilePictureView);
    }

    @Override
    public void setViewsForEntranceAnim() {
        editNickName.setVisibility(View.INVISIBLE);
        editUserIntro.setVisibility(View.INVISIBLE);
        rateText.setVisibility(View.INVISIBLE);
        shareText.setVisibility(View.INVISIBLE);
        logoutText.setVisibility(View.INVISIBLE);
        pictureActionsLayout.setVisibility(View.INVISIBLE);
    }

    @Override
    public Runnable getEntranceAnimations() {
        return new Runnable() {
            @Override
            public void run() {
                // Animate the description in after the image animation
                // is done. Slide and fade the text in from underneath
                // the picture.

                final long duration = (long) (ANIM_DURATION * sAnimatorScale);

                Animation fadeIn = new AlphaAnimation(0, 1);
                fadeIn.setInterpolator(new AccelerateInterpolator()); //add this
                fadeIn.setDuration(duration/2);

                editNickName.setVisibility(View.VISIBLE);
                editNickName.setAnimation(fadeIn);
                editNickName.animate();
                editUserIntro.setVisibility(View.VISIBLE);
                editUserIntro.setAnimation(fadeIn);
                editUserIntro.animate();
                rateText.setVisibility(View.VISIBLE);
                shareText.setVisibility(View.VISIBLE);
                logoutText.setVisibility(View.VISIBLE);
                rateText.animate().alpha(1);
                shareText.animate().alpha(1);
                logoutText.animate().alpha(1);
                pictureActionsLayout.setVisibility(View.VISIBLE);
                pictureActionsLayout.setAnimation(fadeIn);
                pictureActionsLayout.animate();
            }
        };
    }

    @Override
    public ImageView getSharedElement(){
        return profilePictureView;
    }

    public void runExitAnimation( ) {
        pictureActionsLayout.animate().alpha(0);
        rateText.animate().alpha(0);
        shareText.animate().alpha(0);
        logoutText.animate().alpha(0);
        //todo textview animations
        editUserIntro.animate()
                .translationY(-editNickName.getHeight())
                .alpha(0)
                .setInterpolator(sAccelerator);

        // First, slide/fade text out of the way
        editNickName.animate()
                .translationY(-editNickName.getHeight())
                .alpha(0)
                .setInterpolator(sAccelerator)
                .withEndAction(getSharedElementAnim());
    }

    /**
     * We listen for the avatar to be downloaded for the first time when we login / sign up
     * @param event - New Avatar Event
     */
    @Subscribe
    public void newAvatar(NewAvatarEvent event) {

        String parsedJid = XmppStringUtils.parseLocalpart(userModel.getJabberId());
        parsedJid = XmppStringUtils.unescapeLocalpart(parsedJid);

        Log.d(TAG, "Got avatar for " + event.username);
        Log.d(TAG, "User is " + parsedJid);

        if(event.username.equals(parsedJid)){
            Log.d(TAG, "Got the avatar" + event.getUri());
            //profilePictureView.setImageURI(event.getUri());
            File avatar = fcpAvatarManager.getAvatar(userModel.getJabberId());
            Picasso.with(this).load(avatar).skipMemoryCache().into(profilePictureView);
        }
    }


    @Subscribe
    public void onAvatarChange(final AvatarUploadedEvent event) {
        Log.d(TAG, "About to set new avatar");
        YoYo.with(Techniques.TakingOff).duration(ANIMATION_DURATION/2).withListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                Picasso.with(getBaseContext()).load(event.avatar).noFade().skipMemoryCache().into(profilePictureView);
                YoYo.with(Techniques.Landing).duration(ANIMATION_DURATION/2).playOn(profilePictureView);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        }).playOn(profilePictureView);
    }

    //TODO save on pause

    private void save() {
        //todo save vcard only only once
        if(!editNickName.getText().toString().equals(userModel.getNickName())){
            Toast.makeText(this, R.string.your_name_has_been_changed, Toast.LENGTH_SHORT).show();

            NickItem newNick = new NickItem("nick" , new Nick(editNickName.getText().toString()));

            //TODO fix and bring back
            // Create a new message to publish the event.
//            PEPPubSub pubSub = new PEPPubSub(newNick);
//            pubSub.setType(IQ.Type.set);
            //pubSub.setFrom(connection.getUser());

            // Send the message that contains the roster
//            fcpConnection.sendPacket(pubSub);

            PEPManager mgr = new PEPManager(fcpConnection.getConnection());
            try {
                mgr.publish(newNick);
            } catch (SmackException.NotConnectedException e) {
                e.printStackTrace();
            }

            userModel.setNickName(normalize(editNickName.getText().toString()));
            userModel.save(fcpConnection.getConnection(), this);
            preferencesManager.setNickname(normalize(editNickName.getText().toString()));
        }
        if(!editUserIntro.getText().toString().equals(userModel.getIntro())){
            Toast.makeText(this, R.string.your_intro_has_been_changed, Toast.LENGTH_SHORT).show();
            userModel.setIntro(editUserIntro.getText().toString());
            userModel.save(fcpConnection.getConnection(), this);
            preferencesManager.setIntro(editUserIntro.getText().toString());
        }
    }

    private String normalize(String text) {
        String normalizedNickName = text.trim();
        normalizedNickName = normalizedNickName.trim().replaceAll(" +", " ");

        return normalizedNickName;
    }

    private void share() {

        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
        String sAux = getResources().getString(R.string.let_me_recommend);
        sAux = sAux + "https://play.google.com/store/apps/details?id=com.fcpware.languagelink \n\n";
        i.putExtra(Intent.EXTRA_TEXT, sAux);
        startActivity(Intent.createChooser(i, getResources().getString(R.string.share_with)));

    }

    private void goToStore() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("market://details?id=com.fcpware.languagelink"));
        startActivity(intent);
    }


    private void logOut() {

        Log.d(TAG, "logOut - clearing preferences");
        SharedPreferencesManager loginDetails = new SharedPreferencesManager(this);
        loginDetails.clearPreferences();

        Session session = Session.getActiveSession();
        if (session != null && !session.isClosed()) {
            Log.d(TAG, "logOut - clearing facebook");
            session.closeAndClearTokenInformation();
            //clear your preferences if saved
        }
        Log.d(TAG, "logOut - stopping service");
        FCPConnection.getInstance().disconnect();
        stopService(new Intent(this, ConnectionService.class));

        Log.d(TAG, "logOut - sending to splash screen");
        Intent i = new Intent(this, SplashActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        System.exit(0);
    }

    /**
     * When we get a Uri back from the gallery, before we upload it we have to do these things
     *
     * 1. rename the file to the jabberId of the user
     * 2. resize the image to 400x400
     * 3. set the image view to the picture
     *
     * upload the associated image/video
     */

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent result) {
        if (requestCode == Crop.REQUEST_PICK && resultCode == Activity.RESULT_OK) {
            Log.d(TAG, "onActivityResult - cropper request pick");
            beginCrop(result.getData());
        } else if (requestCode == Crop.REQUEST_CROP) {
            Log.d(TAG, "onActivityResult - cropper request crop");
            handleCrop(resultCode, result);
        } else if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            Log.d(TAG, "onActivityResult - intent request image capture ");
            beginCrop(Uri.fromFile(new File(uncroppedImageLocationPath)));
        }
    }


    //we need to create a file the picture that will be taken from the camera to be downloaded into
    public Uri setImageUri() {
        // Store image in dcim
        File uncroppedImageLocation = new File(Environment.getExternalStorageDirectory() + "/DCIM/", "image.png");

        Uri imgUri = Uri.fromFile(uncroppedImageLocation);
        this.uncroppedImageLocationPath = uncroppedImageLocation.getAbsolutePath();
        return imgUri;
    }


    private void beginCrop(Uri data) {
        Log.d(TAG, "beginCrop - got a result starting crop now");
        Uri outputUri = Uri.fromFile(new File(this.getCacheDir(), "cropped"));
        new Crop(data).output(outputUri).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        Log.d(TAG, "handleCrop - got a result starting crop now");
        if (resultCode == Activity.RESULT_OK) {
            ImageResize resize = new ImageResize(this, 400, 400, Crop.getOutput(result));
            Bitmap resizeBitmap = resize.getResizeImage();

            //This piece of code handles the rotation of the bitmap
            //A jpg will have rotation information placed in it that will mess up an image view
            //so we should take his into account

            //TODO clean up code
            ExifInterface exif = null;
            try {
                exif = new ExifInterface(Crop.getOutput(result).getPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

            Matrix matrix = new Matrix();
            switch (orientation) {
                case ExifInterface.ORIENTATION_NORMAL:
                    // Do nothing. The original image is fine.
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    matrix.postRotate(90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    matrix.postRotate(180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    matrix.postRotate(270);
                    break;
            }

            Bitmap bmRotated = Bitmap.createBitmap(resizeBitmap, 0, 0, resizeBitmap.getWidth(), resizeBitmap.getHeight(), matrix, true);
            resizeBitmap.recycle();

            //TODO clean
            FCPAvatarManager fcpAvatarManager = FCPAvatarManager.getInstance(this);
            FCPUserModel userModel = FCPUserModel.getInstance();
            fcpAvatarManager.saveUsersAvatar(userModel.getJabberId(), bmRotated);

            //TODO send an event to set the profile pic

            //profilePictureView.setImageBitmap(bmRotated);

            File uncroppedImageLocation = new File(Environment.getExternalStorageDirectory() + "/DCIM/", "image.png");
            uncroppedImageLocation.delete();

        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}


