package com.fcpware.languagelink.UI.chat;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.fcpware.languagelink.ORM.ChatSummary;
import com.fcpware.languagelink.R;
import com.fcpware.languagelink.events.BusProvider;
import com.fcpware.languagelink.events.NewSummaryEvent;
import com.fcpware.languagelink.managers.FCPChatSummaryManager;
import com.squareup.otto.Subscribe;

import java.util.List;

/**
 * list view with a summary of all chats
 */
public class ChatSummaryListFragment extends ListFragment {

    private static final String TAG = "ChatListFragment";
    private ChatListAdapter chatListAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        BusProvider.getInstance().register(this);
        View root = inflater.inflate(R.layout.fragment_main_chat_list, container, false);

        FCPChatSummaryManager fcpChatSummaryManager = FCPChatSummaryManager.getInstance(getActivity());

        chatListAdapter = new ChatListAdapter(getActivity(), fcpChatSummaryManager.getSummaries());
        setListAdapter(chatListAdapter);

        return root;
    }

    @Subscribe
    public void newChatSummary(NewSummaryEvent event) {
        Log.d(TAG, "onBuddyListSet - buddy list has been set or changed");
        chatListAdapter.notifyDataSetChanged();
    }

    /**
     * The adapter responsible for inflating the views that represent a chat summary
     */
    public class ChatListAdapter extends BaseAdapter {

        private Context context;
        private List<ChatSummary> summaries;


        public ChatListAdapter(Context context, List<ChatSummary> summaries ) {
            this.summaries = summaries;
            this.context = context;
        }

        @Override
        public int getCount() {
            return summaries.size();
        }

        @Override
        public Object getItem(int position) {
            return summaries.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ChatSummary chatSummary = summaries.get(position);
            ChatSummaryView chatSummaryView;


            if(convertView==null){
                chatSummaryView = new ChatSummaryView(context);
            } else {
                chatSummaryView = (ChatSummaryView) convertView;
            }

            chatSummaryView.setSummary(chatSummary);


            return chatSummaryView;
        }
    }
}
