package com.fcpware.languagelink.UI;

import android.animation.TimeInterpolator;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import com.fcpware.languagelink.BaseActivity;
import com.nineoldandroids.animation.ObjectAnimator;

public abstract class SharedElementActivity extends BaseActivity {

    public static final TimeInterpolator sDecelerator = new DecelerateInterpolator();
    public static final TimeInterpolator sAccelerator = new AccelerateInterpolator();
    public static final int ANIM_DURATION = 300;
    private static final String PACKAGE_NAME = "com.fcpware.languagelink";
    public static float sAnimatorScale = 1;

    public ColorDrawable mBackground;
    public int mOriginalOrientation;
    public float mHeightScale, mWidthScale;
    public int mLeftDelta, mTopDelta;
    public int thumbnailTop, thumbnailLeft, thumbnailWidth, thumbnailHeight;

    protected abstract void setViewsForEntranceAnim();

    protected abstract ImageView getSharedElement();
    /**
     * The exit animation is basically a reverse of the enter animation, except that if
     * the orientation has changed we simply scale the picture back into the center of
     * the screen.
     */
    protected abstract void runExitAnimation();
    protected abstract Runnable getEntranceAnimations();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Retrieve the data we need for the picture/description to display and
        // the thumbnail to animate it from
        Bundle bundle = getIntent().getExtras();
        thumbnailTop = bundle.getInt(PACKAGE_NAME + ".top");
        thumbnailLeft = bundle.getInt(PACKAGE_NAME + ".left");
        thumbnailWidth = bundle.getInt(PACKAGE_NAME + ".width");
        thumbnailHeight = bundle.getInt(PACKAGE_NAME + ".height");
        mOriginalOrientation = bundle.getInt(PACKAGE_NAME + ".orientation");

    }

    public void shouldRunAnimation(Bundle savedInstanceState, final ImageView sharedElement) {
        // Only run the animation if we're coming from the parent activity, not if
        // we're recreated automatically by the window manager (e.g., device rotation)
        if (savedInstanceState == null) {
            ViewTreeObserver observer = sharedElement.getViewTreeObserver();
            observer.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {

                @Override
                public boolean onPreDraw() {
                    sharedElement.getViewTreeObserver().removeOnPreDrawListener(this);

                    // Figure out where the thumbnail and full size versions are, relative
                    // to the screen and each other
                    int[] screenLocation = new int[2];
                    sharedElement.getLocationOnScreen(screenLocation);
                    mLeftDelta = thumbnailLeft - screenLocation[0];
                    mTopDelta = thumbnailTop - screenLocation[1];

                    // Scale factors to make the large version the same size as the thumbnail
                    mWidthScale = (float) thumbnailWidth / sharedElement.getWidth();
                    mHeightScale = (float) thumbnailHeight / sharedElement.getHeight();

                    runEnterAnimation(sharedElement);

                    return true;
                }
            });
        }
    }


    public void runEnterAnimation(ImageView sharedElement) {
        setViewsForEntranceAnim();

        final long duration = (long) (ANIM_DURATION * sAnimatorScale); //todo twice is bad

        // Set starting values for properties we're going to animate. These
        // values scale and position the full size version down to the thumbnail
        // size/location, from which we'll animate it back up
        sharedElement.setPivotX(0);
        sharedElement.setPivotY(0);
        sharedElement.setScaleX(mWidthScale);
        sharedElement.setScaleY(mHeightScale);
        sharedElement.setTranslationX(mLeftDelta);
        sharedElement.setTranslationY(mTopDelta);

        // Animate scale and translation to go from thumbnail to full size
        sharedElement.animate()
                .setDuration(duration)
                .scaleX(1).scaleY(1)
                .translationX(0).translationY(0)
                .setInterpolator(sDecelerator)
                .withEndAction(getEntranceAnimations());

        // Fade in the black background
        ObjectAnimator bgAnim = ObjectAnimator.ofInt(mBackground, "alpha", 0, 255);
        bgAnim.setDuration(duration);
        bgAnim.start();
    }



    public Runnable getSharedElementAnim() {
        return new Runnable() {
            public void run() {
                final long duration = (long) (ANIM_DURATION * sAnimatorScale);

                // Caveat: configuration change invalidates thumbnail positions; just animate
                // the scale around the center. Also, fade it out since it won't match up with
                // whatever is actually in the center
                ImageView sharedElement = getSharedElement();
                final boolean fadeOut;
                if (getResources().getConfiguration().orientation != mOriginalOrientation) {
                    sharedElement.setPivotX(sharedElement.getHeight() / 2);
                    sharedElement.setPivotY(sharedElement.getWidth() / 2);
                    mLeftDelta = 0;
                    mTopDelta = 0;
                    fadeOut = true;
                } else {
                    fadeOut = false;
                }

                Animation fadeOutAnim = new AlphaAnimation(1, 0);
                fadeOutAnim.setInterpolator(new AccelerateInterpolator()); //add this
                fadeOutAnim.setDuration(duration / 2);

                // Animate image back to thumbnail size/location
                sharedElement.animate()
                        .setDuration(duration)
                        .scaleX(mWidthScale).scaleY(mHeightScale)
                        .translationX(mLeftDelta).translationY(mTopDelta)
                        .withEndAction(exit);

                if (fadeOut) {
                    sharedElement.animate().alpha(0);
                }

                // Fade out background
                ObjectAnimator bgAnim = ObjectAnimator.ofInt(mBackground, "alpha", 0);
                bgAnim.setDuration(duration);
                bgAnim.start();
            }
        };
    }



    Runnable exit = new Runnable() {
        public void run() {
            backPressed();
            overridePendingTransition(0, 0);
        }
    };

    private void sendBack() {
        Bundle extras = getIntent().getExtras();
        boolean launchedFromNotif = false;

        if (extras.containsKey("EXTRA_LAUNCHED_BY_NOTIFICATION")) {
            launchedFromNotif = extras.getBoolean("EXTRA_LAUNCHED_BY_NOTIFICATION");
        }

        if (launchedFromNotif) {
            // Launched from notification, handle as special case
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
            finish();
        } else {
            runExitAnimation();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                sendBack();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        sendBack();
    }

    private void backPressed() {
        super.onBackPressed();
    }
}
