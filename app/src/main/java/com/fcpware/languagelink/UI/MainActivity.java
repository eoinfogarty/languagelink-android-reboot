package com.fcpware.languagelink.UI;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.astuetz.PagerSlidingTabStrip;
import com.fcpware.languagelink.R;
import com.fcpware.languagelink.UI.chat.ChatSummaryListFragment;
import com.fcpware.languagelink.UI.friends.FriendsListFragment;
import com.fcpware.languagelink.events.BusProvider;

public class MainActivity extends FragmentActivity
{
    private static final String TAG = "MainActivity";

    public MainActivity() {
        // Required empty public constructor
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BusProvider.getInstance().register(this);
        setContentView(R.layout.activity_main);

        ViewPager mViewPager = (ViewPager) findViewById(R.id.pager);
        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);

        TabFragmentPagerAdapter mTabPagerAdapter = new TabFragmentPagerAdapter(getSupportFragmentManager());

        mViewPager.setAdapter(mTabPagerAdapter);
        tabs.setViewPager(mViewPager);
    }


    public class TabFragmentPagerAdapter extends FragmentPagerAdapter implements PagerSlidingTabStrip.IconTabProvider{

        final int FRIENDS_TAB = 0;
        final int CHAT_TAB = 1;

        private int tabIcons[] = {R.drawable.ic_friends, R.drawable.ic_chat_white_48dp};
        private final String[] titles = { "Friends", "Chat"};

        public TabFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int SelectedTab) {
            switch (SelectedTab) {
                //TODO create instance
                case FRIENDS_TAB:
                    return new FriendsListFragment();
                case CHAT_TAB:
                    return new ChatSummaryListFragment();
                default:
                    return null;
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }

        @Override
        public int getCount() {
            return tabIcons.length;
        }

        @Override
        public int getPageIconResId(int position) {
            return tabIcons[position];
        }
    }
}
