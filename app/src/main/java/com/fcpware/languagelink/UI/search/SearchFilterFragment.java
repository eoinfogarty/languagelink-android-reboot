package com.fcpware.languagelink.UI.search;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.fcpware.languagelink.FCPConnection;
import com.fcpware.languagelink.R;
import com.fcpware.languagelink.events.BusProvider;
import com.fcpware.languagelink.events.NewLanguage;
import com.fcpware.languagelink.managers.SharedPreferencesManager;
import com.fcpware.languagelink.models.FCPLanguageModel;
import com.fcpware.languagelink.models.FCPUserModel;
import com.nhaarman.listviewanimations.itemmanipulation.DynamicListView;
import com.nhaarman.listviewanimations.itemmanipulation.swipedismiss.OnDismissCallback;
import com.nhaarman.listviewanimations.itemmanipulation.swipedismiss.undo.TimedUndoAdapter;
import com.nhaarman.listviewanimations.itemmanipulation.swipedismiss.undo.UndoAdapter;
import com.nhaarman.listviewanimations.util.Insertable;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

/**
 * Contains a list of languages the user has on their profile that they will use to search
 */
public class SearchFilterFragment extends Fragment{

    // Singleton variable
    private static SearchFilterFragment instance = null;

    private static final String TAG = "FilterFragment";
    private DynamicListView learnList;
    private DynamicListView shareList;

    private String learn, share;
    private  LanguageAdapter learnAdapter, shareAdapter;

    public SearchFilterFragment() {
    }

    public static SearchFilterFragment newInstance(){
        Log.d(TAG, "Creating a new instance of FilterFragment");
        if(instance==null){
            instance = new SearchFilterFragment();
        }
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_search_filters, container, false);

        final FilterHeaderView learnHeader  = (FilterHeaderView) root.findViewById(R.id.learnHeader);
        final FilterHeaderView shareHeader  = (FilterHeaderView) root.findViewById(R.id.shareHeader);

        learnHeader.setType(R.string.i_want_to_learn);
        shareHeader.setType(R.string.i_can_share);

        learnList = (DynamicListView) root.findViewById(R.id.learnlist);
        shareList = (DynamicListView) root.findViewById(R.id.sharelist);

        learnAdapter = createAdapterForList(learnList, LanguageAdapter.LEARN_TYPE);
        shareAdapter = createAdapterForList(shareList, LanguageAdapter.SHARE_TYPE);

        Button search = (Button) root.findViewById(R.id.search);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d(TAG, "Learn selected " + learnList.getCheckedItemPosition());
                Log.d(TAG, "Share selected " + shareList.getCheckedItemPosition());

                if(learnList.getCheckedItemPosition()<0){
                    YoYo.with(Techniques.Tada).playOn(learnHeader);
                    return;
                }

                if(shareList.getCheckedItemPosition()<0){
                    YoYo.with(Techniques.Tada).playOn(shareHeader);
                    return;
                }
                learn = (String) learnAdapter.getItem(learnList.getCheckedItemPosition());
                share = (String) shareAdapter.getItem(shareList.getCheckedItemPosition());

                Log.d(TAG, "Learn = " + learn);
                Log.d(TAG, "Share = " + share);

                if (learn != null && share != null) {
                    //TODO search results into parent activity
                    SearchResultsFragment resultsFragment = new SearchResultsFragment(learn, share);

                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .addToBackStack("filter")
                            .setCustomAnimations(R.anim.slide_up_out,R.anim.slide_up_in)
                            .replace(R.id.container, resultsFragment, "results")
                            .commit();
                }


            }
        });

        return root;
    }

    private LanguageAdapter createAdapterForList(DynamicListView list, int type) {

        final LanguageAdapter adapter = new LanguageAdapter(list, getActivity(), type);

        TimedUndoAdapter undoAdapter = new TimedUndoAdapter(adapter, getActivity(),
                new OnDismissCallback() {
                    @Override
                    public void onDismiss(@NonNull final ViewGroup listView, @NonNull final int[] reverseSortedPositions) {
                        for (int position : reverseSortedPositions) {
                            adapter.remove(position);
                        }
                    }
                }
        );

        undoAdapter.setAbsListView(list);
        list.setAdapter(undoAdapter);
        list.enableSimpleSwipeUndo();
        list.setSelector(R.drawable.list_selector);

        if(list.getCount()>0){
            list.setItemChecked(0,true);
        }

        return adapter;
    }

    private class LanguageAdapter extends BaseAdapter implements Insertable, UndoAdapter {

        public static final int LEARN_TYPE = FCPLanguageModel.BEGINNER;
        public static final int SHARE_TYPE = FCPLanguageModel.FLUENT;

        private DynamicListView parentList;
        private Context context;
        private int adapterType;

        private ArrayList<FCPLanguageModel> languages = new ArrayList<>();

        public LanguageAdapter(DynamicListView parentList, Context context, int adapterType) {
            BusProvider.getInstance().register(this);
            this.adapterType = adapterType;
            this.parentList = parentList;
            this.context = context;

            for(FCPLanguageModel language: FCPUserModel.getInstance().getLanguages()){
                addLanguageIfType(language);
            }

        }

        //adds the language to the adapter if the language matches the type of the adapter
        public void addLanguageIfType(FCPLanguageModel language) {

            Boolean userIsLessThanFluent = language.getProficiencyCode() < FCPLanguageModel.FLUENT;
            Boolean userIsFluent = language.getProficiencyCode() == FCPLanguageModel.FLUENT;

            if (adapterType == LEARN_TYPE && userIsLessThanFluent ||
                adapterType == SHARE_TYPE && userIsFluent) {

                languages.add(language);
                notifyDataSetChanged();

                parentList.setItemChecked(getCount()-1, true);

            }
        }

        @Override
        public int getCount() {
            return languages.size();
        }

        @Override
        public Object getItem(int position) {
            return languages.get(position).getName();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            FCPLanguageModel language = languages.get(position);

            if (convertView == null) {
                holder = new ViewHolder();
                convertView = LayoutInflater.from(context).inflate(R.layout.view_language, parent, false);
                holder.root = (LinearLayout) convertView.findViewById(R.id.root);
                holder.language = (TextView) convertView.findViewById(R.id.language);
                holder.delete = (ImageButton) convertView.findViewById(R.id.delete);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.language.setText(language.getName());

            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TranslateAnimation transAnimation = new TranslateAnimation(0, -150, 0, 0);
                    transAnimation.setDuration(500);
                    holder.root.startAnimation(transAnimation);
                }
            });

            return convertView;
        }

        class ViewHolder {
            LinearLayout root;
            TextView language;
            ImageButton delete;
        }

        //To make sure the header is displayed even if there are no languages on the list
        @Override
        public boolean isEmpty(){
            return false;
        }

        @Subscribe
        public void newLanguage(NewLanguage event){
            Log.d(TAG, "New language " +  event.newLanguage.getName());
            addLanguageIfType(event.getNewLanguage());
        }

        @Override
        public void add(int position, @NonNull Object o) {
            Log.d(TAG, "Add being called");
            languages.add(position, ((FCPLanguageModel) o));

            parentList.setItemChecked(position, true);
            notifyDataSetChanged();
        }

        public void remove(int position) {

            FCPUserModel.getInstance().getLanguages().remove(languages.get(position));
            languages.remove(position);

            //TODO saving evertime is not efficient so should just save one
            FCPUserModel.getInstance().save(FCPConnection.getInstance().getConnection(), getActivity());
            //TODO move into FCPUserModel
            SharedPreferencesManager preferencesManager = new SharedPreferencesManager(getActivity());
            preferencesManager.saveLanguageFile(FCPUserModel.getInstance().getLanguages());

            notifyDataSetChanged();

            if(parentList.getCheckedItemPosition()>position||parentList.getCheckedItemPosition()==parentList.getCount()){
                Log.d(TAG, "its greater than or at the end of the list, take one away");
                parentList.setItemChecked(parentList.getCheckedItemPosition()-1,true);
            }
        }

        @NonNull
        @Override
        public View getUndoView(final int position, final View convertView, @NonNull final ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = LayoutInflater.from(context).inflate(R.layout.view_undo_delete, parent, false);
            }
            return view;
        }

        @NonNull
        @Override
        public View getUndoClickView(@NonNull final View view) {
            return view.findViewById(R.id.undoBtn);
        }
    }
}
