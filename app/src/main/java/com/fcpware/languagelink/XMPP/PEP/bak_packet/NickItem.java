package com.fcpware.languagelink.XMPP.PEP.bak_packet;

import com.fcpware.languagelink.XMPP.PEP.PEPItem;

import org.jivesoftware.smackx.nick.packet.Nick;

/**
 * Created by eoinfogarty on 23/09/2014.
 */
public class NickItem extends PEPItem {

    Nick nick;
    /**
     * Creates a new PEPItem.
     *
     * @param id
     */
    public NickItem(String id, Nick nick) {
        super(id);
        this.nick = nick;
    }

    @Override
    public String getNode() {
        return "http://jabber.org/protocol/nick";
    }

    @Override
    public String getItemDetailsXML() {

        return nick.toXML();
    }


    public Nick getNick() {
        return nick;
    }
}
