package com.fcpware.languagelink.XMPP;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import com.facebook.model.GraphUser;
import com.fcpware.languagelink.Constants;
import com.fcpware.languagelink.XMPP.vcard.FCPVCardManager;
import com.fcpware.languagelink.XMPP.vcard.packet.VCard;
import com.fcpware.languagelink.XMPP.vcard.provider.VCardProvider;
import com.fcpware.languagelink.managers.FCPAvatarManager;
import com.fcpware.languagelink.managers.SharedPreferencesManager;
import com.fcpware.languagelink.service.ConnectionService;
import com.fcpware.languagelink.service.LocalBinder;

import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smack.sasl.provided.SASLPlainMechanism;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.iqregister.AccountManager;
import org.jivesoftware.smackx.search.ReportedData;
import org.jivesoftware.smackx.search.UserSearchManager;
import org.jivesoftware.smackx.vcardtemp.VCardManager;
import org.jivesoftware.smackx.xdata.Form;
import org.jxmpp.util.XmppStringUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Create a singleton XMPP Client to manage the connection to the server
 */

public class XMPPClient {
    private static final String TAG = "XMPPClient";
    // Singleton variables
    private static XMPPClient instance = null;

    private SharedPreferencesManager userLoginDetails;
    private Context mContext;

    private boolean mBounded;
    private ConnectionService mService;
    private ServiceConnection connectionService = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(TAG,"onServiceConnected - binding and adding listener");
            LocalBinder binder = (LocalBinder) service;
            mService = (ConnectionService) binder.getService();
            mBounded = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBounded = false;
            Log.d(TAG, "onServiceDisconnected");
        }
    };

    public XMPPClient(Context context) {
        Log.d(TAG, "Creating the client");
        mContext = context;

        userLoginDetails = new SharedPreferencesManager(mContext);
        if (userLoginDetails.hasLoggedIn()) {
            Log.d(TAG, "User has already logged in - connecting");
            doBindService();
        }
    }

    public synchronized static XMPPClient getInstance(Context context) {
        if (instance == null) {
            instance = new XMPPClient(context);
        }
        return instance;
    }

    void doBindService() {
        // use 0 as a flag , will return true if you connected to the service otherwise false if the service doesnt exist
        if (!serviceIsRunning(ConnectionService.class)) {
            Log.d(TAG, "Starting service");
            mContext.getApplicationContext().startService(new Intent(mContext, ConnectionService.class));
        }
        Log.d(TAG, "Binding to service");
        mContext.getApplicationContext().bindService(new Intent(mContext, ConnectionService.class), connectionService, Context.BIND_AUTO_CREATE);

    }

    private boolean serviceIsRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    /**
     *  Method called from the login screen
     *  Starts a thread to check if the user allready exists
     */
    public void connectWithFacebook(GraphUser user) {
        Log.d(TAG, "connectWithFacebook - starting SearchForUserTask for " + user.getProperty("email"));
        new SearchForUser(user).execute();
    }

    /**
     * Thread that searches for a user if they exist start a service and set their login details
     * otherwise create an account for them
     */
    private class SearchForUser extends AsyncTask<Void, Void, Boolean> {

        private GraphUser facebookProfile;
        private String username;
        private XMPPTCPConnection connection;

        public SearchForUser(GraphUser user) {
            facebookProfile = user;
            username = XmppStringUtils.escapeLocalpart(facebookProfile.getProperty("email").toString());
            Log.d(TAG, "SearchForUser - Jabber Username:" + username);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                Log.d(TAG, "SearchForUser - doInBackground: creating temporary connection");
                //create a temporary connection
                ProviderManager.removeIQProvider(VCardManager.ELEMENT, VCardManager.NAMESPACE);
                ProviderManager.addIQProvider(FCPVCardManager.ELEMENT, FCPVCardManager.NAMESPACE, new VCardProvider());
                Log.d(TAG, "Current list of IQ providers after adding custom vcard :  " + ProviderManager.getIQProviders().toString());


                XMPPTCPConnectionConfiguration.Builder conf = XMPPTCPConnectionConfiguration.builder();
                conf.setHost(Constants.HOST);
                conf.setPort(Constants.PORT);
                conf.setServiceName(Constants.HOST);
                conf.setSendPresence(false);
                conf.setDebuggerEnabled(true);

                connection = new XMPPTCPConnection(conf.build());
                SASLAuthentication.blacklistSASLMechanism("SCRAM-SHA-1");
                SASLAuthentication.blacklistSASLMechanism("DIGEST-MD5");
                SASLAuthentication.registerSASLMechanism(new SASLPlainMechanism());
                connection.connect();
                connection.login(username,"test");

                UserSearchManager search = new UserSearchManager(connection);
                Form searchForm = search.getSearchForm("vjud." + connection.getServiceName());
                Form answerForm = searchForm.createAnswerForm();
                answerForm.setAnswer("user", username);
                ReportedData data = search.getSearchResults(answerForm, "vjud." + connection.getServiceName());

                //todo return error/no result or result
                return data.getRows().isEmpty();

            } catch (final XMPPException | IOException | SmackException e) {
                e.printStackTrace();
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean noResults) {
            //No results from the search ie the user doesn't exist
            if (noResults) {
                Log.d(TAG, "SearchForUser - User doesn't exist, creating account");
                new CreateAccount(connection, facebookProfile).execute();
            } else {
                Log.d(TAG, "SearchForUser - User already exists, setting userLoginDetails and connecting");
                Log.d(TAG, "SearchForUser - " + username + " - " + "test" );
                userLoginDetails.setLoginData(connection);
                //TODO should pass the connection to the service
                doBindService();
                connection.instantShutdown();
            }
        }

        private class CreateAccount extends AsyncTask<Void, Void, Void> {
            private GraphUser user;
            private String username;
            private XMPPTCPConnection connection;
            private VCard usersProfile;

            public CreateAccount(XMPPTCPConnection connection, GraphUser user) {
                this.connection = connection;
                this.user = user;
                username = XmppStringUtils.escapeLocalpart(facebookProfile.getProperty("email").toString());

                Log.d(TAG, "CreateAccount - Jabber Username:" + username);
            }

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    Log.d(TAG, "CreateAccount - Creating account");
                    AccountManager accountManager = AccountManager.getInstance(connection);
                    accountManager.createAccount(username, "test");

                    // To give time to pull the facebook profile picture
                    connection.setPacketReplyTimeout(10000);

                    usersProfile = new VCard();
                    usersProfile.setJabberId(username + "@" + Constants.HOST);
                    usersProfile.setEmailHome(user.getProperty("email").toString());
                    usersProfile.setNickName(user.getName());
                    usersProfile.setAvatar(getFacebookAvatarByteArray());

                    usersProfile.save(connection);

                    Log.d(TAG, "CreateAccount - Account created and VCard saved");
                    userLoginDetails.setLoginData(connection, usersProfile);
                    String url = String.format("https://graph.facebook.com/%s/picture?width=400&height=400", user.getId());
                    Log.d(TAG, "CreateAccount - Facebook Profile url : " + url);
                    InputStream inputStream = new URL(url).openStream();
                    Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

                    //Send to amazon
                    FCPAvatarManager fcpAvatarManager = FCPAvatarManager.getInstance(mContext);
                    fcpAvatarManager.saveUsersAvatar(username, bitmap);

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "CreateAccount - Exception: ", e);
                }

                return null;
            }

            private byte[] getFacebookAvatarByteArray() throws IOException {

                String smallUrl = String.format("https://graph.facebook.com/%s/picture?width=32&height=32", user.getId());
                Log.d(TAG, "CreateAccount - Facebook Profile url : " + smallUrl);
                InputStream smallInputStream = new URL(smallUrl).openStream();
                Bitmap smallBitmap = BitmapFactory.decodeStream(smallInputStream);
                ByteArrayOutputStream vcardStream = new ByteArrayOutputStream();
                smallBitmap.compress(Bitmap.CompressFormat.JPEG, 100, vcardStream);

                return vcardStream.toByteArray();
            }

            @Override
            protected void onPostExecute(Void result) {
                doBindService();
                //TODO should pass the connection to the service
                connection.instantShutdown();
            }
        }
    }
}
