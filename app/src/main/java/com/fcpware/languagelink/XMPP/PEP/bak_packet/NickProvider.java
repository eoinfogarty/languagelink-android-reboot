package com.fcpware.languagelink.XMPP.PEP.bak_packet;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.provider.PacketExtensionProvider;
import org.jivesoftware.smackx.nick.packet.Nick;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Created by eoinfogarty on 23/09/2014.
 */
public class NickProvider extends PacketExtensionProvider<NickNames> {

    public NickNames parse(XmlPullParser parser, int initialDepth) throws XmlPullParserException, IOException, SmackException {

        /**
         *         http://oneminutedistraction.wordpress.com/tag/java-xmpp-jabber-smack-pep-xep163-196-user-gaming/
         */
        boolean stop = false;
        int evtType;

        String n;
        String id = null;

        Nick nick = new Nick("");
        while (!stop) {
            evtType = parser.next();
            n = parser.getName();
            switch (evtType) {
                case XmlPullParser.START_TAG:
                    if ("item".equals(n))
                    id = parser.getAttributeValue("", "id");
                    else if ("nick".equals(n))
                    nick.setName(parser.nextText());
                    break;
                case XmlPullParser.END_TAG:
                    //Stop parsing when we hit </item>
                    stop = "item".equals(n);
                    break;
            }
        }
        return (new NickNames(new NickItem(id, nick)));
    }
}