package com.fcpware.languagelink.XMPP.PEP.bak_packet;

import com.fcpware.languagelink.XMPP.PEP.PEPEvent;

/**
 * The nickname pep event
 * This is fired when a user updates their nickname
 * it is caught and parsed by all the friends who are subscribed
 */
public class NickNames extends PEPEvent {

    private NickItem nickItem;

    public NickNames(NickItem nick) {
        nickItem = nick;

    }

    public NickItem getNickItem() {
        return nickItem;
    }
    @Override
    public String getNamespace() {
        return "http://jabber.org/protocol/pubsub";
    }
    public String getNode() {
        return "http://jabber.org/protocol/nick";
    }
    public String getItemDetailsXML() {
        return nickItem.toXML();
    }
    @Override
    public String toXML() {

        return ("<event xmlns=\"" + getNamespace() + "\">" +
                    "<items node=\"" + getNode() + "\">"  + getItemDetailsXML() + "</items>" +
                "</event>");
    }
}
