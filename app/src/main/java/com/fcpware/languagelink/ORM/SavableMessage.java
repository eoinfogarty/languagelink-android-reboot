package com.fcpware.languagelink.ORM;

import com.orm.SugarRecord;

import java.util.Date;


/**
 * Created by eoinfogarty on 24/11/14.
 *
 * deliver status can have 3 states 0 NOT_SENT, 1 SENT, 2 DELIVERED
 * //TODO explain
 */
public class SavableMessage extends SugarRecord<SavableMessage> {
    private String receivedFrom;
    private String sentTo;
    private String type;
    private String body;
    private Date timestamp;
    private Date delayedTimestamp;
    private int deliveryStatus;

    private Boolean read;

    //required default constructor for ORM to work
    public SavableMessage() {

    }

    public SavableMessage(String receivedFrom, String sentTo, String type, String body, Date timestamp, Boolean read, Date delayedTimestamp, int deliveryStatus) {
        this.receivedFrom = receivedFrom;
        this.sentTo = sentTo;
        this.type = type;
        this.body = body;
        this.timestamp = timestamp;
        this.read = read;
        this.delayedTimestamp = delayedTimestamp;
        this.deliveryStatus = deliveryStatus;
    }

    public Date getDelayedTimestamp() {
        return delayedTimestamp;
    }

    public void setDelayedTimestamp(Date delayedTimestamp) {
        this.delayedTimestamp = delayedTimestamp;
    }

    public int getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(int deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public String getReceivedFrom() {
        return receivedFrom;
    }

    public void setReceivedFrom(String receivedFrom) {
        this.receivedFrom = receivedFrom;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getSentTo() {
        return sentTo;
    }

    public void setSentTo(String sentTo) {
        this.sentTo = sentTo;
    }

    public Boolean getRead() {
        return read;
    }

    public void setRead(Boolean read) {
        this.read = read;
    }
}
