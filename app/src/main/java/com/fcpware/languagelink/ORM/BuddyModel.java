package com.fcpware.languagelink.ORM;

import android.util.Log;

import com.fcpware.languagelink.XMPP.vcard.packet.VCard;
import com.fcpware.languagelink.models.FCPLanguageModel;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import org.jivesoftware.smack.packet.Presence;

import java.util.ArrayList;

import static org.jivesoftware.smack.roster.packet.RosterPacket.ItemStatus;
import static org.jivesoftware.smack.roster.packet.RosterPacket.ItemType;

/**
 * The buddy model is the class we use as a to save and reference information about our friends
 * It extends  SugarRecord<ChatSummary> so is savable
 *
 * It saves:
 * Buddies jid
 * Buddies VCard
 *
 * Information from the users vcards can be accessed from here
 *
 */

//todo make sure languages and intro are updated

public class BuddyModel extends SugarRecord<BuddyModel>{
    @Ignore private static final String TAG = "FCPUserModel";

    //Allready exists in friend profile activity
    @Ignore public static int FRIEND_REQUEST = 2;
    @Ignore public static int FRIEND = 3;
    @Ignore public static int PENDING_REQUEST = 1;

    private String jid;
    private byte[] avatar;
    private String intro;
    private String nickname;
    private int friendStatus;
    //todo change to private
    public String languagesString;

    //information about the user we wont save
    @Ignore private Presence presence = new Presence(Presence.Type.unavailable); //default

    public BuddyModel(){

    }

    public BuddyModel(String jid, VCard vCard) {
        this.jid = jid;

        setWithVCard(vCard);
    }

    public byte[] getAvatar() {
        return avatar;
    }

    public void setAvatar(byte[] avatar) {
        Log.d(TAG, "Avatar set :");

        this.avatar = avatar;
        this.save();
    }

    public String getNickName() {
        return nickname;
    }

    public String getIntro() {
        return intro;
    }

    public void setNickName(String nickName) {
        this.nickname = nickName;
        this.save();
    }

    public void setLanguages(ArrayList<FCPLanguageModel> languages) {
        Log.d(TAG, "new languages size : " + languages.size());
        Log.d(TAG, "old languages size : " + getLanguages().size());

        this.languagesString = new Gson().toJson(languages);
        this.save();
    }

    public ArrayList getLanguages() {

        //TODO move this to a method that is used on creation
        //TODO then normally pass back an array list

        Gson gson = new Gson();
        JsonParser parser = new JsonParser();
        JsonArray jArray = parser.parse(languagesString).getAsJsonArray();

        ArrayList<FCPLanguageModel> languages = new ArrayList<FCPLanguageModel>();

        for(JsonElement obj : jArray ){
            FCPLanguageModel language = gson.fromJson( obj , FCPLanguageModel.class);
            languages.add(language);
        }

        return languages;
    }


    public Presence getPresence() {

        return presence;
        //return presence == null ? new Presence(Presence.Type.unavailable) : presence;

    }

    public void setPresence(Presence presence) {
        this.presence = presence;

    }

    public String getJid() {
        return jid;
    }

    public void setJabberId(String jid) {

        this.jid = jid;
        this.save();
    }

    public void setWithVCard(VCard vCard) {
        avatar = vCard.getAvatar();
        nickname = vCard.getNickName();
        intro = vCard.getIntro();
        jid = vCard.getJabberId();
        languagesString = new Gson().toJson(vCard.getLanguages());
    }

    public int getFriendStatus() {
        return friendStatus;
    }

    public void setFriendStatus(ItemType type, ItemStatus status) {

        //TODO some of these sates are not needed
        if(type== ItemType.none&&status== ItemStatus.subscribe||type== ItemType.to)
        {
            friendStatus = PENDING_REQUEST;
        } else if(type== ItemType.none&&status==null
                ||type== ItemType.from&&status!= ItemStatus.subscribe){
            friendStatus = FRIEND_REQUEST;
        } else {
            friendStatus = FRIEND;
        }

        this.save();
    }


}
