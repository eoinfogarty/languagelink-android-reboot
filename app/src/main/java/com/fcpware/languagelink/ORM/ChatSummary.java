package com.fcpware.languagelink.ORM;


import com.orm.SugarRecord;

import java.util.Date;

/**
 * This class encapsulates the summary information of a chat between users
 *
 * We have the Jid so we can get our buddies avatar and nickname
 * We have the last message received in the conversation so we can get the body of the message and the date it was received
 * Finally we have a count of all the unread messages in the conversation
 *
 */
public class ChatSummary extends SugarRecord<ChatSummary> {

    private String avatar;
    private String userJid; //so we can handle different users logging in and out
    private String buddyJid;
    private String nickname;
    private long unreadMessages;
    private Date lastMessageDate;
    private String lastMessageBody;

    public ChatSummary(){

    }

    public ChatSummary(String avatar, String userJid, String buddyJid, String nickname, long unreadMessages, Date lastMessageDate,  String lastMessageBody ) {

        this.avatar = avatar;
        this.userJid = userJid;
        this.buddyJid = buddyJid;
        this.nickname = nickname;
        this.unreadMessages = unreadMessages;
        this.lastMessageDate = lastMessageDate;
        this.lastMessageBody = lastMessageBody;

    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getBuddyJid() {
        return buddyJid;
    }

    public void setBuddyJid(String buddyJid) {
        this.buddyJid = buddyJid;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public long getUnreadMessages() {
        return unreadMessages;
    }

    public void setUnreadMessages(long unreadMessages) {
        this.unreadMessages = unreadMessages;
    }

    public Date getLastMessageDate() {
        return lastMessageDate;
    }

    public void setLastMessageDate(Date lastMessageDate) {
        this.lastMessageDate = lastMessageDate;
    }

    public String getLastMessageBody() {
        return lastMessageBody;
    }

    public void setLastMessageBody(String lastMessageBody) {
        this.lastMessageBody = lastMessageBody;
    }

    public String getUserJid() {
        return userJid;
    }

    public void setUserJid(String userJid) {
        this.userJid = userJid;
    }
}
