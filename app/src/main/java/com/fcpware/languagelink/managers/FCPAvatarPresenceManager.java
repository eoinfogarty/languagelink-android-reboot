package com.fcpware.languagelink.managers;

import android.content.Context;
import android.util.Log;

import com.fcpware.languagelink.FCPConnection;
import com.fcpware.languagelink.ORM.BuddyModel;
import com.fcpware.languagelink.XMPP.vcard.packet.VCardTempXUpdatePresenceExtension;
import com.fcpware.languagelink.models.FCPUserModel;

import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.filter.AndFilter;
import org.jivesoftware.smack.filter.PacketExtensionFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.DefaultPacketExtension;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jxmpp.util.XmppStringUtils;

/**
 * Presence manager that intercepts all presence updates and add the vcard avatar hash
 */
public class FCPAvatarPresenceManager {
    private static final String TAG = "FCPAvatarPresenceManager";

    // Singleton variable
    private static FCPAvatarPresenceManager instance = null;

    private VCardTempXUpdatePresenceExtension vCardTempXUpdatePresenceExtension = null;

    private FCPAvatarManager fcpAvatarModel;
    private FCPConnection fcpConnection;
    private FCPUserModel userModel;
    private Context context;


    public synchronized static FCPAvatarPresenceManager getInstance(Context context) {
        if (instance == null) {
            instance = new FCPAvatarPresenceManager(context);
        }
        return instance;
    }

    public FCPAvatarPresenceManager(Context context) {
        Log.d(TAG, "Creating FCPAvatarPresenceManager");
        this.context = context;

        userModel = FCPUserModel.getInstance();
        fcpAvatarModel = FCPAvatarManager.getInstance(this.context);
        fcpConnection = FCPConnection.getInstance();

        // Creates the presence extension to generates the  the element
        // name "x" and the namespace "vcard-temp:x:update" containing
        // the avatar SHA-1 hash.
        vCardTempXUpdatePresenceExtension =
                new VCardTempXUpdatePresenceExtension(userModel.getAvatar());

        fcpConnection.getConnection().addPacketInterceptor(vCardTempXUpdatePresenceExtension, new PacketTypeFilter(Presence.class));
        createContactPhotoPresenceListener();

    }


    /**
     * Creates a listener to call a parser which manages presence packets with
     * the element name "x" and the namespace "vcard-temp:x:update".
     */
    public void createContactPhotoPresenceListener() {
        // Registers the listener.
        fcpConnection.getConnection().addPacketListener(
                new PacketListener() {
                    public void processPacket(Packet packet) {
                        // Calls the parser to manages this presence packet.
                        //parseContactPhotoPresence(packet);
                        Log.d(TAG, "processPacket - We got a vcard-temp:x:update");
                        parseContactPhotoPresence(packet);
                    }
                },
                // Creates a filter to only listen to presence packet with the
                // element name "x" and the namespace "vcard-temp:x:update".
                new AndFilter(new PacketTypeFilter(Presence.class),
                        new PacketExtensionFilter(
                                VCardTempXUpdatePresenceExtension.ELEMENT_NAME,
                                VCardTempXUpdatePresenceExtension.NAMESPACE)
                )
        );
    }

    /**
     * Parses a contact presence  with the element name "x" and the
     * namespace "vcard-temp:x:update", in order to decide if the SHA-1 avatar
     * contained in the photo tag represents a new avatar for this contact.
     *
     * @param packet The presence received to parse.
     */
    public void parseContactPhotoPresence(Packet packet) {
        String userID = XmppStringUtils.parseBareJid(packet.getFrom());
        Log.d(TAG, "parseContactPhotoPresence - Checking if  " + userID +  "'s avatar needs updating");
        FCPBuddyManager fcpBuddyManager = FCPBuddyManager.getInstance(context);
        BuddyModel user = fcpBuddyManager.getBuddy(userID);

        /**
         * If this contact is not yet in our contact list, then there is no need
         * to manage this photo update.
         */
        if (user == null) {
            Log.d(TAG, "parseContactPhotoPresence - Not on contact list , must be yours");
            return;
        }
        Log.d(TAG, "parseContactPhotoPresence - Getting current avatar");
        byte[] currentAvatar = user.getAvatar();

        // Get the packet extension which contains the photo tag.
        DefaultPacketExtension defaultPacketExtension =
                (DefaultPacketExtension) packet.getExtension(
                        VCardTempXUpdatePresenceExtension.ELEMENT_NAME,
                        VCardTempXUpdatePresenceExtension.NAMESPACE);

        Log.d(TAG, "parseContactPhotoPresence - the packet extension : " + defaultPacketExtension.toXML());
        String packetPhotoSHA1 = defaultPacketExtension.getValue("photo");
        Log.d(TAG, "parseContactPhotoPresence - just the SHA " + packetPhotoSHA1);
        // If this presence packet has a photo tag with a SHA-1 hash
        // which differs from the current avatar SHA-1 hash
        if (packetPhotoSHA1 != null &&
                !packetPhotoSHA1.equals(VCardTempXUpdatePresenceExtension.getImageSha1(currentAvatar))) {
            Log.d(TAG, "parseContactPhotoPresence - Hashes are different setting vcard and downloading avatar");
            //we have to pull the new vcard
            fcpBuddyManager.setBuddy(userID);
            //download the new avatar from amazon
            userID = fcpAvatarModel.parseEmailFromEntry(userID);
            fcpAvatarModel.saveAvatars(new String[]{userID});
            //TODO and send a callback to anybody listening that this user has a new avatar
        }

    }

    /**
     * Updates the presence extension to advertise a new photo SHA-1 hash
     * corresponding to the new avatar given in parameter.
     *
     * @param byteArray The new avatar set for this account.
     */
    public void publishAvatarUpdatePresence(byte[] byteArray) {
        // If the image has changed, then updates the presence extension and
        // send immediately a presence packet to advertise the photo update.
        //TODO doesnt work
        if (vCardTempXUpdatePresenceExtension.updateImage(byteArray)) {
            try {
                Presence presence = new Presence(Presence.Type.available);
                fcpConnection.getConnection().sendPacket(presence);
            } catch (SmackException.NotConnectedException e) {
                e.printStackTrace();
            }
        }
    }
}
