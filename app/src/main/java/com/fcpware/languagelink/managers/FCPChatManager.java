package com.fcpware.languagelink.managers;

import android.content.Context;
import android.util.Log;

import com.fcpware.languagelink.FCPConnection;
import com.fcpware.languagelink.ORM.SavableMessage;
import com.fcpware.languagelink.events.BusProvider;
import com.fcpware.languagelink.events.NewChatEvent;
import com.fcpware.languagelink.events.NewMessageEvent;

import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.chat.ChatManagerListener;
import org.jivesoftware.smack.chat.ChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jxmpp.util.XmppStringUtils;

import java.util.Date;
import java.util.HashMap;

/**
 * Handles chat and summaries
 */
public class FCPChatManager implements ChatManagerListener, ChatMessageListener {

    private static final String TAG = "FCPChatManager";

    // Singleton variable
    private static FCPChatManager instance = null;

    private FCPNotificationManager fcpNotificationManager;
    private FCPBuddyManager fcpBuddyManager;
    private ChatManager chatmanager;

    private HashMap<String, Chat> chats = new HashMap<String, Chat>();


    public synchronized static FCPChatManager getInstance(Context context) {
        if(instance==null){
            instance = new FCPChatManager(context);
        }
        return instance;
    }

    public FCPChatManager(Context context){
        BusProvider.getInstance().register(this);
        Log.d(TAG, "Creating the ChatManager");

        FCPConnection fcpConnection = FCPConnection.getInstance();
        chatmanager = ChatManager.getInstanceFor(fcpConnection.getConnection());
        fcpNotificationManager = FCPNotificationManager.getInstance(context);

        fcpBuddyManager = FCPBuddyManager.getInstance(context);
        chatmanager.setMatchMode(ChatManager.MatchMode.SUPPLIED_JID);
        chatmanager.addChatListener(this);
    }

    @Override
    public void chatCreated(Chat newChat, boolean createdLocally) {
        Log.d(TAG , "Chat created start : " + chats.size());

        if(!createdLocally) {
            Log.d(TAG , "Not a local chat adding listener");
            //check if a chat all ready exists with that user and replace the chat
            newChat.addMessageListener(this);

        }

        Log.d(TAG, "Chat details  participant: "  + newChat.getParticipant());
        Log.d(TAG, "Thread ID: "  + newChat.getThreadID());
        Log.d(TAG, "Listeners: "  + newChat.getListeners().size());

        chats.put(XmppStringUtils.parseBareJid(newChat.getParticipant()), newChat);
        BusProvider.getInstance().post(new NewChatEvent(newChat.getParticipant(), newChat));
        Log.d(TAG , "Chat created finish chats size: " + chats.size());
    }

    @Override
    public void processMessage(Chat chat, Message message) {

        if(message.getBody()!=null) {
            String from = XmppStringUtils.parseBareJid(message.getFrom());
            saveMessage(message, from, false, from);
            fcpNotificationManager.messageNotification(fcpBuddyManager.getBuddy(from), message);
        }
    }

    public SavableMessage saveMessage(Message message, String from, Boolean read, String chatPartner) {
        Log.d(TAG, "saveMessage - from " + from );
        Log.d(TAG, "saveMessage - to " + XmppStringUtils.parseBareJid(message.getTo()));
        Log.d(TAG, "saveMessage - type " +  message.getType().name());
        Log.d(TAG, "saveMessage - read " + read );
        Log.d(TAG, "saveMessage - chatPartner " + chatPartner );

        SavableMessage messageORM = new SavableMessage(XmppStringUtils.parseBareJid(from),
                XmppStringUtils.parseBareJid(message.getTo()),
                message.getType().name(),
                message.getBody(),
                new Date(),
                read, //read status
                new Date(), //delayed timestamp
                0);   // delivery status

        Log.d(TAG, "saveMessage - " + messageORM.getBody());

        messageORM.save();
        BusProvider.getInstance().post(new NewMessageEvent(chatPartner));   //inform chat summary manager
        return messageORM;
    }

    public Chat getChat(String jabberId) {
        Log.d(TAG, "Getting chat for : " + jabberId);
        for(Chat chat:chats.values())
        {
            Log.d(TAG, "chat : " + chat.getParticipant());
            Log.d(TAG, "chat : " + chat.getThreadID());
        }
        if(chats.get(jabberId)!=null)
        {
            Log.d(TAG, "Chat found and returning");
            Log.d(TAG, "thread" + chats.get(jabberId).getThreadID());
            return chats.get(jabberId);
        }
        else {
            Log.d(TAG, "Creating new chat");
            return chatmanager.createChat(jabberId,this);
        }

    }
}
