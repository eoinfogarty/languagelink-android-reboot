package com.fcpware.languagelink.managers;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.fcpware.languagelink.ORM.BuddyModel;
import com.fcpware.languagelink.R;
import com.fcpware.languagelink.UI.chat.ChatActivity;
import com.fcpware.languagelink.UI.friends.FriendProfileActivity;
import com.squareup.picasso.Picasso;

import org.jivesoftware.smack.packet.Message;
import org.jxmpp.util.XmppStringUtils;

import java.io.File;
import java.io.IOException;

/**
 * The class that handles notifications you receive in the status bar
 */
public class FCPNotificationManager {

    private static final String TAG = "FCPNotificationManager";

    private static final int FRIEND_REQUEST_ID = 0;
    private static final int MESSAGE_NOTIFICATION_ID = 1;

    private static FCPNotificationManager instance = null;  //singleton variable
    private NotificationManager notificationManager;  // android service
    private NotificationCompat.Builder mBuilder;
    private Context context;

    // Notification defaults
    // private long[] vibratePattern = {0,120,120,360,120,120,120,120,360,120,120,360,120,120,120,120};     // "ll" in morse code
    private long[] vibratePattern = {0,500,110,500,110,450,110,200,110,170,40,450,110,200,110,170,40,500};    // star wars
    private Uri notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    private int notificationSmallIcon = R.drawable.ic_launcher;
    private int notificationLedColor = Color.BLUE;
    private int notificationOffMs = 1000;
    private int notificationOnMs = 1000;

    public synchronized static FCPNotificationManager getInstance(Context context) {
        if(instance==null){
            instance = new FCPNotificationManager(context);
        }
        return instance;
    }

    public FCPNotificationManager(Context context){
        Log.d(TAG, "constructor called");
        this.context = context;
        // Gets an instance of the NotificationManager service
        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    public void friendRequestNotification(final BuddyModel request){
        Log.d(TAG, "friendRequestNotification - friend Request");

        String friendRequest = context.getString(R.string.you_got_a_friend_request);
        String friendRequestFrom = context.getString(R.string.you_got_a_friend_request_from) + " " + request.getNickName() ;

        final NotificationCompat.Builder mBuilder
                = createNotificationBuilder(friendRequest, friendRequestFrom);

        String parsedEntry = XmppStringUtils.parseLocalpart(request.getJid());
        parsedEntry = XmppStringUtils.unescapeLocalpart(parsedEntry);
        //load from url from a thread

        final String finalParsedEntry = parsedEntry;
        Thread thread = new Thread()
        {
            @Override
            public void run() {
                try {
                    mBuilder.setLargeIcon(Picasso.with(context)
                            //todo move to constant
                            .load("https://s3.amazonaws.com/language-exchange/avatars/" + finalParsedEntry)
                            .resizeDimen(R.dimen.notification_icon_width_height, R.dimen.notification_icon_width_height)
                            .get());

                    Intent resultIntent = new Intent(context, FriendProfileActivity.class);
                    resultIntent.putExtra("resultEmailAddress", XmppStringUtils.parseLocalpart(request.getJid()));
                    resultIntent.putExtra("EXTRA_LAUNCHED_BY_NOTIFICATION", true);
                    //resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                    //TODO Should create a backstack
                    PendingIntent resultPendingIntent =
                            PendingIntent.getActivity(
                                    context,
                                    2332,
                                    resultIntent,
                                    PendingIntent.FLAG_ONE_SHOT
                            );
                    mBuilder.setContentIntent(resultPendingIntent);

                    // Builds the notification and issues it.
                    notificationManager.notify(FRIEND_REQUEST_ID, mBuilder.build());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };

        thread.start();
    }

    public void messageNotification(final BuddyModel sender, Message received) {

        Log.d(TAG, "messageNotification :" + received.getBody() + " from " + received.getFrom());
        mBuilder =  createNotificationBuilder(sender.getNickName(), received.getBody());

        //TODO might be null, especially when the connection is restarted or cache is cleared and the buddylist is not restarted yet
        // todo move to a thread

        //Load a a file
        String parsedEntry = XmppStringUtils.parseLocalpart(sender.getJid());
        parsedEntry = XmppStringUtils.unescapeLocalpart(parsedEntry);

        final String finalParsedEntry = parsedEntry;
//        final File avatar = new File(context.getCacheDir(), parsedEntry);

        Thread thread = new Thread()
        {
            @Override
            public void run() {
                try { //TODO avatar not showing
                    mBuilder.setLargeIcon(Picasso.with(context)
                            //todo move to constant
                            .load("https://s3.amazonaws.com/language-exchange/avatars/" + finalParsedEntry)
                            .resizeDimen(R.dimen.notification_icon_width_height, R.dimen.notification_icon_width_height)
                            .get());

                    Intent resultIntent = new Intent(context, ChatActivity.class);
                    resultIntent.putExtra("friendJabberId", sender.getJid());
                    resultIntent.putExtra("EXTRA_LAUNCHED_BY_NOTIFICATION", true);

                    //TODO Should create a backstack
                    PendingIntent resultPendingIntent =
                            PendingIntent.getActivity(
                                    context,
                                    32223,
                                    resultIntent,
                                    0
                            );
                    mBuilder.setContentIntent(resultPendingIntent);

                    // Builds the notification and issues it.
                    notificationManager.notify(MESSAGE_NOTIFICATION_ID, mBuilder.build());
                    //mBuilder.setLargeIcon(Picasso.with(context).load(avatar).resizeDimen(R.dimen.notification_icon_width_height, R.dimen.notification_icon_width_height).get());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };

        thread.start();


    }

    /**
     * builds a notification with parameters provided and defaults used bu every notification
     * @param contentTitle
     * @param contentText
     * @return
     */

    private NotificationCompat.Builder createNotificationBuilder(String contentTitle, String contentText) {

        return new NotificationCompat.Builder(context)
                        //TODO Should be black and white now
                        .setSmallIcon(notificationSmallIcon)
                        .setContentTitle(contentTitle)
                        .setContentText(contentText)
                        .setVibrate(vibratePattern)
                        .setSound(notificationSound)
                        .setLights(notificationLedColor, notificationOnMs, notificationOffMs)
                        // hide the notification after its selected
                        .setAutoCancel(true);
    }

    public void friendRequestConfirmed(final BuddyModel buddyModel) {

        Log.d(TAG, "friendRequestConfirmed :" + buddyModel.getNickName());
        mBuilder =  createNotificationBuilder(buddyModel.getNickName(), buddyModel.getNickName() + context.getString(R.string.confirmed_friend_request));

        //TODO might be null, especially when the connection is restarted or cache is cleared and the buddylist is not restarted yet
        //Load a a file
        String parsedEntry = XmppStringUtils.parseLocalpart(buddyModel.getJid());
        parsedEntry = XmppStringUtils.unescapeLocalpart(parsedEntry);
        final File avatar = new File(context.getCacheDir(), parsedEntry);
        final String finalParsedEntry = parsedEntry;
        Thread thread = new Thread()
        {
            @Override
            public void run() {
            try {
                    //todo check for file and if no file get from amazon
                    mBuilder.setLargeIcon(Picasso.with(context)
                            //todo move to constant
                            .load("https://s3.amazonaws.com/language-exchange/avatars/" + finalParsedEntry)
                            .resizeDimen(R.dimen.notification_icon_width_height, R.dimen.notification_icon_width_height)
                            .get());
                } catch (IOException e) {
                    e.printStackTrace();
                }


            Intent resultIntent = new Intent(context, ChatActivity.class);
            resultIntent.putExtra("friendJabberId", buddyModel.getJid());
            resultIntent.putExtra("EXTRA_LAUNCHED_BY_NOTIFICATION", true);

            //TODO Should create a backstack
            PendingIntent resultPendingIntent =
                    PendingIntent.getActivity(
                            context,
                            32223,
                            resultIntent,
                            0
                    );
            mBuilder.setContentIntent(resultPendingIntent);

            // Builds the notification and issues it.
            notificationManager.notify(MESSAGE_NOTIFICATION_ID, mBuilder.build());
        }
    };

    thread.start();
    }
}
