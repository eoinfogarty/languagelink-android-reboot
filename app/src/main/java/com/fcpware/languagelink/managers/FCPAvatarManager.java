package com.fcpware.languagelink.managers;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;

import com.fcpware.languagelink.FCPConnection;
import com.fcpware.languagelink.amazons3.network.TransferController;
import com.fcpware.languagelink.events.AvatarUploadedEvent;
import com.fcpware.languagelink.events.BusProvider;
import com.fcpware.languagelink.models.FCPUserModel;
import com.fcpware.languagelink.utilities.ImageResize;
import com.squareup.otto.Subscribe;

import org.jxmpp.util.XmppStringUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * the uploading/downloading of avatar image
 * the scaling of avatar images for upload/download
 * the scaling of avatar images for vcards
 * the hashing of avatar images for vcards
 * comparing the avatar hashs of presence updates with the vcard
 */
public class FCPAvatarManager {
    private static final String TAG = "FCPAvatarManager";

    // Singleton variable
    private static FCPAvatarManager instance = null;
    private FCPConnection fcpConnection;
    private Context mContext;

    public synchronized static FCPAvatarManager getInstance(Context context) {
        if(instance==null){
            instance = new FCPAvatarManager(context);
        }
        return instance;
    }

    public FCPAvatarManager(Context context){
        Log.d(TAG, "Creating FCPAvatarManager");
        mContext = context;

        fcpConnection = FCPConnection.getInstance();
    }

    public void setAvatars(Collection<String> friendsJids) {
        saveAvatars(getAvatarsNeeded(friendsJids));
    }

    /**
     * saves your friends avatar from amazon to apps private cache directory
     *
     */
    public void saveAvatars(String[] friendsJids) {
        Log.d(TAG, "saveAvatars - Downloading avatar to cache directory - " + Arrays.toString(friendsJids));
        TransferController.download(mContext, friendsJids);
        saveThumbnailAvatarHashes(friendsJids);

    }

    /**
     * here we save the hashes of the thumbnails of the avatar in our friends vcard
     * we need this hash to check if we need to download a new avatar when we get the vcard
     *
     * @param friendsJids
     */
    private void saveThumbnailAvatarHashes(String[] friendsJids) {

    }

    /**
    * Check the apps cache directory for friends avatars
    * If the avatar is not in the cache directory we add it to a list of avatars to be downloaded
     */
    private String[] getAvatarsNeeded(Collection<String> friendsJids) {
        ArrayList<String> avatarsToDownload = new ArrayList<String>();

        //For Debug: show me the files that all ready exist in the cache directory
        Log.d(TAG, "checkForWhoNeedsAvatars - These are the files allready in the cache directory ");
        File dir = new File(mContext.getCacheDir(), "/");
        if (dir.exists()) {
            for (File f : dir.listFiles()) {
                Log.d(TAG, " " + f.getName());
            }
        }

        for(String entry : friendsJids){
            String parsedEntry = parseEmailFromEntry(entry);
            Log.d(TAG, "checkForWhoNeedsAvatars - Checking for file for - " + parsedEntry);
            File download = new File(mContext.getCacheDir(), parsedEntry);
            if(!download.exists()){
                Log.d(TAG, "checkForWhoNeedsAvatars - File does not exist adding to list for download");
                avatarsToDownload.add(parsedEntry);
            }
        }

        return avatarsToDownload.toArray(new String[avatarsToDownload.size()]);
    }

    /**
    * Delete the avatar image file from the cache directory if it exists
     */
    public void removeAvatar(String deletedEntry) {

        String parsedEntry = parseEmailFromEntry(deletedEntry);
        Log.d(TAG, "removeAvatar - looking for avatar file for " + parsedEntry);
        File dir = new File(mContext.getCacheDir(), "/");
        if (dir.exists()) {
            for (File file : dir.listFiles()) {
                if(file.getName().equals(parsedEntry)){
                    Log.d(TAG, "removeAvatar - Found the file deleting now");
                    boolean deleted = file.delete();
                    if(!deleted){
                        Log.e(TAG, "removeAvatar - File was not deleted");
                    }
                }
            }
        }
    }

    /**
     *  We have to parse and unescape your friends jabberId so it will become their email
     * ie eoin/40example.com@54.54.54.54 becomes eoin@example.com
     */
    public String parseEmailFromEntry(String entry) {

        Log.d(TAG, " " +entry);

        entry = XmppStringUtils.parseLocalpart(entry);
        entry = XmppStringUtils.unescapeLocalpart(entry);

        return entry;
    }

    /**
    * Uploads a users Avatar to amazon with the users email address
    * Also saves the same file in the cache directory
     */
    public void saveUsersAvatar(String username, Bitmap bitmap) {
        //first save the avatar to cache
        File cacheDir = mContext.getCacheDir();
        if(!cacheDir.exists()){
            cacheDir.getParentFile().mkdir();
        }

        String parsedEntry = parseEmailFromEntry(username);
        File avatar = new File(cacheDir, parsedEntry);

        try {
                FileOutputStream fOut = new FileOutputStream(avatar);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fOut);

                fOut.flush();
                fOut.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        // then upload to amazon
        ByteArrayOutputStream amazonStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, amazonStream);
        TransferController.upload(mContext, Uri.fromFile(avatar));

        //from here we register to listen for AvatarUploadedEvents with the username
        BusProvider.getInstance().register(this);


    }

    @Subscribe
    public void onAvatarUploaded(AvatarUploadedEvent event) {
        Log.d(TAG, "onAvatarUploaded - " + event.key);
        //scale it to vcard size 32x32
        ImageResize resize = new ImageResize(mContext, 32, 32, event.avatar);
        Bitmap vcardBitmap = resize.getResizeImage();
        //set the vcard
        FCPUserModel user = FCPUserModel.getInstance();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        vcardBitmap.compress(Bitmap.CompressFormat.PNG, 80, stream);
        byte[] byteArray = stream.toByteArray();

        user.setAvatar(byteArray);
        user.save(fcpConnection.getConnection(), mContext);

        SharedPreferencesManager userLoginDetails = new SharedPreferencesManager(mContext);
        userLoginDetails.setLoginData(fcpConnection.getConnection(), user.getVCard());
    }

    public File getAvatar(String jabberId) {

        //TODO cache might be cleared and there will be no avatar
        Log.d(TAG, "Getting avatar for " + jabberId);
        String parsedEntry = parseEmailFromEntry(jabberId);

        return new File(mContext.getCacheDir(), parsedEntry);
    }
}
