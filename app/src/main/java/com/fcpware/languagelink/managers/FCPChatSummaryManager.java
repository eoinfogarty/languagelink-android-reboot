package com.fcpware.languagelink.managers;

import android.content.Context;
import android.util.Log;

import com.fcpware.languagelink.FCPConnection;
import com.fcpware.languagelink.ORM.BuddyModel;
import com.fcpware.languagelink.ORM.ChatSummary;
import com.fcpware.languagelink.ORM.SavableMessage;
import com.fcpware.languagelink.events.BusProvider;
import com.fcpware.languagelink.events.NewAvatarEvent;
import com.fcpware.languagelink.events.NewMessageEvent;
import com.fcpware.languagelink.events.NewSummaryEvent;
import com.fcpware.languagelink.events.SummaryUpdateEvent;
import com.fcpware.languagelink.models.FCPUserModel;
import com.squareup.otto.Subscribe;

import org.jxmpp.util.XmppStringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * This class handles all summaries of chats.
 * It will listen for the updates needed for its summaries
 * It will then send an event to the ui telling it it update/refresh
 * <p/>
 * events to listen for:
 * new messages
 * new avatar
 * new nickname //TODO
 * <p/>
 * events to produce
 * summary update
 */
public class FCPChatSummaryManager {

    private static final String TAG = "FCPChatSummaryManager";

    public static final int UPDATE_AVATAR = 0;
    public static final int UPDATE_MESSAGE = 1;
    public static final int UPDATE_NICKNAME = 2;

    private static FCPChatSummaryManager instance = null;

    private List<ChatSummary> summaries = new ArrayList<ChatSummary>();
    private FCPBuddyManager buddyManager;
    private FCPUserModel userModel;
    private FCPConnection fcpConnection;


    public synchronized static FCPChatSummaryManager getInstance(Context context) {
        if (instance == null) {
            instance = new FCPChatSummaryManager(context);
        }
        return instance;
    }

    public FCPChatSummaryManager(Context context) {
        BusProvider.getInstance().register(this); //otto
        Log.d(TAG, "Creating the FCPChatSummaryManager");
        buddyManager = FCPBuddyManager.getInstance(context);
        userModel = FCPUserModel.getInstance();
        Log.d(TAG, "Userjid = " +  FCPUserModel.getInstance().getJabberId());
        Log.d(TAG, "Userjid = " +  FCPUserModel.getInstance().getNickName());
        //get all chat summaries that are mine
        //todo sometimes null
        summaries = ChatSummary.find(ChatSummary.class, " user_jid = ? " , FCPUserModel.getInstance().getJabberId());
        //summaries = ChatSummary.listAll(ChatSummary.class);

        for (ChatSummary summary : summaries) {
            Log.d(TAG, "Summary : " + summary.getNickname());
        }
        sortSummariesByDate();
    }

    private long getUnreadMessageCount(String jid) {
        return SavableMessage.count(SavableMessage.class, " received_from = ? AND sent_to = ? AND read = ? ", new String[]{jid, userModel.getJabberId() , "0"});
    }

    public List<ChatSummary> getSummaries() {
        Log.d(TAG, " Returning summaries : " + summaries.size());
        return summaries;
    }

    // we want to get the last message by timestamp but it will come back as a list first
    public SavableMessage getLastMessage(String jid) {
        List<SavableMessage> lastMessageList = SavableMessage.find(SavableMessage.class,
                                                                   " (sent_to = ? AND received_from = ?) or (sent_to = ? AND received_from = ?) ",
                                                                    new String[]{jid, userModel.getJabberId(), userModel.getJabberId() , jid},
                                                                    "", "timestamp DESC", "1");

        return lastMessageList.get(0);
    }

    /**
     *  Delete a summary object when a friend is removed
     */
    public void deleteSummary(String user){
        Log.d(TAG, "deleteSummary - for " + user);
        for(ChatSummary summary: summaries){
            if(summary.getBuddyJid().equals(user)){
                ChatSummary.deleteAll(ChatSummary.class, "user_jid = ? AND buddy_jid = ?", userModel.getJabberId() , user );
                summaries.remove(summary);
                BusProvider.getInstance().post(new NewSummaryEvent(null));
                break;
            }
        }
    }

    /**
     * Make a new summary object for a new chat
     *
     * @param participant
     */
    public void createSummary(String participant) {

        Log.d(TAG, "Creating a new summary for = " + participant);
        // get the nickname of the user we are talking to
        BuddyModel buddy = buddyManager.getBuddy(participant);
        String nickname = buddy.getNickName();
        Log.d(TAG, "buddy nickname = " + nickname);

        ChatSummary newChatSummary = new ChatSummary();

        Log.d(TAG, "createSummary - avatar - " + buddyManager.getAvatarUriString(participant));
        newChatSummary.setAvatar(buddyManager.getAvatarUriString(participant));
        newChatSummary.setUserJid(userModel.getJabberId());
        newChatSummary.setBuddyJid(participant);
        newChatSummary.setNickname(nickname);

        // get the body , the date and the unread count
        SavableMessage lastMessage = getLastMessage(participant);
        newChatSummary.setLastMessageBody(lastMessage.getBody());
        newChatSummary.setLastMessageDate(lastMessage.getTimestamp());

        long unreadMessageCount = getUnreadMessageCount(participant);
        Log.d(TAG, "createSummary -unread message count : " + unreadMessageCount);
        newChatSummary.setUnreadMessages(unreadMessageCount);
        newChatSummary.save();

        summaries.add(newChatSummary);
        sortSummariesByDate();

        //TODO merge I need only one post,
        // send an event to anybody listening to update ie the chat list view
        BusProvider.getInstance().post(new NewSummaryEvent(newChatSummary));
    }

    /**
     * When a new avatar has been downloaded we will check if one of our chat summaries needs to hear about his update
     * We also need to update our summary object
     *
     * @param event - New Avatar Event
     */
    @Subscribe
    public void onAvatarChange(NewAvatarEvent event) {
        Log.d(TAG, "onAvatarChange - " + event.username);
        for (ChatSummary chatSummary : summaries) {
            String parsedJid = XmppStringUtils.escapeLocalpart(XmppStringUtils.parseLocalpart(chatSummary.getBuddyJid()));

            if (parsedJid.equals(event.username)) {
                Log.d(TAG, "onAvatarChange - avatar for a summary has changed we need to update");
                chatSummary.setAvatar(buddyManager.getAvatarUriString(chatSummary.getBuddyJid()));
                chatSummary.save();

                // send an event to anybody listening to update
                BusProvider.getInstance().post(new SummaryUpdateEvent(chatSummary, UPDATE_AVATAR));
            }
        }
    }

    /**
     * All new messages will pass through here to give the manager a chance to update its summaries
     * <p/>
     * We should also check here if a new summary needs to be created
     *
     * @param message - New Avatar Event
     */
    @Subscribe
    public void onNewMessage(NewMessageEvent message) {
        Log.d(TAG, "onNewMessage - " + message.username);
        String parsedJid = XmppStringUtils.parseBareJid(message.username);

        Boolean alreadyExist = false;
        for (ChatSummary chatSummary : summaries) {


            if (parsedJid.equals(chatSummary.getBuddyJid())&&userModel.getJabberId().equals(chatSummary.getUserJid())) {
                Log.d(TAG, "onNewMessage - got the chat ");
                // get the body , the date and the unread count
                SavableMessage lastMessage = getLastMessage(chatSummary.getBuddyJid());
                chatSummary.setLastMessageBody(lastMessage.getBody());
                chatSummary.setLastMessageDate(lastMessage.getTimestamp());

                long unreadMessageCount = getUnreadMessageCount(chatSummary.getBuddyJid());
                chatSummary.setUnreadMessages(unreadMessageCount);

                chatSummary.save();
                alreadyExist = true;

                // send an event to anybody listening to update
                BusProvider.getInstance().post(new SummaryUpdateEvent(chatSummary, UPDATE_MESSAGE));

                if(sortSummariesByDate()){
                    //the order of the summaries has been updated so ui should update too
                    BusProvider.getInstance().post(new NewSummaryEvent(chatSummary));
                }
            }
        }

        if (!alreadyExist) {
            createSummary(parsedJid);
        }
    }

    /**
     *  returns boolean to tell if the list actually needed sorting
     */

    private Boolean sortSummariesByDate() {
        List<ChatSummary> unsortedSummaries = new ArrayList<ChatSummary>(summaries);
        Collections.sort(summaries, new LastActiveComparator());

        Boolean hasBeenSorted = !unsortedSummaries.equals(summaries);
        unsortedSummaries.clear();

        Log.d(TAG, "sortSummariesByDate - sorted : " + hasBeenSorted);
        return hasBeenSorted;
    }

    /**
     * We want to order our list of chats by most recent so we need this comparator to rearrange it
     */
    public class LastActiveComparator implements Comparator<ChatSummary> {
        @Override
        public int compare(ChatSummary chat1, ChatSummary chat2) {
            Date chat1TimeStamp = chat1.getLastMessageDate();
            Date chat2TimeStamp = chat2.getLastMessageDate();

            return chat2TimeStamp.compareTo(chat1TimeStamp);
        }
    }
}
