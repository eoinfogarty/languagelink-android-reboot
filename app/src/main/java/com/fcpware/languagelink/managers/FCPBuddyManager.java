package com.fcpware.languagelink.managers;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.fcpware.languagelink.Constants;
import com.fcpware.languagelink.FCPConnection;
import com.fcpware.languagelink.ORM.BuddyModel;
import com.fcpware.languagelink.ORM.SavableMessage;
import com.fcpware.languagelink.R;
import com.fcpware.languagelink.XMPP.vcard.packet.VCard;
import com.fcpware.languagelink.events.BuddyListSetEvent;
import com.fcpware.languagelink.events.BuddyUpdateEvent;
import com.fcpware.languagelink.events.BusProvider;
import com.fcpware.languagelink.events.NewAvatarEvent;
import com.fcpware.languagelink.events.RosterEvent;
import com.squareup.otto.Subscribe;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.roster.packet.RosterPacket;
import org.jxmpp.util.XmppStringUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * This class manages friend objects
 * this is not the same as the roster manager
 * the buddy manager manages the objects associated with the names on the roster
 */
public class FCPBuddyManager {

    private static final String TAG = FCPBuddyManager.class.getSimpleName();

    public static final int BUDDY_UPDATE_NICK = 0;
    public static final int BUDDY_UPDATE_PRESENCE = 1;
    public static final int BUDDY_UPDATE_AVATAR = 2;

    private static FCPBuddyManager instance = null;    // Singleton variable

    private FCPNotificationManager fcpNotificationManager;
    private FCPAvatarManager fcpAvatarManager;
    private FCPConnection fcpConnection;
    private Context context;

    private HashMap<String, BuddyModel> buddies; //the key is the jid of the buddy
    private HashMap<String, Presence> presences;

    public synchronized static FCPBuddyManager getInstance(Context context) {
        if (instance == null) {
            instance = new FCPBuddyManager(context);
        }
        return instance;
    }

    public FCPBuddyManager(Context context) {
        BusProvider.getInstance().register(this);
        this.context = context;

        fcpConnection = FCPConnection.getInstance();
        fcpAvatarManager = FCPAvatarManager.getInstance(this.context);
        fcpNotificationManager = FCPNotificationManager.getInstance(this.context);


        buddies = new HashMap<String, BuddyModel>();
        presences = new HashMap<String, Presence>();

        List<BuddyModel> savedBuddies = BuddyModel.listAll(BuddyModel.class);
        Log.d(TAG, "Saved Buddies");
        for(BuddyModel model: savedBuddies){
            buddies.put(model.getJid(), model);
            Log.d(TAG, "Friend : " + model.getJid());
            Log.d(TAG, "Nickname : " + model.getNickName() );
            Log.d(TAG, "Intro : " + model.getIntro());
            Log.d(TAG, "Avatar : " + Arrays.toString(model.getAvatar()));
            Log.d(TAG, "Status : " + model.getFriendStatus());
            Log.d(TAG, "Languages : " + model.languagesString);
        }

    }

    //events from the roster manager that we reflect in our buddy list and pass send an event to inform the ui
    @Subscribe
    public void rosterEvent(RosterEvent event) {

        switch (event.getAction()) {
            case FCPRosterManager.REQUEST:
                fcpNotificationManager.friendRequestNotification(buddies.get(event.getEntry()));
            case FCPRosterManager.UPDATED:
                setBuddy(event.getEntry());
                setSubscription(event.getEntry(), event.getEntryStatus(), event.getEntryType());
                setPresence(event.getEntry(), event.getPresence());
                break;
            case FCPRosterManager.ADDED:
                setBuddy(event.getEntry());
                setSubscription(event.getEntry(), event.getEntryStatus(), event.getEntryType());
                setPresence(event.getEntry(), presences.get(event.getEntry()));
                break;
            case FCPRosterManager.DELETED:
                removeBuddy(event.getEntry());
                break;
            case FCPRosterManager.FRIEND_REQUEST_CONFIRMED:
                fcpNotificationManager.friendRequestConfirmed(buddies.get(event.getEntry()));
                break;
            default:

        }
        // Tell anyone who is listening buddy list has been set
        BusProvider.getInstance().post(new BuddyListSetEvent());
    }

    /**
     * Check to see if the buddy is all ready on the list list, if they are we can ignore
     * If they are not we should add them
     * adding or updating a buddy is done here
     *
     * @param buddyJid - the jid of the buddy to be set
     */
    public void setBuddy(String buddyJid) {

        //new buddy
        if (!buddies.containsKey(buddyJid)) {

            BuddyModel buddyModel = new BuddyModel();
            try {
                VCard buddyVCard = new VCard();
                buddyVCard.load(fcpConnection.getConnection(), buddyJid);
                buddyModel = new BuddyModel(buddyJid, buddyVCard);

            } catch (SmackException  | XMPPException.XMPPErrorException  | ClassCastException e) {

                buddyModel = setNoVcardAvailable(buddyJid, buddyModel);
            }

            buddyModel.save();
            buddies.put(buddyJid, buddyModel);
        }
    }

    private BuddyModel setNoVcardAvailable(String buddyJid, BuddyModel buddyModel) {
        Log.d(TAG, "Couldn't vcard for this user");
        //Show default facebook profile
        Resources res = context.getResources();
        Drawable drawable = res.getDrawable(R.drawable.com_facebook_profile_picture_blank_portrait);
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] bitMapData = stream.toByteArray();

        buddyModel.setAvatar(bitMapData);
        buddyModel.setNickName(" ");
        buddyModel.setJabberId(buddyJid);

        return buddyModel;
    }

    public void setNickname(String buddyJid, String nickname) {
        if (buddies.containsKey(buddyJid)) { // ie not from yourself
            buddies.get(buddyJid).setNickName(nickname);
            // Tell anyone who is listening buddy list has been changed
            BusProvider.getInstance().post(new BuddyUpdateEvent(BUDDY_UPDATE_NICK, buddyJid, null, nickname, null));
        }
    }

    public void setPresence(String buddyJid, Presence presence) {
        //if the presence is null just set it as unavailable
        if (presence == null) {
            presence = new Presence(Presence.Type.unavailable);
        }

        presences.put(buddyJid, presence); // all presences the client will receive
        if (buddies.containsKey(buddyJid)) {
            presences.put(buddyJid, presence);
            buddies.get(buddyJid).setPresence(presence);
            // Tell anyone who is listening buddy has been updated
            BusProvider.getInstance().post(new BuddyUpdateEvent(BUDDY_UPDATE_PRESENCE, buddyJid, null, null, presence));
        }
    }

    /**
     * When a new avatar has been downloaded we will check if one of our chat summaries needs to hear about his update
     * We also need to update our summary object
     *
     * @param event - New Avatar Event
     */
    @Subscribe
    public void onAvatarChange(NewAvatarEvent event) {
        String constructedJid = XmppStringUtils.escapeLocalpart(event.username) + "@" + Constants.HOST;

        if (buddies.containsKey(constructedJid)) { // ie not from yourself
            // Tell anyone who is listening buddy has been updated
            BusProvider.getInstance().post(new BuddyUpdateEvent(BUDDY_UPDATE_AVATAR, constructedJid, getAvatarUriString(constructedJid), null, null));
        }
    }

    public void setSubscription(String buddyJid, RosterPacket.ItemStatus entryStatus, RosterPacket.ItemType entryType) {
        if (buddies.containsKey(buddyJid)) { // ie not from yourself
            buddies.get(buddyJid).setFriendStatus(entryType, entryStatus);
        }
    }

    public void removeBuddy(String deletedJid) {
        FCPChatSummaryManager fcpChatSummaryManager = FCPChatSummaryManager.getInstance(this.context);
        buddies.get(deletedJid).delete();
        buddies.remove(deletedJid);
        fcpAvatarManager.removeAvatar(deletedJid);
        fcpChatSummaryManager.deleteSummary(deletedJid);
        SavableMessage.deleteAll(SavableMessage.class, "sent_to = ? or received_from = ?", deletedJid, deletedJid);
    }

    public HashMap<String, BuddyModel> getBuddyMap() {
        return buddies;
    }

    // Return a buddy who is on your roster or create a buddy model and return it
    public BuddyModel getBuddy(String buddyJid) {
        //todo could be null
        return buddies.get(buddyJid);
    }

    //TODO refactor
    public BuddyModel getModel(String buddyJid) {

        BuddyModel buddy = buddies.get(buddyJid);

        if(buddy==null){
            try {


                VCard usersVCard = new VCard();
                usersVCard.load(fcpConnection.getConnection(), buddyJid);

                buddy = new BuddyModel(buddyJid, usersVCard);

            } catch (XMPPException.XMPPErrorException | SmackException e) {
                e.printStackTrace();
            }
        }


        return buddy;
    }

    public String getAvatarUriString(String buddyJid) {
        File avatar = fcpAvatarManager.getAvatar(buddyJid);
        return avatar.toURI().toString();
    }
}
