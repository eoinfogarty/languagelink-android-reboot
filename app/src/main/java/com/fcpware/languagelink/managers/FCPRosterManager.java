package com.fcpware.languagelink.managers;

import android.content.Context;
import android.util.Log;

import com.fcpware.languagelink.FCPConnection;
import com.fcpware.languagelink.UI.friends.FriendProfileActivity;
import com.fcpware.languagelink.XMPP.PEP.PEPEvent;
import com.fcpware.languagelink.XMPP.PEP.PEPListener;
import com.fcpware.languagelink.XMPP.PEP.bak_packet.NickNames;
import com.fcpware.languagelink.events.BusProvider;
import com.fcpware.languagelink.events.RosterEvent;
import com.fcpware.languagelink.models.FCPUserModel;

import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.roster.RosterEntry;
import org.jivesoftware.smack.roster.RosterListener;
import org.jivesoftware.smack.roster.packet.RosterPacket;
import org.jivesoftware.smackx.nick.packet.Nick;
import org.jxmpp.util.XmppStringUtils;

import java.util.Collection;
import java.util.HashMap;

/**
 * The roster manager gets the users roster
 * It manages any changes to the roster and passes that info to the buddy manager
 * It will handle subscriptions
 */
public class FCPRosterManager implements PEPListener, RosterListener {

    private static final String TAG = "FCPRosterManager";
    //these are the events that the roster manager will fire
    public static final int ADDED = 0;
    public static final int UPDATED = 1;
    public static final int DELETED = 2;
    public static final int REQUEST = 3;
    public static final int FRIEND_REQUEST_CONFIRMED = 4;

    private static FCPRosterManager instance = null;     // Singleton variable
    private FCPAvatarManager fcpAvatarManager;
    private FCPUserModel fcpUserModel;
    private FCPBuddyManager fcpBuddyManager;
    private FCPConnection fcpConnection;
    private Roster roster;
    private Context context;

    private HashMap<String , Presence> presences;

    public synchronized static FCPRosterManager getInstance(Context context) {
        if(instance==null){
            instance = new FCPRosterManager(context);
        }
        return instance;
    }

    public FCPRosterManager(Context context) {
        BusProvider.getInstance().register(this);
        Log.d(TAG, "Creating the Roster Manager - getting roster");
        this.context = context;

        fcpAvatarManager = FCPAvatarManager.getInstance(context);
        fcpBuddyManager = FCPBuddyManager.getInstance(context);
        fcpConnection = FCPConnection.getInstance();
        fcpUserModel = FCPUserModel.getInstance();


        roster = Roster.getInstanceFor(fcpConnection.getConnection());
        roster.setSubscriptionMode(Roster.SubscriptionMode.manual);
        roster.addRosterListener(this);

        fcpConnection.getConnection().addPacketListener(new PresenceSubscriptionListener().invoke(),
                                                        new PresenceSubscriptionFilter().invoke());
    }


    //TODO move pep events to buddy manager
    @Override
    public void eventReceived(String from, PEPEvent pepEvent) {
        Log.d(TAG, "Event from " + from + " : " + pepEvent.toXML());
        Log.d(TAG, "Element Name : " + pepEvent.getElementName());
        Log.d(TAG, "Namespace : " + pepEvent.getNamespace());

        //TODO
        NickNames items = (NickNames) pepEvent;
        Nick nick = items.getNickItem().getNick();

        fcpBuddyManager.setNickname(from, nick.getName());
    }

    @Override
    public void entriesAdded(Collection<String> entries) {
        Log.d(TAG,  "Entries added : " + entries.toString());
        //for every entry added create a user model and set it to the map
        fcpAvatarManager.setAvatars(entries);

        for(String entry : entries){

            RosterPacket.ItemStatus entryStatus = roster.getEntry(entry).getStatus();
            RosterPacket.ItemType entryType = roster.getEntry(entry).getType();

            Log.d(TAG, "Presence is as follows " + roster.getPresence(entry) );
            Presence presence = roster.getPresence(entry);


            BusProvider.getInstance().post(new RosterEvent(ADDED,
                                                           entry,
                                                           entryStatus,
                                                           entryType,
                                                           presence));
        }

//        fcpBuddyManager.syncWithRoster(entries);
    }

    @Override
    public void entriesUpdated(Collection<String> strings) {
        //for every entry added create a user model and set it to the map
        for(String updatedEntry : strings){

            RosterPacket.ItemStatus entryStatus = roster.getEntry(updatedEntry).getStatus();
            RosterPacket.ItemType entryType = roster.getEntry(updatedEntry).getType();
            Presence presence = roster.getPresence(updatedEntry);

            BusProvider.getInstance().post(new RosterEvent(UPDATED,
                                                           updatedEntry,
                                                           entryStatus,
                                                           entryType,
                                                           presence));

        }
    }

    @Override
    public void entriesDeleted(Collection<String> strings) {
        Log.d(TAG,  "Entries to delete" + strings.toString());
        for(String deletedEntry : strings){
            BusProvider.getInstance().post(new RosterEvent(DELETED,
                                                           deletedEntry,
                                                           null,
                                                           null,
                                                           null ));
        }
    }

    public void removeBuddy(String jabberID) {
        try {
            roster.removeEntry(roster.getEntry(jabberID));
        } catch (SmackException.NotLoggedInException e) {
            e.printStackTrace();
        } catch (SmackException.NoResponseException e) {
            e.printStackTrace();
        } catch (XMPPException.XMPPErrorException e) {
            e.printStackTrace();
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void presenceChanged(Presence presence) {
        Log.d(TAG, "presenceChanged for " + presence.getFrom() + " to " + presence.toString());
        String buddy = XmppStringUtils.parseBareJid(presence.getFrom());
        Log.d(TAG, "roster shoews presence as " + roster.getPresence(presence.getFrom()));
        roster.getEntry(presence.getFrom());
        fcpBuddyManager.setPresence(buddy, presence);

    }

    public int getUserStatus(String jid) {
        RosterEntry user = roster.getEntry(jid);
        int status = 0;

        if(user == null){
            status = FriendProfileActivity.NOT_A_FRIEND;
            Log.d(TAG, "Not a friend");
        } else {

            if(user.getStatus() == RosterPacket.ItemStatus.subscribe &&
               user.getType() == RosterPacket.ItemType.none)
            {
                status = FriendProfileActivity.PENDING_REQUEST;
                Log.d(TAG, "Pending Request");
            }

            if(user.getType()==RosterPacket.ItemType.both ||
               user.getType()==RosterPacket.ItemType.to)
            {
                status = FriendProfileActivity.FRIEND;
            }

            if(user.getType()==RosterPacket.ItemType.from){
                status = FriendProfileActivity.FRIEND_REQUEST;
            }

        }

        return status;
    }

    private class PresenceSubscriptionListener {
        public PacketListener invoke() {
            return new PacketListener() {
                @Override
                public void processPacket(Packet packet) throws SmackException.NotConnectedException {
                    Presence subscriptionPresence = (Presence) packet;

                    Log.d(TAG, "processPacket - We got a subscription presence packet");
                    Log.d(TAG, "processPacket - From : " + subscriptionPresence.getFrom());
                    Log.d(TAG, "processPacket - Type : " + subscriptionPresence.getType());

                    if(subscriptionPresence.getType()== Presence.Type.subscribe){
                        //automatically send a subscribed
                        Presence subscribed = new Presence(Presence.Type.subscribed);
                        subscribed.setTo(subscriptionPresence.getFrom());
                        fcpConnection.sendPacket(subscribed);
                    }

                    //if we get a subscription request we dont want to approve it , we should wait for the user to do it
                    if (subscriptionPresence.getType() == Presence.Type.subscribe &&
                            roster.getEntry(subscriptionPresence.getFrom()) == null) {
                        Log.d(TAG, "processPacket - We got a friend request");
                        createFriendRequest(subscriptionPresence);
                    }

                    //if we get a subscription request we dont want to approve it , we should wait for the user to do it
                    if (subscriptionPresence.getType() == Presence.Type.subscribe &&
                            roster.getEntry(subscriptionPresence.getFrom()) != null) {
                        Log.d(TAG, "processPacket - We got a friend request confirmation");
                        BusProvider.getInstance().post(new RosterEvent(FRIEND_REQUEST_CONFIRMED,
                                subscriptionPresence.getFrom(),
                                null,
                                null,
                                null));
                    }

                    if (subscriptionPresence.getType() == Presence.Type.unsubscribe) {
                        Log.d(TAG, "Removing buddy now");
                        //TODO slow and causes packet response timout
                        BusProvider.getInstance().post(new RosterEvent(DELETED,
                                                                       subscriptionPresence.getFrom(),
                                                                       null,
                                                                       null,
                                                                       null ));
                        removeBuddy(subscriptionPresence.getFrom());
                    }
                }
            };
        }

        private void createFriendRequest(Presence presence) {
            RosterPacket.ItemType entryType = RosterPacket.ItemType.none;
            RosterPacket.ItemStatus entryStatus = RosterPacket.ItemStatus.subscribe;

            RosterPacket friendRequest = new RosterPacket();
            friendRequest.setType(IQ.Type.set);

            RosterPacket.Item requestDetails = new RosterPacket.Item(presence.getFrom(), null);
            requestDetails.setItemType(entryType);
            requestDetails.setItemStatus(entryStatus);
            friendRequest.addRosterItem(requestDetails);
            fcpConnection.sendPacket(friendRequest);
            BusProvider.getInstance().post(new RosterEvent(REQUEST,
                                                           presence.getFrom(),
                                                           entryStatus,
                                                           entryType,
                                                           presence ));
        }
    }

    private class PresenceSubscriptionFilter {
        public PacketFilter invoke() {
            return new PacketFilter() {
                @Override
                public boolean accept(Packet packet) {
                    if (packet instanceof Presence) {
                        Presence presence = (Presence) packet;
                        if (presence.getType().equals(Presence.Type.subscribed)
                                || presence.getType().equals(Presence.Type.subscribe)
                                || presence.getType().equals(Presence.Type.unsubscribe)
                                || presence.getType().equals(Presence.Type.unsubscribed)) {
                            return true;
                        }
                    }
                    return false;
                }
            };
        }
    }
}
