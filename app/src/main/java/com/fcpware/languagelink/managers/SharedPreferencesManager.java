package com.fcpware.languagelink.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;
import android.util.Log;

import com.fcpware.languagelink.XMPP.vcard.packet.VCard;
import com.fcpware.languagelink.models.FCPLanguageModel;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jxmpp.util.XmppStringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * //todo
 * Created by eoinfogarty on 11/09/2014.
 */
public class SharedPreferencesManager {

    public static final String PREFS_NAME = "LoginDetails";
    public static final int PRIVATE_MODE = 0;
    public SharedPreferences setting;
    public SharedPreferences.Editor editor;
    public Context context;

    public SharedPreferencesManager(Context context) {
        this.context = context;
        setting = context.getSharedPreferences(PREFS_NAME, PRIVATE_MODE);
    }

    //Save user data by calling this method
    public void setLoginData(XMPPConnection connection) {

        // load vcard and save normally
        VCard vCard = new VCard();
        try {
            vCard.load(connection);
        } catch (SmackException.NoResponseException |
                 SmackException.NotConnectedException |
                 XMPPException.XMPPErrorException e) {

            e.printStackTrace();
        }

        setLoginData(connection,vCard);
    }

    //Save user data by calling this method
    public void setLoginData(XMPPConnection connection, VCard vCard) {
        Log.d("Shared Preferences", "setLoginData - " + vCard.getJabberId());
        Log.d("Shared Preferences", "setLoginData - " + vCard.getNickName());
        editor = setting.edit();
        editor.putBoolean("Login", true);
        editor.putString("Username", XmppStringUtils.parseBareJid(connection.getUser()));
        editor.putString("Password", "test");

        //vcard details
        Log.d("SharedPreferences", vCard.getJabberId());
        editor.putString("JabberId", vCard.getJabberId());
        editor.putString("EmailHome", vCard.getEmailHome());
        editor.putString("Nickname", vCard.getNickName());
        editor.putString("Intro", vCard.getIntro());
        editor.putString("Avatar", Base64.encodeToString(vCard.getAvatar(), Base64.DEFAULT));
        saveLanguageFile(vCard.getLanguages());

        editor.apply();
    }
    //this method for clearing all of the user information
    public void clearPreferences() {
        editor = setting.edit();
        editor.clear();
        editor.apply();
    }

    public void setNickname(String newNick){
        editor = setting.edit();
        editor.putString("Nickname", newNick);
        editor.apply();

    }

    public void setIntro(String intro){
        editor = setting.edit();
        editor.putString("Intro", intro);
        editor.apply();

    }

    public void saveLanguageFile(ArrayList languages){
        //save the users language abilities to file
        File fileDir = context.getFilesDir(); // we dont want this to be erased by the cache
        File languageFile = new File(fileDir, "languages");

        try {

            FileOutputStream fos = new FileOutputStream(languageFile);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(languages);

            oos.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Boolean hasLoggedIn(){
        return setting.getBoolean("Login", false);
    }

    public String getUsername(){
        return setting.getString("Username","");
    }

    public String getPassword(){
        return setting.getString("Password","");
    }

    public VCard getVCard() {

        VCard vCard = new VCard();
        vCard.setJabberId(setting.getString("JabberId", null));
        vCard.setNickName(setting.getString("Nickname", null));
        vCard.setEmailHome(setting.getString("EmailHome", null));
        vCard.setIntro(setting.getString("Intro", null));
        vCard.setAvatar(Base64.decode(setting.getString("Avatar", null), Base64.DEFAULT));

        //get languages from file
        //TODO Find a better way of saving the users details
        //TODO not good if you rotate it will save
        //load the users language abilities to file
        File fileDir = context.getFilesDir(); // we dont want this to be erased by the cache
        File languageFile = new File(fileDir, "languages");

        try {

            FileInputStream fis = new FileInputStream(languageFile);
            ObjectInputStream ois = new ObjectInputStream(fis);
            ArrayList<FCPLanguageModel> userLanguages = (ArrayList<FCPLanguageModel>) ois.readObject();
            vCard.setLanguages(userLanguages);

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return vCard;
    }

}
