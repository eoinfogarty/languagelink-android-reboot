package com.fcpware.languagelink;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;

import com.fcpware.languagelink.events.BusProvider;
import com.fcpware.languagelink.events.ConnectionEvent;
import com.squareup.otto.Subscribe;

public class BaseActivity extends ActionBarActivity {

    private static final String TAG = "BaseActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume - registering with bus");
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause - unregister with bus");
        BusProvider.getInstance().unregister(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart - starting flurry session");
    }

    @Override
    protected void onStop(){
        super.onStop();
        Log.d(TAG, "onStop - ending flurry session");
    }

    @Subscribe
    public void connectionEvent(ConnectionEvent event) {
        Log.d(TAG, "connectionEvent - connected = " + event.isConnected());

    }
}
